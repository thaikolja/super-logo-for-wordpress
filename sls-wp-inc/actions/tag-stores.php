<?php

if (!empty($_POST)) {extract($_POST);}


if (is_array($sls_wp_id)==1) {
	$rplc_arr=array_fill(0, count($sls_wp_id), "%d");
	$id_string=implode(",", array_map(array($wpdb, "prepare"), $rplc_arr, $sls_wp_id)); 	
} else {
	$id_string=$wpdb->prepare("%d", $sls_wp_id);
}
if ($act=="add_tag") {

	//$wpdb->query($wpdb->prepare("UPDATE ".SLS_WP_TABLE." SET sls_wp_tags=CONCAT(IFNULL(sls_wp_tags, ''), %s ) WHERE sls_wp_id IN ($id_string)", sls_wp_prepare_tag_string(strtolower($sls_wp_tags)))); 
	$wpdb->query($wpdb->prepare("UPDATE ".SLS_WP_TABLE." SET sls_wp_tags=CONCAT(IFNULL(sls_wp_tags, ''), %s ) WHERE sls_wp_id IN ($id_string)", sls_wp_prepare_tag_string($sls_wp_tags))); 
	//sls_wp_process_tags(sls_wp_prepare_tag_string(strtolower($sls_wp_tags)), "insert", $sls_wp_id); 
	sls_wp_process_tags(sls_wp_prepare_tag_string($sls_wp_tags), "insert", $sls_wp_id); 
}
elseif ($act=="remove_tag") {

	if (empty($sls_wp_tags)) {

		$wpdb->query("UPDATE ".SLS_WP_TABLE." SET sls_wp_tags='' WHERE sls_wp_id IN ($id_string)");
		sls_wp_process_tags("", "delete", $id_string);
	}
	else {		
		
		$wpdb->query($wpdb->prepare("UPDATE ".SLS_WP_TABLE." SET sls_wp_tags=REPLACE(sls_wp_tags, %s, '') WHERE sls_wp_id IN ($id_string)", $sls_wp_tags.",")); 
		$wpdb->query($wpdb->prepare("UPDATE ".SLS_WP_TABLE." SET sls_wp_tags=REPLACE(sls_wp_tags, %s, '') WHERE sls_wp_id IN ($id_string)", $sls_wp_tags."&#44;")); 
		sls_wp_process_tags($sls_wp_tags, "delete", $id_string); 
	}
}
?>