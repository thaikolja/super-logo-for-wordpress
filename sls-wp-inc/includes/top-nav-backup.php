<?php
$sls_wp_top_nav_hash[]='information';
$sls_wp_top_nav_links[SLS_WP_INFORMATION_PAGE]=__("<span class='fa fa-bolt'>&nbsp;</span> Quick Start", SLS_WP_TEXT_DOMAIN);

$sls_wp_top_nav_hash[]='logos';
$sls_wp_top_nav_links[SLS_WP_MANAGE_LOCATIONS_PAGE]=__("<span class='fa fa-circle'>&nbsp;</span> Logos", SLS_WP_TEXT_DOMAIN);
$sls_wp_top_nav_hash[]='add-logo';
$sls_wp_top_nav_links[SLS_WP_ADD_LOCATIONS_PAGE]=__("<span class='fa fa-plus'>&nbsp;</span> Add a Logo", SLS_WP_TEXT_DOMAIN);
$sls_wp_top_nav_hash[]='import';
$sls_wp_top_nav_links[SLS_WP_IMPORT_PAGE]=__("<span class='fa fa-globe'>&nbsp;</span> Import/Export", SLS_WP_TEXT_DOMAIN);
$sls_wp_top_nav_hash[]='settings'; 
$sls_wp_top_nav_links[SLS_WP_SETTINGS_PAGE1]=__("<span class='fa fa-cog'>&nbsp;</span> Settings", SLS_WP_TEXT_DOMAIN);

if (function_exists("do_sls_wp_hook")){
	do_sls_wp_hook("sls_wp_top_nav_links", "", array(&$sls_wp_top_nav_hash, &$sls_wp_top_nav_links, &$sls_wp_top_nav_sub_links));
}

	
print "<br>";
$style_var = "";
if (!empty($_POST['sls_wp_thanks'])) {$sls_wp_vars['thanks'] = $_POST['sls_wp_thanks']; unset($_POST);}
$sls_wp_thanks = (!empty($sls_wp_vars['thanks']))? $sls_wp_vars['thanks'] : "";
print <<<EOQ
<ul class="tablist">\n
EOQ;
$ctr=0; $tsn_links_js="<script>\nvar tsn_link_arr = [];"; $tsn_links_output="";
$tm_st = ((time() - strtotime($sls_wp_vars["start"]))/60/60/24>=30);
foreach ($sls_wp_top_nav_links as $key=>$value) {
	$current_var=(preg_match("@$_GET[page]@",$key))? "current_top_link" : "" ;
	if (isset($_GET['pg'])){
		if($sls_wp_top_nav_hash[$ctr]=="logos"){
		print "<li class=\"top_nav_li $sls_wp_top_nav_hash[$ctr]\" id=\"\"><a href=\"$key\"  id='__$sls_wp_top_nav_hash[$ctr]' class='button button-primary' style=''>$value</a></li>\n";
		} else {
		print "<li class=\"top_nav_li $sls_wp_top_nav_hash[$ctr]\" id=\"$current_var\"><a href=\"$key\"  id='__$sls_wp_top_nav_hash[$ctr]' class='button button-primary' style=''>$value</a></li>\n";
		}
	
	} else {
	
	if($sls_wp_top_nav_hash[$ctr]=="add-logo"){
	
	print "<li class=\"top_nav_li $sls_wp_top_nav_hash[$ctr]\" id=\"\"><a href=\"$key\"  id='__$sls_wp_top_nav_hash[$ctr]' class='button button-primary' style=''>$value</a></li>\n";
	
	} else {
	print "<li class=\"top_nav_li $sls_wp_top_nav_hash[$ctr]\" id=\"$current_var\"><a href=\"$key\"  id='__$sls_wp_top_nav_hash[$ctr]' class='button button-primary' style=''>$value</a></li>\n";
	}
	
	
	}

	$tsn_links_js.="tsn_link_arr['$sls_wp_top_nav_hash[$ctr]']='';";

	if (!empty($sls_wp_top_nav_sub_links[$sls_wp_top_nav_hash[$ctr]])) {
		$cur = ""; $ctr2=0;
		foreach ($sls_wp_top_nav_sub_links[$sls_wp_top_nav_hash[$ctr]] as $key2=>$value2) {
			if (preg_match("@$sls_wp_top_nav_hash[$ctr]@", $_SERVER['REQUEST_URI'])) {
				if (empty($_GET['pg']) && !preg_match("@&pg@", $value2)) {
					$cur = "current_sub_link";
				} elseif (!empty($_GET['pg']) && preg_match("@$_GET[pg]@", $value2)) {
					$cur = "current_sub_link";
				}  else {
					$cur = "";
				}
				$tsn_links_output.="<a href='$value2' class='$cur'>$key2</a>";
			}
			
			$tsn_links_js .= "tsn_link_arr['$sls_wp_top_nav_hash[$ctr]']+=\"<a href='$value2' class='top_nav_sub_a $cur' id='$sls_wp_top_nav_hash[$ctr]_$ctr2' onmouseover='level3_links(this, &quot;__$sls_wp_top_nav_hash[$ctr]&quot;, &quot;show&quot;)' onmouseout='level3_links(this, &quot;__$sls_wp_top_nav_hash[$ctr]&quot;, &quot;hide&quot;)'>$key2</a>\";";
				if (!empty($sls_wp_top_nav_sub2_links[$value2])) {
					$tsn_links_js.= "tsn_link_arr['$sls_wp_top_nav_hash[$ctr]_$ctr2']='';";
					foreach ($sls_wp_top_nav_sub2_links[$value2] as $level3_title => $level3_url) {
						
						$tsn_links_js.= "tsn_link_arr['{$sls_wp_top_nav_hash[$ctr]}_{$ctr2}']+=\"<a href='$level3_url' class='' id=''>$level3_title</a>\"; " ;
					}
				}
			$ctr2++;
		}
	}
	$ctr++;
}


$tsn_links_js.="jQuery(document).ready(function(){ {$style_var} });\n";
$tsn_links_js.="</script>";


$sls_wp_vars['sls_wp_latest_version_check_time'] = (empty($sls_wp_vars['sls_wp_latest_version_check_time']))? date("Y-m-d H:i:s") : $sls_wp_vars['sls_wp_latest_version_check_time'];
if (empty($sls_wp_vars['sls_wp_latest_version']) || (time() - strtotime($sls_wp_vars['sls_wp_latest_version_check_time']))/60>=(60*12)){ 
	
	$sls_wp_latest_version = '';
	
	
	$sls_wp_vars['sls_wp_latest_version_check_time'] = date("Y-m-d H:i:s");
	$sls_wp_vars['sls_wp_latest_version'] = $sls_wp_latest_version;
} else {
	$sls_wp_latest_version = $sls_wp_vars['sls_wp_latest_version'];
}

if (strnatcmp($sls_wp_latest_version, $sls_wp_version) > 0) { 
	$sls_wp_plugin = SLS_WP_DIR . "/super-logo-showcase.php";
	$sls_wp_update_link = admin_url('update.php?action=upgrade-plugin&plugin=' . $sls_wp_plugin);
	$sls_wp_update_link_nonce = wp_nonce_url($sls_wp_update_link, 'upgrade-plugin_' . $sls_wp_plugin);
	$sls_wp_update_msg = "&nbsp;&gt;&nbsp;<a href='$sls_wp_update_link_nonce' style='color:#900; font-weight:bold;' onclick='confirmClick(\"".__("You will now be updating to Store Locator", SLS_WP_TEXT_DOMAIN)." v$sls_wp_latest_version, ".__("click OK or Confirm to continue", SLS_WP_TEXT_DOMAIN).".\", this.href); return false;'>".__("Update to", SLS_WP_TEXT_DOMAIN)." $sls_wp_latest_version</a>";
} else {
	$sls_wp_update_msg = "";
}


if ( defined("SLS_WP_ADDONS_PLATFORM_DIR") ) {
	$sls_wp_vars['sls_wp_latest_ap_check_time'] = (empty($sls_wp_vars['sls_wp_latest_ap_check_time']))? date("Y-m-d H:i:s") : $sls_wp_vars['sls_wp_latest_ap_check_time'];

	if ( (empty($sls_wp_vars['sls_wp_latest_ap_version']) || (time() - strtotime($sls_wp_vars['sls_wp_latest_ap_check_time']))/60>=(60*12)) ) { //12-hr db caching of version info
		$ap_update = sls_wp_remote_data(array(
			'pagetype' => 'ap',
			'dir' => SLS_WP_ADDONS_PLATFORM_DIR, 
			'key' => sls_wp_data('sls_wp_license_' . SLS_WP_ADDONS_PLATFORM_DIR)
		));
		$ap_latest_version = (!empty($ap_update[0]))? preg_replace("@\.zip|".SLS_WP_ADDONS_PLATFORM_DIR."\.@", "", $ap_update[0]['filename']) : 0;
		
		
		$sls_wp_vars['sls_wp_latest_ap_check_time'] = date("Y-m-d H:i:s");
		$sls_wp_vars['sls_wp_latest_ap_version'] = $ap_latest_version;
	} else {
		$ap_latest_version = $sls_wp_vars['sls_wp_latest_ap_version'];
	}

	$ap_readme = SLS_WP_ADDONS_PLATFORM_PATH."/readme.txt"; 
	if (file_exists($ap_readme)) {
		$rm_txt = file_get_contents($ap_readme);
		preg_match("/\n[ ]*stable tag:[ ]?([^\n]+)(\n)?/i", $rm_txt, $cv); //var_dump($rm_txt); var_dump($cv);
		$ap_version = (!empty($cv[1]))? trim($cv[1]) : "1.0" ;
	} else {$ap_version = "1.0";}
	
	$ap_title = ucwords(str_replace("-", " ", SLS_WP_ADDONS_PLATFORM_DIR));
	$ap_update_msg = ucwords(str_replace("-", " ", SLS_WP_ADDONS_PLATFORM_DIR))." Version $ap_latest_version is available";
	$ap_update = (strnatcmp($ap_latest_version, $ap_version) > 0)? "&nbsp;|&nbsp;<a href='#' style='color:#900; font-weight: bold;' onclick='alert(\"$ap_title v$ap_latest_version ".__("is available for download -- you are currently using v$ap_version. \\n\\n\\t1) Please use the download link from the email receipt sent to you for your $ap_title purchase, \\n\\n\\t2) Extract the zip file to your computer, then \\n\\n\\t3) Upload the &apos;".SLS_WP_ADDONS_PLATFORM_DIR."&apos; folder to &apos;".SLS_WP_ADDONS_PATH."&apos; on your website using FTP for the latest $ap_title version", SLS_WP_TEXT_DOMAIN).".\"); return false;' title='$ap_update_msg'>Get the latest version{$ap_latest_version}</a>" : "" ;
} else { $ap_update = ""; }



print "</ul>
<div class='clearfix'></div>
";





if (!extension_loaded("curl")) {
	if (!empty($_GET['curl_msg']) && $_GET['curl_msg'] == 1){$sls_wp_vars['curl_msg'] = 'hide'; }
	if (empty($sls_wp_vars['curl_msg']) || $sls_wp_vars['curl_msg'] != 'hide') {
		print "<br><div class='sls-wp-menu-alert' style='line-height: 22px;'><b>".__("Important Note", SLS_WP_TEXT_DOMAIN).":</b><br>
		".__("It appears that you do not have <a href='http://us3.php.net/manual/en/book.curl.php' target='_blank'>cURL</a> actively running on this website.  cURL or <a href='http://us3.php.net/manual/en/function.file-get-contents.php' target='_blank'>file_get_contents()</a> needs to be active in order to run Super Logo Showcase", SLS_WP_TEXT_DOMAIN).".
		<br>
(<a href='".$_SERVER['REQUEST_URI']."&curl_msg=1'>".__("Hide Message", SLS_WP_TEXT_DOMAIN)."</a>)
		</div>";
			
	}
}


if (!empty($_GET['file_perm_msg']) && $_GET['file_perm_msg'] == 1){$sls_wp_vars['file_perm_msg'] = 'hide'; }
if (empty($sls_wp_vars['file_perm_msg']) || $sls_wp_vars['file_perm_msg'] != 'hide') {
	$sls_wp_vars['file_perm_check_time'] = (empty($sls_wp_vars['file_perm_check_time']))? date("Y-m-d H:i:s") : $sls_wp_vars['file_perm_check_time'];
	
	if (!isset($sls_wp_vars['perms_need_update']) || $sls_wp_vars['perms_need_update'] == 1 || ($sls_wp_vars['perms_need_update'] == 0 && (time() - strtotime($sls_wp_vars['file_perm_check_time']))/60 >= (60*1)) ) { // 1-hr checks, when last check showed no files needed permissions updates
		
		sls_wp_permissions_check();
		$sls_wp_vars['file_perm_check_time'] = date("Y-m-d H:i:s");
	}
}


if (!empty($_GET['csv_imp_msg']) && $_GET['csv_imp_msg'] == 1){$sls_wp_vars['csv_imp_msg'] = 'hide'; }
if (empty($sls_wp_vars['csv_imp_msg']) || $sls_wp_vars['csv_imp_msg'] != 'hide') {
	$max_input_vars_value = ini_get('max_input_vars');
	$max_input_default = ( !empty($max_input_vars_value) && $max_input_vars_value <= 1000 ); 
	$csv_needs_mod = 
	( 
		( (file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/csv-xml-importer-exporter.php") 
			&& sls_wp_data('sls_wp_activation_csv-xml-importer-exporter')!==NULL) 
			|| (file_exists(SLS_WP_ADDONS_PATH."/csv-importer-exporter-g2/csv-importer-exporter-g2.php") 
			&& sls_wp_data('sls_wp_activation_csv-importer-exporter-g2')!==NULL)
		) 
		&& strnatcmp(phpversion(), '5.3.9') >= 0 
		&& $max_input_default
	);
	
	if ($csv_needs_mod) {

	}
}


$sls_wp_notice_id = 'files_in_addons_dir';
if (!empty($_GET[$sls_wp_notice_id]) && $_GET[$sls_wp_notice_id] == 1){$sls_wp_vars[$sls_wp_notice_id] = 'hide'; }
if (empty($sls_wp_vars[$sls_wp_notice_id]) || $sls_wp_vars[$sls_wp_notice_id] != 'hide') {
	$addons_contents = glob(SLS_WP_ADDONS_PATH."/*.*", GLOB_NOSORT);
	if (!empty($addons_contents)) {
	   foreach ($addons_contents as $a_item) {
		$the_a_file = str_replace(SLS_WP_ADDONS_PATH."/", "", $a_item);
		if (!is_dir($a_item) && $the_a_file != "index.php" && $the_a_file != "dummy.php" && !preg_match("@\.zip$@", $the_a_file) && !preg_match("@error@", $the_a_file) ) {
			$not_a_dir[] = $the_a_file;
		}
	   }
	   if (!empty($not_a_dir)) {
		print "<br><div class='sls-wp-menu-alert' style='line-height: 22px;'><b>".__("Important Note", SLS_WP_TEXT_DOMAIN).":</b><br>
		".__("You have placed files in your 'addons' directory here", SLS_WP_TEXT_DOMAIN).": <b>/".str_replace(ABSPATH, "", SLS_WP_ADDONS_PATH)."/</b>. ".__("There should only be folders.  All addon-related files need to be inside of their proper addon folder in order to work with Store Locator", SLS_WP_TEXT_DOMAIN).". (<b>e.g.</b> <b style='color:DarkGreen'>".__("Correct", SLS_WP_TEXT_DOMAIN).":</b> /addons<b>/addons-platform/</b>addons-platform.php, <b style='color: DarkRed'>".__("Incorrect", SLS_WP_TEXT_DOMAIN).":</b> /addons/addons-platform.php) <br><br><b>".__("Files that need to be moved", SLS_WP_TEXT_DOMAIN)."</b>: ";
		print "".implode($not_a_dir, ",  ");
		print "<br><div style='float:right'>
(<a href='".$_SERVER['REQUEST_URI']."&{$sls_wp_notice_id}=1'>".__("Hide Message Permanently", SLS_WP_TEXT_DOMAIN)."</a>)</div>
<br clear='all'>
		</div>";
	   }
      }
}


if (!empty($_POST) && function_exists("do_sls_wp_hook")){ do_sls_wp_hook("sls_wp_admin_form_post"); /*print "<br>";*/ }
if (function_exists("do_sls_wp_hook")) { do_sls_wp_hook("sls_wp_admin_data"); } 
?>

<?php  sls_wp_data('sls_wp_vars', 'update', $sls_wp_vars); ?>