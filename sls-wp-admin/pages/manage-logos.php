<?php
include_once(SLS_WP_INCLUDES_PATH."/top-nav.php");
?>
<div class='wrap'>
<?php

if (!empty($_GET['edit'])){ print "<style>#wpadminbar {display:none !important;}</style>"; }

sls_wp_initialize_variables();

$hidden="";
foreach($_GET as $key=>$val) {
	
	if ($key!="q" && $key!="o" && $key!="d" && $key!="changeView" && $key!="start") {
		$hidden.="<input type='hidden' value='$val' name='$key'>\n"; 
	}
}

include(SLS_WP_ACTIONS_PATH."/process-logos.php");

print "<table style='width:100%'><tr><td>";
print "<div class='mng_loc_forms_links'>";

if (empty($_GET['q'])){ $_GET['q']=""; }
$search_value = ($_GET['q']==="")? "" : sls_comma(stripslashes($_GET['q'])) ;

print "<div><form name='searchForm'><!--input type='button' class='button-primary' value='Add New' onclick=\"\$aLD=jQuery('#addLocationsDiv');if(\$aLD.css('display')!='block'){\$aLD.fadeIn();}else{\$aLD.fadeOut();}return false;\">&nbsp;&nbsp;--><input value='".$search_value."' name='q' type='text' placeholder='Search'>$hidden</form></div>";

print "<div>
<nobr><select name='sls_wp_admin_locations_per_page' onchange=\"LF=document.forms['locationForm'];salpp=document.createElement('input');salpp.type='hidden';salpp.value=this.value;salpp.name='sls_wp_admin_locations_per_page';LF.appendChild(salpp);LF.act.value='locationsPerPage';LF.submit();\">
<optgroup label='# ".__("Locations", SLS_WP_TEXT_DOMAIN)."'>";

$opt_arr=array(10,25,50,100,200,300,400,500,1000,2000,4000,5000,10000);
foreach ($opt_arr as $value) {
	$selected=($sls_wp_admin_locations_per_page==$value)? " selected " : "";
	print "<option value='$value' $selected>$value</option>";
}
print "</optgroup></select>
</nobr>
</div>";

if (!empty($_GET['_wpnonce'])){ $_SERVER['REQUEST_URI'] = str_replace("&_wpnonce=".$_GET['_wpnonce'], "", $_SERVER['REQUEST_URI']);}

$is_normal_view = ($sls_wp_vars['location_table_view']=="Normal");
$is_using_tagger = (sls_wp_data('sls_wp_location_updater_type')=="Tagging");




function regeocoding_link(){
	global $wpdb, $where, $master_check, $sls_wp_uploads_path, $web_domain, $extra, $sls_wp_base, $sls_wp_uploads_base, $text_domain;
	if (file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-link.php")) {
		include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-link.php");	
	}
}
if (function_exists("addto_sls_wp_hook")) {addto_sls_wp_hook('sls_wp_mgmt_bar_links', 'regeocoding_link', '', '', 'csv-xml-importer-exporter');} 
else {regeocoding_link();}

if (file_exists(SLS_WP_ADDONS_PATH."/multiple-field-updater/multiLocationUpdate.php") && !function_exists("do_sls_wp_hook")) {
	print "<div> | ".$updater_type."</div>";
}

print "</div>";
print "</td><td>";


sls_wp_sls_set_query_defaults();

if(file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-query.php")){
	include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-query.php");
}


	$numMembers=$wpdb->get_results("SELECT sls_wp_id FROM ".SLS_WP_TABLE." $where");
	$numMembers2=count($numMembers); 
	$start=(empty($_GET['start']))? 0 : $_GET['start'];
	$num_per_page=$sls_wp_vars['admin_locations_per_page']; //edit this to determine how many locations to view per page of 'Manage Locations' page
	if ($numMembers2!=0) {include(SLS_WP_INCLUDES_PATH."/search-links.php");}


print "</td></tr></table>";

print "<form name='locationForm' method='post' enctype='multipart/form-data'>";

if(empty($_GET['d'])) {$_GET['d']="";} if(empty($_GET['o'])) {$_GET['o']="";}


$master_check = (!empty($master_check))? $master_check : "" ;
include(SLS_WP_INCLUDES_PATH."/logos-management.php");
print "<table class='widefat' cellspacing=0 id='loc_table'>
<thead><tr >
<th colspan='1'><input type='checkbox' onclick='checkAll(this,document.forms[\"locationForm\"])' id='master_checkbox' $master_check></th>

<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_pos&d=$d'>".__("Position", SLS_WP_TEXT_DOMAIN)."</a></th>";

if (function_exists("do_sls_wp_hook") && !empty($sls_wp_columns)){
	do_sls_wp_location_table_header();
} else {
	
	$th_co = ($is_normal_view)? "</th>\n<th>" : ", " ;
	$th_style = ($is_normal_view)? "" : "style='white-space: nowrap;' " ;
	
	print "<th {$th_style}><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_logo&d=$d'>".__("Name", SLS_WP_TEXT_DOMAIN)."</a>{$th_co}
  Logo{$th_co}
<a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_state&d=$d'>".__("Category", SLS_WP_TEXT_DOMAIN)."</a>{$th_co}
Published status</th>
";

}
$upnext;
$downnext;
print "
<th colspan='2'>".__("Actions", SLS_WP_TEXT_DOMAIN)."</th>
</tr></thead>";

	$o=esc_sql($o); $d=esc_sql($d); 
	$start=esc_sql($start); $num_per_page=esc_sql($num_per_page); 
	if ($locales=$wpdb->get_results("SELECT * FROM ".SLS_WP_TABLE." $where ORDER BY $o $d LIMIT $start, $num_per_page", ARRAY_A)) { 
		if (function_exists("do_sls_wp_hook") && !empty($sls_wp_columns)){
			
			$colspan=($sls_wp_vars['location_table_view']!="Normal")? 	(count($sls_wp_columns)-count($sls_wp_omitted_columns)+4) : (count($sls_wp_normal_columns)-3+4);
		} else {
			$colspan=($sls_wp_vars['location_table_view']!="Normal")? 	18 : 11;
		}
		
		$bgcol="";
		$moveRow=0;
		foreach ($locales as $value) {
			$bgcol=($bgcol==="" || $bgcol=="#eee")?"#fff":"#eee";			
			$value=array_map("trim",$value);
			
			if (!empty($_GET['edit']) && $value['sls_wp_id']==$_GET['edit']) {
				sls_wp_single_location_info($value, $colspan, $bgcol);
			}
			else {
				
			
				if(empty($_GET['edit'])) {$_GET['edit']="";}
				$edit_link = str_replace("&edit=$_GET[edit]", "",$_SERVER['REQUEST_URI'])."&edit=" . $value['sls_wp_id'] ."#a$value[sls_wp_id]'";
				
				$up_link = SLS_WP_MANAGE_LOCATIONS_PAGE."&up=".$value['sls_pos']."&upid=".$value['sls_wp_id'] ."";
				$down_link = SLS_WP_MANAGE_LOCATIONS_PAGE."&down=" . $value['sls_pos'] ."&upid=" . $value['sls_wp_id'] ."";
				
				print "<tr style='background-color:$bgcol' id='sls_wp_tr-$value[sls_wp_id]'>
			<th><input type='checkbox' name='sls_wp_id[]' value='$value[sls_wp_id]'></th>
			
			<td> $value[sls_pos] </td>";

				if (function_exists("do_sls_wp_hook") && !empty($sls_wp_columns)){
					do_sls_wp_location_table_body($value);
				} else {
					if ($is_normal_view) {
						
						$tco_address = $tco_address2 = $tco_city = $tco_state = $tco_zip = "</td>\n<td>";
						$strong_addr_open = $strong_addr_close = "";
					} else {
						
					}
					
					$upload_image_dir=$sls_wp_uploads_path."/images/".$value['sls_wp_id'].'/';
					$img = '';
					if (is_dir($upload_image_dir)) {
						$upload_dir=$sls_wp_uploads_path."/images/".$value['sls_wp_id'].'/*';
						$sls_wp_uploads_base=$sls_wp_uploads['baseurl']."/sls-wp-uploads";
						
						$upload_dir=$sls_wp_uploads_path."/images/".$value['sls_wp_id'].'/';
						$sls_wp_uploads_base=$sls_wp_uploads['baseurl']."/sls-wp-uploads/images/";
						if(file_exists($upload_dir."ori_".$value['sls_wp_id'].".png")) {
							$img=$sls_wp_uploads_base.$value['sls_wp_id'].'/'.$value['sls_wp_id'].'.png';
						}
					}
					if(!empty($img))
					{
						$image='<img class="portrait" src="'.$img.'?t='.time().'">';	
						
					}
					else{
						$image='<img class="portrait" src="'.SLS_WP_BASE.'/images/default.png">';	;
					}
					$image='<div class="super-img">'.$image.'</div>';
				$sls_category = ($value['sls_wp_state']=='default' || $value['sls_wp_state']=='')? "None" : $value['sls_wp_state'] ;
				$publish=($value['sls_wp_is_published']=='1')? " Yes " : "No";	
					print "<td> $value[sls_wp_logo] </td>\n<td>{$image}</td> {$tco_state}
$sls_category</td>

<td>$publish</td>";

					if ($sls_wp_vars['location_table_view']!="Normal") {
						print "<td>$value[sls_wp_description]</td>
<td>$value[sls_wp_url]</td>
<td>$value[sls_wp_hours]</td>
<td>$value[sls_wp_phone]</td>
<td>$value[sls_wp_fax]</td>
<td>$value[sls_wp_email]</td>
<td>$value[sls_wp_image]</td>";
					}
				}
				print "
				<td><a class='edit_loc_link' href='".$edit_link."' id='$value[sls_wp_id]'><span class='fa fa-pencil'>&nbsp;</span>".__("Edit", SLS_WP_TEXT_DOMAIN)."</a>&nbsp;| <a class='del_loc_link' href='".wp_nonce_url("$_SERVER[REQUEST_URI]&delete=$value[sls_wp_id]", "delete-location_".$value['sls_wp_id'])."' onclick=\"confirmClick('Are you sure you wish to delete this store?', this.href); return false;\" id='$value[sls_wp_id]'><span class='fa fa-trash'>&nbsp;</span>".__("Delete", SLS_WP_TEXT_DOMAIN)."</a>
				</td>";
				if($moveRow>0 && $moveRow<$numMembers2-1)
				{
				print "<td>&nbsp;&nbsp;<a class='edit_loc_link' href='".$up_link."' id='$value[sls_wp_id]'><img src='".SLS_WP_BASE."/images/up.png'></a>
				&nbsp;&nbsp;<a class='edit_loc_link' href='".$down_link."' id='$value[sls_wp_id]'><img src='".SLS_WP_BASE."/images/down.png'></a></td></tr>";
				}
				else if ($moveRow==0){
				print "<td>
					&nbsp;&nbsp;<a style='margin-left: 33px;' class='edit_loc_link' href='".$down_link."' id='$value[sls_wp_id]'><img src='".SLS_WP_BASE."/images/down.png'></a></td></tr>";
				}
				else {
					print "<td>&nbsp;&nbsp;<a  class='edit_loc_link' href='".$up_link."' id='$value[sls_wp_id]'><img src='".SLS_WP_BASE."/images/up.png'></a>
	</td></tr>";
				}
				$moveRow++;
			}
		}
	} else {
		$cleared=(!empty($_GET['q']))? str_replace("q=".str_replace(" ", "+", $_GET['q']) , "", $_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI'] ;
		$notice=(!empty($_GET['q']))? __("No results for this Search of ", SLS_WP_TEXT_DOMAIN)."<b>\"$_GET[q]\"</b> | <a href='$cleared'>".__("Clear&nbsp;Results", SLS_WP_TEXT_DOMAIN)."</a> $view_link" : __("You have no available logos", SLS_WP_TEXT_DOMAIN);
		print "<tr><td colspan='5'>$notice | <a href='".SLS_WP_ADD_LOCATIONS_PAGE."'>".__("Add a Logo", SLS_WP_TEXT_DOMAIN)."</a></td></tr>";
	}
	print "</table>
	<input name='act' type='hidden'><br>";
	wp_nonce_field("manage-locations_bulk");

if ($numMembers2!=0) {include(SLS_WP_INCLUDES_PATH."/search-links.php");}

print "</form>"; 
?>
</div>
<?php include(SLS_WP_INCLUDES_PATH."/sls-wp-footer.php"); ?>
<script>
	function deleteMarker(a){
		var url='<?php echo SLS_WP_BASE;?>/sls-wp-admin/pages/';
     jQuery.ajax({
		  type: 'POST',
		  data: {img:a} ,
		  url: '<?php echo SLS_WP_BASE;?>/sls-wp-admin/pages/imageDelete.php',
		  dataType:'json',
		  success: function(data, textStatus, XMLHttpRequest){
             jQuery('#editImage'+a).append("<span style='color:green; margin-left:50px; float:right;'>Deleted Succeslsully</span>");
		     jQuery('#editImage'+a).fadeOut(400);
			 jQuery('#sls_wp_image').prop('disabled', false);
		  },
		  error: function(MLHttpRequest, textStatus, errorThrown){
		  }
		  });
	
	}
	
	function delMarker(a){
		var url='<?php echo SLS_WP_BASE;?>/sls-wp-admin/pages/';
     jQuery.ajax({
		  type: 'POST',
		  data: {img_mrk:a} ,
		  url: '<?php echo SLS_WP_BASE;?>/sls-wp-admin/pages/imageDelete.php',
		  dataType:'json',
		  success: function(data, textStatus, XMLHttpRequest){
             jQuery('#editCmarker'+a).append("<span style='color:green; margin-left:50px; float:right;'>Deleted Succeslsully</span>");
		     jQuery('#editCmarker'+a).fadeOut(400);
			 jQuery('#sls_wp_marker').prop('disabled', false);
		  },
		  error: function(MLHttpRequest, textStatus, errorThrown){
		  }
		  });
	
	}
	
	function delImgBg(a){
		var url='<?php echo SLS_WP_BASE;?>/sls-wp-admin/pages/';
     jQuery.ajax({
		  type: 'POST',
		  data: {img_bg:a} ,
		  url: '<?php echo SLS_WP_BASE;?>/sls-wp-admin/pages/imageDelete.php',
		  dataType:'json',
		  success: function(data, textStatus, XMLHttpRequest){
             jQuery('#editBgImg'+a).append("<span style='color:green; margin-left:50px; float:right;'>Deleted Succeslsully</span>");
		     jQuery('#editBgImg'+a).fadeOut(400);
			 jQuery('#sls_wp_bg_image').prop('disabled', false);
		  },
		  error: function(MLHttpRequest, textStatus, errorThrown){
		  }
		  });
	
	}
</script>
