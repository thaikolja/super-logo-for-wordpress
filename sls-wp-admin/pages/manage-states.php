<?php
include_once(SLS_WP_INCLUDES_PATH."/top-nav.php");
?>
<div class='wrap'>
<?php

$wherecat = (!empty($_GET['q']))? $wpdb->prepare(" WHERE sls_wp_state LIKE '%%%s%%'", $_GET['q']) : "" ; //die($where);;

if (!empty($_GET['edit'])){ print "<style>#wpadminbar {display:none !important;}</style>"; }

sls_wp_initialize_variables();

$hidden="";
foreach($_GET as $key=>$val) {
	
	if ($key!="q" && $key!="o" && $key!="d" && $key!="changeView" && $key!="start") {
		$hidden.="<input type='hidden' value='$val' name='$key'>\n"; 
	}
}

include(SLS_WP_ACTIONS_PATH."/process-states.php");

print "<table style='width:100%'><tr><td>";
print "<div class='mng_loc_forms_links'>";

if (empty($_GET['q'])){ $_GET['q']=""; }
$search_value = ($_GET['q']==="")? "" : sls_comma(stripslashes($_GET['q'])) ;

print "<div><form name='searchForm'><!--input type='button' class='button-primary' value='Add New' onclick=\"\$aLD=jQuery('#addLocationsDiv');if(\$aLD.css('display')!='block'){\$aLD.fadeIn();}else{\$aLD.fadeOut();}return false;\">&nbsp;&nbsp;--><input value='".$search_value."' name='q' type='text' placeholder='Search'>$hidden</form></div>";

print "<div>
<nobr><select name='sls_wp_admin_locations_per_page' onchange=\"LF=document.forms['locationForm'];salpp=document.createElement('input');salpp.type='hidden';salpp.value=this.value;salpp.name='sls_wp_admin_locations_per_page';LF.appendChild(salpp);LF.act.value='locationsPerPage';LF.submit();\">
<optgroup label='# ".__("Locations", SLS_WP_TEXT_DOMAIN)."'>";

$opt_arr=array(10,25,50,100,200,300,400,500,1000,2000,4000,5000,10000);
foreach ($opt_arr as $value) {
	$selected=($sls_wp_admin_locations_per_page==$value)? " selected " : "";
	print "<option value='$value' $selected>$value</option>";
}
print "</optgroup></select>
</nobr>
</div>";

if (!empty($_GET['_wpnonce'])){ $_SERVER['REQUEST_URI'] = str_replace("&_wpnonce=".$_GET['_wpnonce'], "", $_SERVER['REQUEST_URI']);}

$is_normal_view = ($sls_wp_vars['location_table_view']=="Normal");
$is_using_tagger = (sls_wp_data('sls_wp_location_updater_type')=="Tagging");

function regeocoding_link(){
	global $wpdb, $where, $master_check, $sls_wp_uploads_path, $web_domain, $extra, $sls_wp_base, $sls_wp_uploads_base, $text_domain;
	if (file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-link.php")) {
		include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-link.php");	
	}
}
if (function_exists("addto_sls_wp_hook")) {addto_sls_wp_hook('sls_wp_mgmt_bar_links', 'regeocoding_link', '', '', 'csv-xml-importer-exporter');} 
else {regeocoding_link();}

if (file_exists(SLS_WP_ADDONS_PATH."/multiple-field-updater/multiLocationUpdate.php") && !function_exists("do_sls_wp_hook")) {
	print "<div> | ".$updater_type."</div>";
}

print "</div>";
print "</td><td>";


sls_wp_state_query_defaults();

if(file_exists(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-query.php")){
	include(SLS_WP_ADDONS_PATH."/csv-xml-importer-exporter/re-geo-query.php");
}


	$numMembers=$wpdb->get_results("SELECT sls_wp_id FROM ".SLS_WP_TABLE_CATEGORY." $wherecat");
	$numMembers2=count($numMembers); 
	$start=(empty($_GET['start']))? 0 : $_GET['start'];
	$num_per_page=$sls_wp_vars['admin_locations_per_page']; //edit this to determine how many locations to view per page of 'Manage Locations' page
	if ($numMembers2!=0) {include(SLS_WP_INCLUDES_PATH."/search-links.php");}


print "</td></tr></table>";

print "<form name='locationForm' method='post' enctype='multipart/form-data'>";

if(empty($_GET['d'])) {$_GET['d']="";} if(empty($_GET['o'])) {$_GET['o']="";}


$master_check = (!empty($master_check))? $master_check : "" ;
include(SLS_WP_INCLUDES_PATH."/states-management.php");
print "<table class='widefat' cellspacing=0 id='loc_table'>
<thead><tr>
<th colspan='1'><input type='checkbox' onclick='checkAll(this,document.forms[\"locationForm\"])' id='master_checkbox' $master_check></th>

<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_id&d=$d'>".__("ID", SLS_WP_TEXT_DOMAIN)."</a></th>";

if (function_exists("do_sls_wp_hook") && !empty($sls_wp_columns)){
	do_sls_wp_location_table_header();
} else {
	
	$th_co = ($is_normal_view)? "</th>\n<th>" : ", " ;
	$th_style = ($is_normal_view)? "" : "style='white-space: nowrap;' " ;
	
	print "<th {$th_style}><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_state&d=$d'>".__("Category Name", SLS_WP_TEXT_DOMAIN)."</a>{$th_co}</th>";

	if ($sls_wp_vars['location_table_view']!="Normal") {
		print "<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_description&d=$d'>".__("Description", SLS_WP_TEXT_DOMAIN)."</a></th>
<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_url&d=$d'>".__("URL", SLS_WP_TEXT_DOMAIN)."</a></th>
<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_ext_url&d=$d'>".__("External YRL", SLS_WP_TEXT_DOMAIN)."</a></th>
<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_hours&d=$d'>".__("Hours", SLS_WP_TEXT_DOMAIN)."</th>
<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_phone&d=$d'>".__("Phone", SLS_WP_TEXT_DOMAIN)."</a></th>
<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_fax&d=$d'>".__("Fax", SLS_WP_TEXT_DOMAIN)."</a></th>
<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_email&d=$d'>".__("Email", SLS_WP_TEXT_DOMAIN)."</a></th>
<th><a href='".str_replace("&o=$_GET[o]&d=$_GET[d]", "", $_SERVER['REQUEST_URI'])."&o=sls_wp_image&d=$d'>".__("Image", SLS_WP_TEXT_DOMAIN)."</a></th>";
	}
}

print "
<th colspan='1'>".__("Actions", SLS_WP_TEXT_DOMAIN)."</th>
</tr></thead>";
	$o=esc_sql($o); $d=esc_sql($d); 
	$start=esc_sql($start); $num_per_page=esc_sql($num_per_page); 
	if ($locales=$wpdb->get_results("SELECT * FROM ".SLS_WP_TABLE_CATEGORY." $wherecat ORDER BY $o $d LIMIT $start, $num_per_page", ARRAY_A)) { 
		if (function_exists("do_sls_wp_hook") && !empty($sls_wp_columns)){
			
			$colspan=($sls_wp_vars['location_table_view']!="Normal")? 	(count($sls_wp_columns)-count($sls_wp_omitted_columns)+4) : (count($sls_wp_normal_columns)-3+4);
		} else {
			$colspan=($sls_wp_vars['location_table_view']!="Normal")? 	18 : 11;
		}
		
		$bgcol="";
		foreach ($locales as $value) {
			$bgcol=($bgcol==="" || $bgcol=="#eee")?"#fff":"#eee";			
			$value=array_map("trim",$value);
			
			if (!empty($_GET['edit']) && $value['sls_wp_id']==$_GET['edit']) {
				sls_wp_single_state_info($value, $colspan, $bgcol);
			}
			else {
			$value['sls_wp_url'] = '';
			$value['sls_wp_ext_url'] = '';
			
				$value['sls_wp_url']=(!sls_url_test($value['sls_wp_url']) && trim($value['sls_wp_url'])!="")? "http://".$value['sls_wp_url'] : $value['sls_wp_url'] ;
				$value['sls_wp_url']=($value['sls_wp_url']!="")? "<a href='$value[sls_wp_url]' target='blank'>".__("View", SLS_WP_TEXT_DOMAIN)."</a>" : "" ;
				
				$value['sls_wp_ext_url']=(!sls_url_test($value['sls_wp_ext_url']) && trim($value['sls_wp_ext_url'])!="")? "http://".$value['sls_wp_ext_url'] : $value['sls_wp_ext_url'] ;
				$value['sls_wp_ext_url']=($value['sls_wp_ext_url']!="")? "<a href='$value[sls_wp_ext_url]' target='blank'>".__("View", SLS_WP_TEXT_DOMAIN)."</a>" : "" ;
			
			
				if(empty($_GET['edit'])) {$_GET['edit']="";}
				$edit_link = str_replace("&edit=$_GET[edit]", "",$_SERVER['REQUEST_URI'])."&edit=" . $value['sls_wp_id'] ."#a$value[sls_wp_id]'";
				
				print "<tr style='background-color:$bgcol' id='sls_wp_tr-$value[sls_wp_id]'>
			<th><input type='checkbox' name='sls_wp_id[]' value='$value[sls_wp_id]'></th>
			
			<td> $value[sls_wp_id] </td>";

				if (function_exists("do_sls_wp_hook") && !empty($sls_wp_columns)){
					do_sls_wp_location_table_body($value);
				} else {
					if ($is_normal_view) {
						
						$tco_address = $tco_address2 = $tco_city = $tco_state = $tco_zip = "</td>\n<td>";
						$strong_addr_open = $strong_addr_close = "";
					} else {
						$tco_address = (!empty($value['sls_wp_state']) && !empty($value['sls_wp_state']))? "<br>" : "" ;
						$strong_addr_open = "<strong>"; $strong_addr_close = "</strong>";
					}
					
					print "<td> $value[sls_wp_state]{$tco_address}";

					if ($sls_wp_vars['location_table_view']!="Normal") {
						print "<td>$value[sls_wp_state]</td>";
					}
				}

				print "<td><a class='edit_loc_link' href='".$edit_link." id='$value[sls_wp_id]'><span class='fa fa-pencil'>&nbsp;</span>".__("Edit", SLS_WP_TEXT_DOMAIN)."</a>&nbsp;| <a class='del_loc_link' href='".wp_nonce_url("$_SERVER[REQUEST_URI]&delete=$value[sls_wp_id]", "delete-location_".$value['sls_wp_id'])."' onclick=\"confirmClick('Are you sure you wish to delete this State?', this.href); return false;\" id='$value[sls_wp_id]'><span class='fa fa-trash'>&nbsp;</span>".__("Delete", SLS_WP_TEXT_DOMAIN)."</a></td>
				</tr>";
			}
		}
	} else {
		$cleared=(!empty($_GET['q']))? str_replace("q=".str_replace(" ", "+", $_GET['q']) , "", $_SERVER['REQUEST_URI']) : $_SERVER['REQUEST_URI'] ;
		$notice=(!empty($_GET['q']))? __("No results for this Search of ", SLS_WP_TEXT_DOMAIN)."<b>\"$_GET[q]\"</b> | <a href='$cleared'>".__("Clear&nbsp;Results", SLS_WP_TEXT_DOMAIN)."</a> $view_link" : __("You have no available Categories", SLS_WP_TEXT_DOMAIN);
		print "<tr><td colspan='5'>$notice | <a href='".SLS_WP_ADD_STATES_PAGE."'>".__("Add a Category", SLS_WP_TEXT_DOMAIN)."</a></td></tr>";
	}
	print "</table>
	<input name='act' type='hidden'><br>";
	wp_nonce_field("manage-locations_bulk");

if ($numMembers2!=0) {include(SLS_WP_INCLUDES_PATH."/search-links.php");}

print "</form>"; 
?>
</div>
<?php include(SLS_WP_INCLUDES_PATH."/sls-wp-footer.php"); ?>
