<?php 
function xml_out($buff) {
	preg_match("@<logoshowcase>.*<\/logoshowcase>@s", $buff, $the_xml);
	return $the_xml[0];
}
if (empty($_GET['debug'])) {
	ob_start("xml_out");
}
header("Content-type: text/xml");
include("sls-wp-inc/includes/sls-wp-env.php");

$sls_wp_ap_xml = array("sls_wp_custom_fields", "sls_wp_xml_columns");
foreach ($sls_wp_ap_xml as $value){ if (!empty($_GET[$value])){ unset($_GET[$value]); } }

$sls_wp_custom_fields = (!empty($sls_wp_xml_columns))? ", ".implode(", ", $sls_wp_xml_columns) : "" ;

if (!empty($_GET)) { $_sl = $_GET; unset($_GET['mode']); unset($_GET['lat']); unset($_GET["lng"]); unset($_GET["radius"]); unset($_GET["edit"]);}
$_GET=array_filter($_GET);

$sls_wp_param_where_clause="";
if (function_exists("do_sls_wp_hook")){ do_sls_wp_hook("sls_wp_xml_query"); }

global $wpdb;
$query=$wpdb->get_results("SELECT * FROM ".SLS_WP_TABLE." WHERE sls_wp_is_published!=0 ", ARRAY_A);

// Xml header
echo '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
echo "<logoshowcase>\n";
echo '<top xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">';


foreach ($query as $row){
  // Xml nodes 
  echo '<logo>';
  echo '<name>' . slsParseToXML($row['sls_wp_logo']) . '</name>';
  echo '<id>'  .slsParseToXML($row['sls_pos']).'</id>';
  
  
  // logo img
	$sls_wp_uploads=wp_upload_dir();
	$sls_wp_uploads_path=$sls_wp_uploads['basedir']."/sls-wp-uploads"; 
	$upload_dir=$sls_wp_uploads_path."/images/".$row['sls_wp_id'].'/';
	$upload_dir_banner=$sls_wp_uploads_path."/images/icons/".$row['sls_wp_id'].'/';
	$sls_wp_uploads_base=$sls_wp_uploads['baseurl']."/sls-wp-uploads/images/";
	$sls_wp_uploads_base_banner=$sls_wp_uploads['baseurl']."/sls-wp-uploads/images/icons/";
	
	if(file_exists($upload_dir."ori_".$row['sls_wp_id'].".png")) {
		$img=$sls_wp_uploads_base.$row['sls_wp_id'].'/ori_'.$row['sls_wp_id'].'.png?t='.time();
	}
	else{
		$img=SLS_WP_BASE.'/images/default.png';
	}
	//banner images check 
	 if(file_exists($upload_dir_banner.$row['sls_wp_id'].".png")) {
		
		$banner=$sls_wp_uploads_base_banner.$row['sls_wp_id'].'/'.$row['sls_wp_id'].'.png?t='.time();
	}
	else{
		$banner='';
	}
  $catgeogy=(trim($row['sls_wp_state'])!="")? slsParseToXML($row['sls_wp_state']) : "default";
  $catgeogy=str_replace("&amp;","&",$catgeogy);
  echo '<imgid>'  .slsParseToXML($img).'</imgid>';
  echo '<banner>'  .slsParseToXML($banner).'</banner>';
  echo '<externalurl>'  .slsParseToXML($row['sls_wp_ext_url']).'</externalurl>';
  echo '<defaultmedia>'  .slsParseToXML($row['sls_wp_default_media']).'</defaultmedia>';
  echo '<popup>'.slsParseToXML($row['sls_wp_pop_up']).'</popup>';
  echo '<category>' .$catgeogy. '</category>';
  echo '<region>Worldwide</region>';
  $row['sls_wp_features']=(trim($row['sls_wp_features'])=="&lt;br&gt;" || trim($row['sls_wp_features'])=="")? "" : $row['sls_wp_features'];
  if($row['sls_wp_features']!='' && $row['sls_wp_features']!='&lt;br&gt;'){
  echo '<features>' . str_replace("&nbsp;","",$row['sls_wp_features']) .'</features>';
  }
  $row['sls_wp_intro']=(trim($row['sls_wp_intro'])=="&lt;br&gt;" || trim($row['sls_wp_intro'])=="")? "" : $row['sls_wp_intro'];
  if($row['sls_wp_intro']!='' && $row['sls_wp_intro']!='&lt;br&gt;'){
  echo '<info>' .str_replace("&nbsp;","",$row['sls_wp_intro']).'</info>';
  }
   $row['sls_wp_add_intro']=(trim($row['sls_wp_add_intro'])=="&lt;br&gt;" || trim($row['sls_wp_add_intro'])=="")? "" : $row['sls_wp_add_intro'];
  if($row['sls_wp_add_intro']!='' && $row['sls_wp_add_intro']!='&lt;br&gt;'){
  echo '<additionalInfo>' .str_replace("&nbsp;","",$row['sls_wp_add_intro']).'</additionalInfo>';
 }
  echo "</logo>\n";
}

// End XML file
echo "</top>\n
</logoshowcase>\n";
if (empty($_GET['debug'])) {
	ob_end_flush();
}


?>