<?php
function sls_wp_move_upload_directories() {

	global $sls_wp_uploads_path, $sls_wp_path;

	$sls_wp_uploads_arr=wp_upload_dir();

	if (!is_dir($sls_wp_uploads_arr['baseurl'])) {

		mkdir($sls_wp_uploads_arr['baseurl'], 0755, true);

	}

	if (!is_dir(SLS_WP_UPLOADS_PATH)) {

		mkdir(SLS_WP_UPLOADS_PATH, 0755, true);

	}

	if (is_dir(SLS_WP_ADDONS_PATH_ORIGINAL) && !is_dir(SLS_WP_ADDONS_PATH)) {

		sls_wp_copyr(SLS_WP_ADDONS_PATH_ORIGINAL, SLS_WP_ADDONS_PATH);

		chmod(SLS_WP_ADDONS_PATH, 0755);

	}

	if (is_dir(SLS_WP_THEMES_PATH_ORIGINAL) && !is_dir(SLS_WP_THEMES_PATH)) {

		sls_wp_copyr(SLS_WP_THEMES_PATH_ORIGINAL, SLS_WP_THEMES_PATH);

		chmod(SLS_WP_THEMES_PATH, 0755);

	}

	if (is_dir(SLS_WP_LANGUAGES_PATH_ORIGINAL) && !is_dir(SLS_WP_LANGUAGES_PATH)) {

		sls_wp_copyr(SLS_WP_LANGUAGES_PATH_ORIGINAL, SLS_WP_LANGUAGES_PATH);

		chmod(SLS_WP_LANGUAGES_PATH, 0755);

	}

	if (is_dir(SLS_WP_IMAGES_PATH_ORIGINAL) && !is_dir(SLS_WP_IMAGES_PATH)) {

		sls_wp_copyr(SLS_WP_IMAGES_PATH_ORIGINAL, SLS_WP_IMAGES_PATH);

		chmod(SLS_WP_IMAGES_PATH, 0755);

	}

	if (!is_dir(SLS_WP_CUSTOM_CSS_PATH)) {

		mkdir(SLS_WP_CUSTOM_CSS_PATH, 0755, true);

	}

	if (!is_dir(SLS_WP_CACHE_PATH)) {

	      mkdir(SLS_WP_CACHE_PATH, 0755, true);

	}

	sls_wp_ht(SLS_WP_CACHE_PATH, 'ht');

	sls_wp_ht(SLS_WP_ADDONS_PATH);

	sls_wp_ht(SLS_WP_UPLOADS_PATH);

}

function sls_wp_ht($path, $type='index'){

	if(is_dir($path) && !is_file($path."/.htaccess") && !is_file($path."/index.php")) {

		if ($type == 'ht') {

$htaccess = <<<EOQ

<FilesMatch "\.(php|gif|jpe?g|png|css|js|csv|xml|json)$">

Allow from all

</FilesMatch>

order deny,allow

deny from all

allow from none

Options All -Indexes

EOQ;

			$filename = $path."/.htaccess";

			$file_handle = @ fopen($filename, 'w+');

			@fwrite($file_handle, $htaccess);

			@fclose($file_handle);

			@chmod($file_handle, 0644);

		} elseif ($type == 'index') {

			$index ='<?php /*empty; prevents directory browsing*/ ?>';

			$filename = $path."/index.php";

			$file_handle = @ fopen($filename, 'w+');

			@fwrite($file_handle, $index);

			@fclose($file_handle);

			@chmod($file_handle, 0644);

		}	

	} elseif (is_dir($path) && is_file($path."/.htaccess") && $type == 'index') {

		@unlink($path."/.htaccess");

		$index ='<?php /*empty; prevents directory browsing*/ ?>';

		$filename = $path."/index.php";

		$file_handle = @ fopen($filename, 'w+');

		@fwrite($file_handle, $index);

		@fclose($file_handle);

		@chmod($file_handle, 0644);		

	}

}

/* -----------------*/

function slsParseToXML($htmlStr) 

{ 

$xmlStr=str_replace('<','&lt;',$htmlStr); 

$xmlStr=str_replace('>','&gt;',$xmlStr); 

$xmlStr=str_replace('"','&quot;',$xmlStr); 

$xmlStr=str_replace("'",'&#39;',$xmlStr); 

$xmlStr=str_replace("&",'&amp;',$xmlStr); 

$xmlStr=str_replace("," ,"&#44;" ,$xmlStr);

return $xmlStr; 

} 

/*-----------------*/

function filter_sls_wp_mdo($the_arr) {

	$input_zone_clause = ($the_arr['input_zone'] == $GLOBALS['input_zone_type']);

	$output_zone_clause = ( !isset($the_arr['output_zone']) || !isset($GLOBALS['output_zone_type']) || ($the_arr['output_zone'] == $GLOBALS['output_zone_type']) );

	return ($input_zone_clause && $output_zone_clause);

}



function sls_wp_md_initialize() {

	global $sls_wp_vars;

	include(SLS_WP_INCLUDES_PATH."/settings-options.php");

	foreach ($sls_wp_mdo as $value) {
		
		if (isset($value['field_name']) && !is_array($value['field_name']) ) {

			$value['default'] = (!isset($value['default']))? "" : $value['default'];

			$default_not_set = !isset($sls_wp_vars[$value['field_name']]);

			$default_set_but_value_set_to_blank = (isset($sls_wp_vars[$value['field_name']]) && strlen(trim($sls_wp_vars[$value['field_name']])) == 0);

			if ( ($default_not_set || $default_set_but_value_set_to_blank) ) {

				$sls_wp_vars[$value['field_name']] = $value['default'];

			} 

			$varname = "sls_wp_".$value['field_name'];  

			global $varname;

			$varname = $sls_wp_vars[$value['field_name']]; 

		} elseif (isset($value['field_name']) && is_array($value['field_name']) ) {

			$value['default'] = (!isset($value['default']))? array_fill(0, count($value['field_name']), "") : $value['default'];

			$ctr = 0;	

			foreach ($value['default'] as $the_default) {

				$the_field = $value['field_name'][$ctr];

				$d_n_s = !isset($sls_wp_vars[$the_field]);

				$d_s_b_v_s_t_b = (isset($sls_wp_vars[$the_field]) && strlen(trim($sls_wp_vars[$the_field])) == 0);
		
				if ( ($d_n_s || $d_s_b_v_s_t_b) ) {

					$sls_wp_vars[$the_field] = $the_default;

				}

				$varname = "sls_wp_".$the_field;  

				global $varname;

				$varname = $sls_wp_vars[$the_field];

				$ctr++;

			} 
		    
		}

	}

}

/*-----------------*/

function sls_wp_initialize_variables() {

global $sls_wp_height, $sls_wp_width, $sls_wp_width_units, $sls_wp_height_units, $sls_wp_radii;

global $sls_wp_icon, $sls_wp_icon2, $sls_wp_google_map_domain, $sls_wp_google_map_country, $sls_wp_theme, $sls_wp_base, $sls_wp_uploads_base, $sls_wp_location_table_view;

global $sls_wp_search_label, $sls_wp_zoom_level, $sls_wp_use_city_search, $sls_wp_use_name_search, $sls_wp_name;

global $sls_wp_radius_label, $sls_wp_website_label, $sls_wp_directions_label, $sls_wp_num_initial_displayed, $sls_wp_load_locations_default;

global $sls_wp_distance_unit, $sls_wp_map_overview_control, $sls_wp_admin_locations_per_page, $sls_wp_instruction_message;

global $sls_wp_map_character_encoding, $sls_wp_start, $sls_wp_map_language, $sls_wp_map_region, $sls_wp_sensor, $sls_wp_geolocate;

global $sls_wp_map_settings, $sls_wp_remove_credits, $sls_wp_api_key, $sls_wp_location_not_found_message, $sls_wp_no_results_found_message; 

global $sls_wp_load_results_with_locations_default, $sls_wp_vars, $sls_wp_city_dropdown_label, $sls_wp_scripts_load, $sls_wp_scripts_load_home, $sls_wp_scripts_load_archives_404;

global $sls_wp_hours_label, $sls_wp_phone_label, $sls_wp_fax_label, $sls_wp_email_label;

$sls_wp_vars=sls_wp_data('sls_wp_vars'); 

sls_wp_md_initialize();
  if(!isset($sls_wp_vars['sls_user_role']) || !isset($sls_wp_vars['admin_locations_per_page']))
	{
	$sls_wp_vars['sensor']="false";
	$sls_wp_vars['geolocate']="";
	$sls_wp_vars['to_top']="1";
    $sls_wp_vars['api_key']="";
	$sls_wp_vars['google_map_country']= "United States";
	$sls_wp_vars['google_map_domain']="maps.google.com";
	$sls_wp_vars['map_region']="";
	$sls_wp_vars['map_language']= "en";
	$sls_wp_vars['map_character_encoding']="";
	$sls_wp_vars['start']=date("Y-m-d H:i:s");
	$sls_wp_vars['name']="Super Store Finder";
	$sls_wp_vars['admin_locations_per_page']="25";
	$sls_wp_vars['location_table_view']="Normal";
	}
	else{
	if (strlen(trim($sls_wp_vars['sensor'])) == 0) {	$sls_wp_vars['sensor'] = ($sls_wp_vars['geolocate'] == '1')? "true" : "false";	}
	$sls_wp_sensor=$sls_wp_vars['sensor'];
	if ($sls_wp_vars['api_key'] === NULL) {	$sls_wp_vars['api_key']="";	}
	$sls_wp_api_key=$sls_wp_vars['api_key'];
	if (strlen(trim($sls_wp_vars['google_map_country'])) == 0) {	$sls_wp_vars['google_map_country']="United States";}
	$sls_wp_google_map_country=$sls_wp_vars['google_map_country'];
	if (strlen(trim($sls_wp_vars['google_map_domain'])) == 0) {	$sls_wp_vars['google_map_domain']="maps.google.com";}
	$sls_wp_google_map_domain=$sls_wp_vars['google_map_domain'];
	if ($sls_wp_vars['map_region'] === NULL) {	$sls_wp_vars['map_region']="";	}
	$sls_wp_map_region=$sls_wp_vars['map_region'];
	if (strlen(trim($sls_wp_vars['map_language'])) == 0) {	$sls_wp_vars['map_language']="en";	}
	$sls_wp_map_language=$sls_wp_vars['map_language'];
	if ($sls_wp_vars['map_character_encoding'] === NULL) {	$sls_wp_vars['map_character_encoding']="";		}
	$sls_wp_map_character_encoding=$sls_wp_vars['map_character_encoding'];
	if (strlen(trim($sls_wp_vars['start'])) == 0) { 	$sls_wp_vars['start']=date("Y-m-d H:i:s"); 	} 
	$sls_wp_start=$sls_wp_vars['start']; 
	if (strlen(trim($sls_wp_vars['name'])) == 0) {	$sls_wp_vars['name']="Super Logos Showcase";	}  
	$sls_wp_name=$sls_wp_vars['name'];
	if (strlen(trim($sls_wp_vars['admin_locations_per_page'])) == 0) {	$sls_wp_vars['admin_locations_per_page']="100";	}
	$sls_wp_admin_locations_per_page=$sls_wp_vars['admin_locations_per_page'];
	if (strlen(trim($sls_wp_vars['location_table_view'])) == 0) {	$sls_wp_vars['location_table_view']="Normal";	}
	$sls_wp_location_table_view=$sls_wp_vars['location_table_view'];
	}

	sls_wp_data('sls_wp_vars', 'add', $sls_wp_vars);

}

/*--------------------------*/

function sls_wp_choose_units($unit, $input_name) {

	$unit_arr[]="%";$unit_arr[]="px";$unit_arr[]="em";$unit_arr[]="pt";

	$select_field="<select name='$input_name'>";

	foreach ($unit_arr as $value) {

		$selected=($value=="$unit")? " selected='selected' " : "" ;

		if (!($input_name=="height_units" && $unit=="%")) {

			$select_field.="\n<option value='$value' $selected>$value</option>";

		}

	}

	$select_field.="</select>";

	return $select_field;

}

/*----------------------------*/

function sls_wp_install_tables() {

	global $wpdb, $sls_wp_db_version, $sls_wp_path, $sls_wp_uploads_path, $sls_wp_hook;

	if (!defined("SLS_WP_TABLE") || !defined("SLS_WP_TAG_TABLE") || !defined("SLS_WP_SETTING_TABLE") || !defined("SLS_WP_TABLE_CATEGORY")){ 
		$sls_wp_db_prefix = $wpdb->prefix; 
	}

	if (!defined("SLS_WP_TABLE")){ define("SLS_WP_TABLE", $sls_wp_db_prefix."sls_wp_logos");}
	if (!defined("SLS_WP_TAG_TABLE")){ define("SLS_WP_TAG_TABLE", $sls_wp_db_prefix."sls_wp_tag"); }
	if (!defined("SLS_WP_SETTING_TABLE")){ define("SLS_WP_SETTING_TABLE", $sls_wp_db_prefix."sls_wp_setting"); }
	if (!defined("SLS_WP_TABLE_CATEGORY")){ define("SLS_WP_TABLE_CATEGORY", $sls_wp_db_prefix."sls_wp_category"); } 

	$table_name = SLS_WP_TABLE;
	$sql = "CREATE TABLE " . $table_name . " (
			sls_wp_id mediumint(8) unsigned NOT NULL auto_increment,
			sls_wp_logo varchar(255) NULL,
			sls_wp_tags mediumtext NULL,
			sls_wp_intro text(1000) NULL,
			sls_wp_features text(1000) NULL,
			sls_wp_nearby text(1000) NULL,
			sls_wp_add_intro text(1000) NULL,
			sls_wp_state varchar(255) NULL,
			sls_wp_image varchar(255) NULL,
			sls_wp_is_published varchar(1) NULL,
			sls_pos int(11),
			sls_wp_ext_url varchar(255) NULL,
			sls_wp_pop_up varchar(255) NULL,
			sls_wp_default_media varchar(255) NULL,
			sls_wp_bg_color varchar(255) NULL,
			sls_wp_active_color varchar(255) NULL,
			PRIMARY KEY  (sls_wp_id)
			) ENGINE=innoDB  DEFAULT CHARACTER SET=utf8  DEFAULT COLLATE=utf8_unicode_ci;";

	$table_name_2 = SLS_WP_TAG_TABLE;
	$sql .= "CREATE TABLE " . $table_name_2 . " (
			sls_wp_tag_id bigint(20) unsigned NOT NULL auto_increment,
			sls_wp_tag_name varchar(255) NULL,
			sls_wp_tag_slug varchar(255) NULL,
			sls_wp_id mediumint(8) NULL,
			PRIMARY KEY  (sls_wp_tag_id)
			) ENGINE=innoDB  DEFAULT CHARACTER SET=utf8  DEFAULT COLLATE=utf8_unicode_ci;";

	$table_name_3 = SLS_WP_SETTING_TABLE;
	$sql .= "CREATE TABLE " . $table_name_3 . " (
			sls_wp_setting_id bigint(20) unsigned NOT NULL auto_increment,
			sls_wp_setting_name varchar(255) NULL,
			sls_wp_setting_value longtext NULL,
			PRIMARY KEY  (sls_wp_setting_id)
			) ENGINE=innoDB  DEFAULT CHARACTER SET=utf8  DEFAULT COLLATE=utf8_unicode_ci;";

	
	$table_name_4 = SLS_WP_TABLE_CATEGORY;

	$sql .= "CREATE TABLE " . $table_name_4 . " (
			sls_wp_id mediumint(8) unsigned NOT NULL auto_increment,
			sls_wp_state varchar(255) NULL,
			cat_icon varchar(255) NULL,
			cat_parent_id mediumint(8) NULL,
			cat_free_flag mediumint(8) NULL,
			PRIMARY KEY  (sls_wp_id)
			) ENGINE=innoDB  DEFAULT CHARACTER SET=utf8  DEFAULT COLLATE=utf8_unicode_ci;";
			
			

	if($wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", $table_name)) != $table_name || $wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", $table_name_2)) != $table_name_2 || $wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", $table_name_3)) != $table_name_3  || $wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", $table_name_4)) != $table_name_4) {
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		sls_wp_data("sls_wp_db_version", 'add', $sls_wp_db_version);
		sls_wp_initialize_variables();

	}

	$installed_ver = sls_wp_data("sls_wp_db_version");
	if( $installed_ver != $sls_wp_db_version ) {
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
		sls_wp_data("sls_wp_db_version", 'update', $sls_wp_db_version);
	}

	if (sls_wp_data("sls_wp_db_prefix")===""){
		sls_wp_data('sls_wp_db_prefix', 'update', $sls_wp_db_prefix);
	}
	sls_wp_move_upload_directories();
}

/*-------------------------------*/

function sls_wp_head_scripts() {

	global $sls_wp_dir, $sls_wp_base, $sls_wp_uploads_base, $sls_wp_path, $sls_wp_uploads_path, $wpdb, $pagename, $sls_wp_map_language, $post, $sls_wp_vars; 		

	$on_sls_wp_page=""; $sls_wp_code_is_used_in_posts=""; $post_ids_array="";

	if (empty($sls_wp_vars['scripts_load']) || $sls_wp_vars['scripts_load'] != 'all') {

		if (empty($_GET['p'])){ $_GET['p']=""; } if (empty($_GET['page_id'])){ $_GET['page_id']=""; }

		$on_sls_wp_page=$wpdb->get_results("SELECT post_name, post_content FROM ".SLS_WP_DB_PREFIX."posts WHERE LOWER(post_content) LIKE '%[super-logo-showcase%' AND (post_name='$pagename' OR ID='".esc_sql($_GET['p'])."' OR ID='".esc_sql($_GET['page_id'])."')", ARRAY_A);		

		$sls_wp_code_is_used_in_posts=$wpdb->get_results("SELECT post_name, ID FROM ".SLS_WP_DB_PREFIX."posts WHERE LOWER(post_content) LIKE '%[super-logo-showcase%' AND post_type='post'", ARRAY_A);

		if ($sls_wp_code_is_used_in_posts) {

			$sls_wp_post_ids=$sls_wp_code_is_used_in_posts;

			foreach ($sls_wp_post_ids as $val) { $post_ids_array[]=$val['ID'];}

		} else {		

			$post_ids_array=array(pow(10,15)); 

		}
	}

	$show_on_all_pages = ( !empty($sls_wp_vars['scripts_load']) && $sls_wp_vars['scripts_load'] == 'all' );

	$show_on_front_page = ( is_front_page() && (!isset($sls_wp_vars['scripts_load_home']) || $sls_wp_vars['scripts_load_home']==1) );

	$show_on_archive_404_pages = ( (is_archive() || is_404()) && $sls_wp_code_is_used_in_posts && (!isset($sls_wp_vars['scripts_load_archives_404']) || $sls_wp_vars['scripts_load_archives_404']==1) );

	$show_on_custom_post_types = ( is_singular() && !is_singular(array('page', 'attachment', 'post')) && !is_front_page() );

	//$show_on_page_templates = ( is_page_template() && !is_front_page() );

	$on_sls_wp_post = is_single($post_ids_array);

//|| $show_on_custom_post_types
	//if (is_page() || is_single()) {

	if ($show_on_all_pages || $on_sls_wp_page  || $show_on_archive_404_pages || $show_on_front_page  || $on_sls_wp_post  || function_exists('show_sls_wp_scripts')) {

		$GLOBALS['is_on_sls_wp_page'] = 1;

		$google_map_domain=($sls_wp_vars['google_map_domain']!="")? $sls_wp_vars['google_map_domain'] : "maps.google.com";

		//  || is_search()



		$sens=(!empty($sls_wp_vars['sensor']) && ($sls_wp_vars['sensor'] === "true" || $sls_wp_vars['sensor'] === "false" ))? "&amp;sensor=".$sls_wp_vars['sensor'] : "&amp;sensor=false" ;

		$lang_loc=(!empty($sls_wp_vars['map_language']))? "&amp;language=".$sls_wp_vars['map_language'] : "" ; 

		$region_loc=(!empty($sls_wp_vars['map_region']))? "&amp;region=".$sls_wp_vars['map_region'] : "" ;

		$key=(!empty($sls_wp_vars['api_key']))? "&amp;key=".$sls_wp_vars['api_key'] : "" ;



		if (empty($_POST) && 1==2) { 

			$nm=(!empty($post->post_name))? $post->post_name : $pagename ;

			$p=(!empty($post->ID))? $post->ID : esc_sql($_GET['p']) ;

			
		} else {

			sls_wp_dyn_js($on_sls_wp_page);

		}

		$has_custom_css=(file_exists(SLS_WP_CUSTOM_CSS_PATH."/super-logo-showcase.css"))? SLS_WP_CUSTOM_CSS_BASE : SLS_WP_CSS_BASE; 

		//mega locator

		wp_enqueue_style( 'mega-font-awesome' , $has_custom_css.'/font-awesome.css' , true , '4.1' );

		wp_enqueue_style( 'mega-normalize' , $has_custom_css.'/normalize.css' , true , '2.0' );

		wp_enqueue_style( 'super-logo-showcase' , $has_custom_css.'/super-logo-showcase.css' , true , '1.0' );

		wp_enqueue_script( 'mega-modernize' , SLS_WP_JS_BASE.'/vendors/modernizr.min.js' , array( 'jquery' ) , '1.0' , true );

		wp_enqueue_script( 'mega-polyfills' , SLS_WP_JS_BASE.'/polyfills/html5shiv.3.7.0.min.js' , array( 'jquery' ) , '3.7' , true );

		wp_enqueue_script( 'mega-homebrew' , SLS_WP_JS_BASE.'/plugins/homebrew.js' , array( 'jquery' ) , '1.0' , true );

		wp_enqueue_script( 'mega-fastclicks' , SLS_WP_JS_BASE.'/plugins/fastclick.min.js' , array( 'jquery' ) , '3.0' , true );

		wp_enqueue_script( 'mega-logo-init' , SLS_WP_JS_BASE.'/init.js' , array( 'jquery' ) , '1.0' , true );

		wp_enqueue_script( 'super-logo-showcase' , SLS_WP_JS_BASE.'/super-logo-showcase.js' , array( 'jquery' ) , '1.0' , true );

		// mega locator

		if (function_exists("do_sls_wp_hook")){do_sls_wp_hook('sls_wp_addon_head_styles');}
		sls_wp_move_upload_directories();
	}

}

function sls_wp_footer_scripts(){

	if (!did_action('wp_head')){ sls_wp_head_scripts();} //if wp_head missing

}
add_action('wp_print_footer_scripts', 'sls_wp_footer_scripts');

function sls_wp_jq() {wp_enqueue_script( 'jquery');}

add_action('wp_enqueue_scripts', 'sls_wp_jq');

/*-----------------------------------*/

function sls_wp_add_options_page() {

	global $sls_wp_dir, $sls_wp_base, $sls_wp_uploads_base, $text_domain, $sls_wp_top_nav_links, $sls_wp_vars, $sls_wp_version;

	$parent_url = SLS_WP_PARENT_URL; 

	$warning_count = 0;

	$warning_title = __("Update(s) currently available for Store Locator", SLS_WP_TEXT_DOMAIN) . ":";

	$sls_wp_vars['sls_wp_latest_version_check_time'] = (empty($sls_wp_vars['sls_wp_latest_version_check_time']))? date("Y-m-d H:i:s") : $sls_wp_vars['sls_wp_latest_version_check_time'];

	if (empty($sls_wp_vars['sls_wp_latest_version']) || (time() - strtotime($sls_wp_vars['sls_wp_latest_version_check_time']))/60>=(60*12)){ //12-hr db caching of version info

		$sls_wp_latest_version = ''; 

		$sls_wp_vars['sls_wp_latest_version_check_time'] = date("Y-m-d H:i:s");

		$sls_wp_vars['sls_wp_latest_version'] = $sls_wp_latest_version;

	} else {

		$sls_wp_latest_version = $sls_wp_vars['sls_wp_latest_version'];

	}


	if (strnatcmp($sls_wp_latest_version, $sls_wp_version) > 0) { 

		$warning_title .= "\n- Super Logos Showcase v{$sls_wp_latest_version} " . __("is available, you are using", SLS_WP_TEXT_DOMAIN). " v{$sls_wp_version}";

		$warning_count++;


		$sls_wp_plugin = SLS_WP_DIR . "/super-logo-showcase.php";

		$sls_wp_update_link = admin_url('update.php?action=upgrade-plugin&plugin=' . $sls_wp_plugin);

		$sls_wp_update_link_nonce = wp_nonce_url($sls_wp_update_link, 'upgrade-plugin_' . $sls_wp_plugin);

		$sls_wp_update_msg = "&nbsp;&gt;&nbsp;<a href='$sls_wp_update_link_nonce' style='color:#900; font-weight:bold;' onclick='confirmClick(\"".__("You will now be updating to Super Logos Showcase", SLS_WP_TEXT_DOMAIN)." v$sls_wp_latest_version, ".__("click OK or Confirm to continue", SLS_WP_TEXT_DOMAIN).".\", this.href); return false;'>".__("Update to", SLS_WP_TEXT_DOMAIN)." $sls_wp_latest_version</a>";

	} else {

		$sls_wp_update_msg = "";

	}

  //   }	

	if ( defined("SLS_WP_ADDONS_PLATFORM_DIR") ) {

	   $sls_wp_vars['sls_wp_latest_ap_check_time'] = (empty($sls_wp_vars['sls_wp_latest_ap_check_time']))? date("Y-m-d H:i:s") : $sls_wp_vars['sls_wp_latest_ap_check_time'];

	   if ( (empty($sls_wp_vars['sls_wp_latest_ap_version']) || (time() - strtotime($sls_wp_vars['sls_wp_latest_ap_check_time']))/60>=(60*12)) ) { //12-hr db caching of version info

		$ap_update = sls_wp_remote_data(array(

			'pagetype' => 'ap',

			'dir' => SLS_WP_ADDONS_PLATFORM_DIR, 

			'key' => sls_wp_data('sls_wp_license_' . SLS_WP_ADDONS_PLATFORM_DIR)

		));

		$ap_latest_version = (!empty($ap_update[0]))? preg_replace("@\.zip|".SLS_WP_ADDONS_PLATFORM_DIR."\.@", "", $ap_update[0]['filename']) : 0;

		$sls_wp_vars['sls_wp_latest_ap_check_time'] = date("Y-m-d H:i:s");

		$sls_wp_vars['sls_wp_latest_ap_version'] = $ap_latest_version;

	   } else {

		$ap_latest_version = $sls_wp_vars['sls_wp_latest_ap_version'];

	   }

	   $ap_readme = SLS_WP_ADDONS_PLATFORM_PATH."/readme.txt"; 

	   if (file_exists($ap_readme)) {

		$rm_txt = file_get_contents($ap_readme);

		preg_match("/\n[ ]*stable tag:[ ]?([^\n]+)(\n)?/i", $rm_txt, $cv); 

		$ap_version = (!empty($cv[1]))? trim($cv[1]) : "1.0" ;

	   } else {$ap_version = "1.6";}

	   if (strnatcmp($ap_latest_version, $ap_version) > 0) {

		$ap_title = ucwords(str_replace("-", " ", SLS_WP_ADDONS_PLATFORM_DIR));

		$warning_title .= "\n- $ap_title v{$ap_latest_version} " . __("is available, you are using", SLS_WP_TEXT_DOMAIN). " v{$ap_version}";

		$warning_count++;

	   }

	} 
	

	$notify = ($warning_count > 0)?  " <span class='update-plugins count-$warning_count' title='$warning_title'><span class='update-count'>" . $warning_count . "</span></span>" : "" ;

	$sls_role='';
	function get_sls_current_user_role() {
		global $wp_roles;
		$current_user = wp_get_current_user();
		$roles = $current_user->roles;
		$role = array_shift($roles);
		return trim($role);
    }
	$sls_role=get_sls_current_user_role();
	if(!isset($sls_wp_vars['sls_user_role'])){
		$sls_wp_vars['sls_user_role']='administrator';
	}
    $userRole=(trim($sls_wp_vars['sls_user_role'])!="")? $sls_wp_vars['sls_user_role'] : "administrator";
	$ex_cat = explode(",", $userRole);
    $ex_cat = array_map( 'trim', $ex_cat );
  if(in_array($sls_role,$ex_cat) || $sls_role=='administrator'){

	$sls_wp_menu_pages['main'] = array('title' => __("Logos Showcase", SLS_WP_TEXT_DOMAIN)."$notify", 'capability' => $sls_role, 'page_url' =>  $parent_url, 'icon' => SLS_WP_BASE.'/images/logo.ico.png', 'menu_position' => 48);
	$sls_wp_menu_pages['sub']['information'] = array('parent_url' => $parent_url, 'title' => __("Quick Start", SLS_WP_TEXT_DOMAIN), 'capability' => $sls_role, 'page_url' => $parent_url);
	$sls_wp_menu_pages['sub']['locations'] = array('parent_url' => $parent_url, 'title' => __("Logos", SLS_WP_TEXT_DOMAIN), 'capability' => $sls_role, 'page_url' => SLS_WP_PAGES_DIR.'/logos.php');	
	 $sls_wp_menu_pages['sub']['state'] = array('parent_url' => $parent_url, 'title' => __("Category", SLS_WP_TEXT_DOMAIN), 'capability' => $sls_role, 'page_url' => SLS_WP_PAGES_DIR.'/states.php'); 
	$sls_wp_menu_pages['sub']['settings'] = array('parent_url' => $parent_url, 'title' => __("Settings", SLS_WP_TEXT_DOMAIN), 'capability' => $sls_role, 'page_url' => SLS_WP_PAGES_DIR.'/settings.php');
	sls_wp_menu_pages_filter($sls_wp_menu_pages);
	}
}

function sls_wp_menu_pages_filter($sls_wp_menu_pages) {

	if (function_exists('do_sls_wp_hook')){do_sls_wp_hook('sls_wp_menu_pages_filter', '', array(&$sls_wp_menu_pages));}

	

	foreach ($sls_wp_menu_pages as $menu_type => $value) {

		if ($menu_type == 'main') {

			add_menu_page ($value['title'], $value['title'], $value['capability'], $value['page_url'], '', $value['icon'], $value['menu_position']);

		}

		if ($menu_type == 'sub'){

			foreach ($value as $sub_value) {

				 add_submenu_page($sub_value['parent_url'], $sub_value['title'], $sub_value['title'], $sub_value['capability'], $sub_value['page_url']);

			}

		}

	}

}

function sls_wp_where_clause_filter(&$where){

	if (function_exists("do_sls_wp_hook")) {do_sls_wp_hook("sls_wp_where_clause_filter");}

}

/*---------------------------------------------------*/

function sls_wp_add_admin_javascript() {

        global $sls_wp_base, $sls_wp_uploads_base, $sls_wp_dir, $google_map_domain, $sls_wp_path, $sls_wp_uploads_path, $sls_wp_map_language, $sls_wp_vars;


		wp_enqueue_script('prettyPhoto', SLS_WP_JS_BASE."/jquery.prettyPhoto.js", "jQuery");
		wp_enqueue_script('sls_wp_func', SLS_WP_JS_BASE."/functions.js", "jQuery");
		wp_enqueue_script('nicEdit', SLS_WP_JS_BASE."/nicEdit.js", "jQuery" );
		wp_enqueue_script('slschosenJquery', SLS_WP_JS_BASE."/chosen/chosen.jquery.js", "jQuery" );
        wp_enqueue_script('slschosenProto', SLS_WP_JS_BASE."/chosen/chosen.proto.min.js", "jQuery" );
		
        print "<script type='text/javascript'>";

        $admin_js = "

        var sls_wp_dir='".$sls_wp_dir."';

        var sls_wp_google_map_country='".$sls_wp_vars['google_map_country']."';

        var sls_wp_base='".SLS_WP_BASE."';

        var sls_wp_path='".SLS_WP_PATH."';

		var default_distance = '';

		var init_zoom = '';

		var zoomhere_zoom = '';

		var geo_settings = '';

		var default_location = '';

		var style_map_color = '';

        var sls_wp_uploads_base='".SLS_WP_UPLOADS_BASE."';

        var sls_wp_uploads_path='".SLS_WP_UPLOADS_PATH."';

        var sls_wp_addons_base=sls_wp_uploads_base+'".str_replace(SLS_WP_UPLOADS_BASE, '', SLS_WP_ADDONS_BASE)."';

        var sls_wp_addons_path=sls_wp_uploads_path+'".str_replace(SLS_WP_UPLOADS_PATH, '', SLS_WP_ADDONS_PATH)."';

        var sls_wp_includes_base=sls_wp_base+'".str_replace(SLS_WP_BASE, '', SLS_WP_INCLUDES_BASE)."';

        var sls_wp_includes_path=sls_wp_path+'".str_replace(SLS_WP_PATH, '', SLS_WP_INCLUDES_PATH)."';

        var sls_wp_cache_path=sls_wp_uploads_path+'".str_replace(SLS_WP_UPLOADS_PATH, '', SLS_WP_CACHE_PATH)."';

        var sls_wp_pages_base=sls_wp_base+'".str_replace(SLS_WP_BASE, '', SLS_WP_PAGES_BASE)."'";

        print preg_replace("@[\\\]@", "\\\\\\", $admin_js); 

        print "</script>\n";

        if (preg_match("@add-logo\.php|locations\.php@", $_SERVER['REQUEST_URI'])) {

			if (!file_exists(SLS_WP_ADDONS_PATH."/point-click-add/point-click-add.js")) {

				$sens=(!empty($sls_wp_vars['sensor']))? "sensor=".$sls_wp_vars['sensor'] : "sensor=false" ;

				$lang_loc=(!empty($sls_wp_vars['map_language']))? "&amp;language=".$sls_wp_vars['map_language'] : "" ; 

				$region_loc=(!empty($sls_wp_vars['map_region']))? "&amp;region=".$sls_wp_vars['map_region'] : "" ;

				$key=(!empty($sls_wp_vars['api_key']))? "&amp;key=".$sls_wp_vars['api_key'] : "" ;


			}

            if (file_exists(SLS_WP_ADDONS_PATH."/point-click-add/point-click-add.js")) {

				$sens=(!empty($sls_wp_vars['sensor']))? "sensor=".$sls_wp_vars['sensor'] : "sensor=false" ;

				$char_enc='&amp;oe='.$sls_wp_vars['map_character_encoding'];

				$google_map_domain=(!empty($sls_wp_vars['google_map_domain']))? $sls_wp_vars['google_map_domain'] : "maps.google.com";

				$api=sls_wp_data('store_locator_api_key');

			}

        }

		if (function_exists('do_sls_wp_hook')){do_sls_wp_hook('sls_wp_addon_admin_scripts');}

}

function sls_wp_remove_conflicting_scripts(){

	if (preg_match("@".SLS_WP_DIR."@", $_SERVER['REQUEST_URI'])){

		wp_dequeue_script('ui-tabs'); 

	}

}

add_action('admin_enqueue_scripts', 'sls_wp_remove_conflicting_scripts');

add_action( 'admin_enqueue_scripts', 'sls_mw_enqueue_color_picker' );

function sls_mw_enqueue_color_picker( $hook_suffix ) {

    // first check that $hook_suffix is appropriate for your administrator's page

    wp_enqueue_style( 'wp-color-picker' );

    wp_enqueue_script( 'my-script-handle', plugins_url('js/docs.js', __FILE__ ), array( 'wp-color-picker' ), false, true );
	
	
}

function sls_wp_add_admin_stylesheet() {

  global $sls_wp_base;
		wp_enqueue_style( 'mega-font-awesome' , SLS_WP_CSS_BASE.'/font-awesome.css' , true , '4.1' );
		wp_enqueue_style( 'mega-sls-wp-admin' , SLS_WP_CSS_BASE.'/sls-wp-admin.css' , true , '1.0' );
		wp_enqueue_style( 'mega-sls-wp-font' , SLS_WP_CSS_BASE.'/sls-wp-pop.css' , true , '1.0' );
		wp_enqueue_style('mega-sls-wp-chosen' , SLS_WP_JS_BASE.'/chosen/chosen.min.css' , true , '1.0' );

}

/*---------------------------------*/

function sls_wp_sls_set_query_defaults() {

	global $where, $o, $d, $sls_wp_searchable_columns, $wpdb;

	$extra="";  

	if (function_exists("do_sls_wp_hook") && !empty($sls_wp_searchable_columns) && !empty($_GET['q'])) {

		foreach ($sls_wp_searchable_columns as $value) {

			$extra .= $wpdb->prepare(" OR $value LIKE '%%%s%%'", $_GET['q']);

		}

	}

	$where=(!empty($_GET['q']))? $wpdb->prepare(" WHERE sls_wp_logo LIKE '%%%s%%' OR sls_wp_state LIKE '%%%s%%'  OR sls_wp_tags LIKE '%%%s%%'", $_GET['q'], $_GET['q'], $_GET['q'])." ".$extra : "" ; //die($where);

	$o=(!empty($_GET['o']))? esc_sql($_GET['o']) : "sls_pos";

	$d=(empty($_GET['d']) || $_GET['d']=="DESC")? "ASC" : "DESC";

}

//for state page function
function sls_wp_state_query_defaults() {
	global $where, $o, $d, $sls_wp_searchable_columns, $wpdb;
	$extra="";  
	if (function_exists("do_sls_wp_hook") && !empty($sls_wp_searchable_columns) && !empty($_GET['q'])) {
		foreach ($sls_wp_searchable_columns as $value) {
			$extra .= $wpdb->prepare(" OR $value LIKE '%%%s%%'", $_GET['q']);
		}
	}
	
	$where=(!empty($_GET['q']))? $wpdb->prepare(" WHERE sls_wp_logo LIKE '%%%s%%' OR sls_wp_address LIKE '%%%s%%' OR sls_wp_city LIKE '%%%s%%' OR sls_wp_state LIKE '%%%s%%' OR sls_wp_zip LIKE '%%%s%%' OR sls_wp_tags LIKE '%%%s%%'", $_GET['q'], $_GET['q'], $_GET['q'], $_GET['q'], $_GET['q'], $_GET['q'])." ".$extra : "" ; //die($where);
	
	$o=(!empty($_GET['o']))? esc_sql($_GET['o']) : "sls_wp_state";
	$d=(empty($_GET['d']) || $_GET['d']=="DESC")? "ASC" : "DESC";
}
//state code end


function sls_set_query_defaults() {sls_wp_sls_set_query_defaults();}

/*--------------------------------------------------------------*/

function sls_do_hyperlink(&$text, $target="'_blank'", $type="both"){

  if ($type=="both" || $type=="protocol") {	

   $text = preg_replace("@[a-zA-Z]+://([.]?[a-zA-Z0-9_/?&amp;%20,=\-\+\-\#])+@s", "<a href=\"\\0\" target=$target>\\0</a>", $text);

  }

  if ($type=="both" || $type=="noprotocol") {

   $text = preg_replace("@(^| )(www([.]?[a-zA-Z0-9_/=-\+-\#])*)@s", "\\1<a href=\"http://\\2\" target=$target>\\2</a>", $text);

  }

  return $text;

}

/*-------------------------------------------------------------*/

function sls_comma($a) {

	$a=str_replace('"', "&quot;", $a);

	$a=str_replace("'", "&#39;", $a);

	$a=str_replace(">", "&gt;", $a);

	$a=str_replace("<", "&lt;", $a);

	$a=str_replace(" & ", " &amp; ", $a);

	return str_replace("," ,"&#44;" ,$a);

	

}

/*------------------------------------------------------------*/

if (!function_exists('addon_activation_message')) {

	function addon_activation_message($url_of_upgrade="") {

		global $sls_wp_dir, $text_domain;

		print "<div style='background-color:#eee; border:solid silver 1px; padding:7px; color:black; display:block;'>".__("You haven't activated this upgrade yet", SLS_WP_TEXT_DOMAIN).". ";

		if (function_exists('do_sls_wp_hook') && !preg_match("/addons\-platform/", $url_of_upgrade) ){

			print "<a href='".SLS_WP_ADDONS_PAGE."'>".__("Activate", SLS_WP_TEXT_DOMAIN)."</a></div><br>";

		} else {

			print __("Go to pull-out Dashboard, and activate under 'Activation Keys' section.", SLS_WP_TEXT_DOMAIN)."</div><br>";

		}

	}

}

/*-----------------------------------------------------------*/

function sls_url_test($url){

	if (preg_match("@^https?://@i", $url)) {

		return TRUE; 

	} else {

		return FALSE; 

	}

}

/*---------------------------------------------------------------*/

function sls_wp_neat_title($ttl,$separator="_") {

	$normalizeChars = array(

    'Š'=>'S', 'š'=>'s', 'Ð'=>'Dj','Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A',

    'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I',

    'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U',

    'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss','à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a',

    'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i',

    'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u',

    'ú'=>'u', 'û'=>'u', 'ü'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y', 'ƒ'=>'f', 

    'ă'=>'a', 'î'=>'i', 'â'=>'a', 'ș'=>'s', 'ț'=>'t', 'Ă'=>'A', 'Î'=>'I', 'Â'=>'A', 'Ș'=>'S', 'Ț'=>'T');

	$ttl = strtr($ttl, $normalizeChars);

	$ttl = html_entity_decode( str_replace("&#39;","'",$ttl) );

	

	$ttl = preg_replace("/@+/", "$separator", 

			preg_replace("/[^[:alnum:]]/", "@", 

				trim(

					preg_replace("/[^[:alnum:]]/", " ", 

						str_replace("'", "", 

							sls_wp_truncate(

								trim(

									strtolower($ttl)

								), 

							100)

						)

					)

				)

			)

		);

	return $ttl;

}

/*-------------------------------*/

function sls_wp_truncate($var,$length=50,$mode="return", $type=1) {

	

	if (strlen($var)>$length) {

		if ($type==1) { 

			$var=substr($var,0,$length);

			$var=preg_replace("@[[:space:]]{1}.{1,10}$@s", "", $var); 

			$var=$var."...";

		}

		elseif ($type==2) { 

			$r_num=mt_rand();

			$r_num2=$r_num."_2";

			$var1=substr($var,0,$length);

			$var2=substr($var,$length, strlen($var)-$length);

			$var="<span id='$r_num'>$var1</span><span id='$r_num2' style='display:none'>".$var1.$var2."</span><a href='#' onclick=\"show('$r_num');show('$r_num2');this.innerHTML=(this.innerHTML.indexOf('more')!=-1)?'(...less)':'(more...)';return false;\">(more...)</a>";

		}

		elseif ($type==3) { 

			$var=substr($var,0,$length);

			$var=$var."...";

		}

	}

	if ($mode!="print") {

		return $var;

	}

	else {

		print $var;

	}

}

/*-----------------------------------------------------------*/

function sls_wp_process_tags($tag_string, $db_action="insert", $sls_wp_id="") {

	global $wpdb;

	$id_string="";

	

	if (!is_array($sls_wp_id) && preg_match("@,@", $sls_wp_id)) {

		$id_string=$sls_wp_id;

		$sls_wp_id=explode(",",$id_string);

		$rplc_arr=array_fill(0, count($sls_wp_id), "%d"); 

		$id_string=implode(",", array_map(array($wpdb, "prepare"), $rplc_arr, $sls_wp_id)); 

	} elseif (is_array($sls_wp_id)) {

		$rplc_arr=array_fill(0, count($sls_wp_id), "%d"); 

 		$id_string=implode(",", array_map(array($wpdb, "prepare"), $rplc_arr, $sls_wp_id)); 

	} else {

		$id_string=$wpdb->prepare("%d", $sls_wp_id); 

	}

	if (preg_match("@,@", $tag_string)) { 

		$tag_string=preg_replace('/[^A-Za-z0-9_\-, ]/', '', $tag_string); 

		$sls_wp_tag_array=array_map('trim',explode(",",trim($tag_string))); 

		//$sls_wp_tag_array=array_map('strtolower', $sls_wp_tag_array); 

	} else { 

		$tag_string=preg_replace('/[^A-Za-z0-9_\-, ]/', '', $tag_string); 

		$sls_wp_tag_array[]=trim($tag_string); 

	} 

	if ($db_action=="insert") {

		$wpdb->query("DELETE FROM ".SLS_WP_TAG_TABLE." WHERE sls_wp_id IN ($id_string)");  //clear current tags for locations being modified 

		$query="INSERT INTO ".SLS_WP_TAG_TABLE." (sls_wp_tag_slug, sls_wp_id) VALUES ";

		if (!is_array($sls_wp_id)) {

			$main_sls_wp_id=($sls_wp_id==="")? $wpdb->insert_id : $sls_wp_id ; 

			foreach ($sls_wp_tag_array as $value)  {

				if (trim($value)!="") {

					$query.="('$value', '$main_sls_wp_id'),";

				}

			}

		} elseif (is_array($sls_wp_id)) {

			foreach ($sls_wp_id as $value2) {

				$main_sls_wp_id=$value2;

				foreach ($sls_wp_tag_array as $value)  {

					if (trim($value)!="") {

						$query.="('$value', '$main_sls_wp_id'),";

					}

				}

			}

		}

		$query=substr($query, 0, strlen($query)-1); 

		

	} elseif ($db_action=="delete") {

		if (trim($tag_string)==="") {

			$query="DELETE FROM ".SLS_WP_TAG_TABLE." WHERE sls_wp_id IN ($id_string)";

			

			$imageId = explode(',', $id_string); //split string into array seperated by ', '

			foreach($imageId as $value) //loop over values

       {
		   $dir=SLS_WP_UPLOADS_PATH."/images/".$value;
			if (is_dir($dir)){ 
				$images = @scandir($dir);
				foreach($images as $k=>$v):
				 if ($v != '.' && $v != '..'){
					unlink($dir.'/'.$v);
				}
				endforeach;
				rmdir($dir);
			}
			
			$dir=SLS_WP_UPLOADS_PATH."/images/icons/".$value;
			if (is_dir($dir)){ 
			$images = @scandir($dir);
				foreach($images as $k=>$v):
				 if ($v != '.' && $v != '..'){
					unlink($dir.'/'.$v);
				}
				endforeach;
				rmdir($dir);
			}
			
			}
		} else {

			$t_string=implode("','", $sls_wp_tag_array);  

			$query="DELETE FROM ".SLS_WP_TAG_TABLE." WHERE sls_wp_id IN ($id_string) AND sls_wp_tag_slug IN ('".trim($t_string)."')"; 

		}

	} 

	$wpdb->query($query);

}

/*-----------------------------------------------------------*/

function sls_wp_ty($file){

	$ty = '';

	return $ty;

}

/*-----------------------------------------------------------*/

function sls_wp_prepare_tag_string($sls_wp_tags) {

	$sls_wp_tags=preg_replace('/\,+/', ', ', $sls_wp_tags); 

	$sls_wp_tags=preg_replace('/(\&\#44\;)+/', '&#44; ', $sls_wp_tags); 

	$sls_wp_tags=preg_replace('/[^A-Za-z0-9_\-,]/', '', $sls_wp_tags); 

	if (substr($sls_wp_tags, 0, 1) == ",") {

		$sls_wp_tags=substr($sls_wp_tags, 1, strlen($sls_wp_tags));

	}

	if (substr($sls_wp_tags, strlen($sls_wp_tags)-1, 1) != "," && trim($sls_wp_tags)!="") {

		$sls_wp_tags.=",";

	}

	$sls_wp_tags=preg_replace('/\,+/', ', ', $sls_wp_tags);

	$sls_wp_tags=preg_replace('/(\&\#44\;)+/', '&#44; ', $sls_wp_tags);

	$sls_wp_tags=preg_replace('/[ ]*,[ ]*/', ', ', $sls_wp_tags); 

 	$sls_wp_tags=preg_replace('/[ ]*\&\#44\;[ ]*/', '&#44; ', $sls_wp_tags); 

	$sls_wp_tags=trim($sls_wp_tags);

	return $sls_wp_tags;

}

/*-----------------------------------------------------------*/

function sls_wp_data($setting_name, $i_u_d_s="select", $setting_value="") {

	global $wpdb;

	if ($i_u_d_s == "insert" || $i_u_d_s == "add" || $i_u_d_s == "update") {

		$setting_value = (is_array($setting_value))? serialize($setting_value) : $setting_value;

		$exists = $wpdb->get_var($wpdb->prepare("SELECT sls_wp_setting_id FROM ".SLS_WP_SETTING_TABLE." WHERE sls_wp_setting_name = %s", $setting_name));

		if (!$exists) {	

			$q = $wpdb->prepare("INSERT INTO ".SLS_WP_SETTING_TABLE." (sls_wp_setting_name, sls_wp_setting_value) VALUES (%s, %s)", $setting_name, $setting_value); 

		} else { 

			$q = $wpdb->prepare("UPDATE ".SLS_WP_SETTING_TABLE." SET sls_wp_setting_value = %s WHERE sls_wp_setting_name = %s", $setting_value, $setting_name);

		}

		$wpdb->query($q);

	} elseif ($i_u_d_s == "delete") {

		$q = $wpdb->prepare("DELETE FROM ".SLS_WP_SETTING_TABLE." WHERE sls_wp_setting_name = %s", $setting_name);

		$wpdb->query($q);

	} elseif ($i_u_d_s == "select" || $i_u_d_s == "get") {

		$q = $wpdb->prepare("SELECT sls_wp_setting_value FROM ".SLS_WP_SETTING_TABLE." WHERE sls_wp_setting_name = %s", $setting_name);

		$r = $wpdb->get_var($q);

		$r = (@unserialize($r) !== false || $r === 'b:0;')? unserialize($r) : $r;  //checking if stored in serialized form
			return $r;

	}

}

/*----------------------------------------------------------------*/

function sls_wp_dyn_js($post_content=""){

	global $sls_wp_dir, $sls_wp_base, $sls_wp_uploads_base, $sls_wp_path, $sls_wp_uploads_path, $wpdb, $sls_wp_version, $pagename, $sls_wp_map_language, $post, $sls_wp_vars;
	global $superlogoshowcase;
	if($post_content){
	    $post_content=$post_content[0]['post_content'];
		if(preg_match('@\[super-logo-showcase(.*)?\]@i', $post_content) && strpos($post_content, 'LAYOUT=metro') !== false) {
		   $sls_layout=2;
		}else if(preg_match('@\[super-logo-showcase(.*)?\]@i', $post_content) && strpos($post_content, 'LAYOUT=standard') !== false) {
		   $sls_layout=1;
		}else{
		   $sls_layout=(trim($sls_wp_vars['sls_layout'])!="")? slsParseToXML($sls_wp_vars['sls_layout']) : "1";
		}
	}else{
		$sls_layout=(trim($sls_wp_vars['sls_layout'])!="")? slsParseToXML($sls_wp_vars['sls_layout']) : "1";
	}

	print "<script type=\"text/javascript\">\n//<![CDATA[\n";
	//general

	$to_top = '';
	$sls_wp_map_settings=(trim($sls_wp_vars['sls_wp_map_settings'])!="")? $sls_wp_vars['sls_wp_map_settings'] : "1";
	$region_show=(trim($sls_wp_vars['region_show'])!="")? $sls_wp_vars['region_show'] : "1";
	$title_show=(trim($sls_wp_vars['title_show'])!="")? $sls_wp_vars['title_show'] : "1";
	$to_top=(trim($sls_wp_vars['to_top'])!="")? $sls_wp_vars['to_top'] : "1";
	//styles

	$main_bg=(trim($sls_wp_vars['main_bg'])!="")? slsParseToXML($sls_wp_vars['main_bg']) : "";

	$style_panel_bg=(trim($sls_wp_vars['style_panel_bg'])!="")? slsParseToXML($sls_wp_vars['style_panel_bg']) : "";

	$style_panel_font=(trim($sls_wp_vars['style_panel_font'])!="")? slsParseToXML($sls_wp_vars['style_panel_font']) : "";

	$style_show_all_font=(trim($sls_wp_vars['style_show_all_font'])!="")? slsParseToXML($sls_wp_vars['style_show_all_font']) : "";

	$style_contact_button_bg=(trim($sls_wp_vars['style_contact_button_bg'])!="")? slsParseToXML($sls_wp_vars['style_contact_button_bg']) : "";

	$style_contact_button_font=(trim($sls_wp_vars['style_contact_button_font'])!="")? slsParseToXML($sls_wp_vars['style_contact_button_font']) : "";

	$style_results_bg=(trim($sls_wp_vars['style_results_bg'])!="")? slsParseToXML($sls_wp_vars['style_results_bg']) : "";

	$style_results_hl_bg=(trim($sls_wp_vars['style_results_hl_bg'])!="")? slsParseToXML($sls_wp_vars['style_results_hl_bg']) : "";

	$style_results_hover_bg=(trim($sls_wp_vars['style_results_hover_bg'])!="")? slsParseToXML($sls_wp_vars['style_results_hover_bg']) : "";

	$style_results_font=(trim($sls_wp_vars['style_results_font'])!="")? slsParseToXML($sls_wp_vars['style_results_font']) : "";

	//labels
	$brands_label=(trim($sls_wp_vars['brands_label'])!="")? slsParseToXML($sls_wp_vars['brands_label']) : "Brands & Partners";
	$of_label=(trim($sls_wp_vars['of_label'])!="")? slsParseToXML($sls_wp_vars['of_label']) : "of";
	$results_label=(trim($sls_wp_vars['results_label'])!="")? slsParseToXML($sls_wp_vars['results_label']) : "results";
	$show_all_label=(trim($sls_wp_vars['show_all_label'])!="")? slsParseToXML($sls_wp_vars['show_all_label']) : "Show All";
	$all_category=(trim($sls_wp_vars['all_category'])!="")? slsParseToXML($sls_wp_vars['all_category']) : "All";
	$select_label=(trim($sls_wp_vars['select_label'])!="")? slsParseToXML($sls_wp_vars['select_label']) : "Select";
	$information_label=(trim($sls_wp_vars['information_label'])!="")? slsParseToXML($sls_wp_vars['information_label']) : "Information";
	$features_label=(trim($sls_wp_vars['features_label'])!="")? slsParseToXML($sls_wp_vars['features_label']) : "Features";
	$additional_info_label=(trim($sls_wp_vars['additional_info_label'])!="")? slsParseToXML($sls_wp_vars['additional_info_label']) : "Additional Info";
	$location_label=(trim($sls_wp_vars['location_label'])!="")? slsParseToXML($sls_wp_vars['location_label']) : "Location";
	$contact_details_label=(trim($sls_wp_vars['contact_details_label'])!="")? slsParseToXML($sls_wp_vars['contact_details_label']) : "Contact Details";
	$streetview_label=(trim($sls_wp_vars['streetview_label'])!="")? slsParseToXML($sls_wp_vars['streetview_label']) : "Categories";
	$cancel=(trim($sls_wp_vars['cancel'])!="")? slsParseToXML($sls_wp_vars['cancel']) : "Cancel";
	$no_brands_found=(trim($sls_wp_vars['no_brands_found'])!="")? slsParseToXML($sls_wp_vars['no_brands_found']) : "No brands found.";
	$load_more=(trim($sls_wp_vars['load_more'])!="")? slsParseToXML($sls_wp_vars['load_more']) : "Load More";
	$sls_grid=(trim($sls_wp_vars['sls_grid'])!="")? slsParseToXML($sls_wp_vars['sls_grid']) : "4";
	$category_btn_bg=(trim($sls_wp_vars['category_btn_bg'])!="")? slsParseToXML($sls_wp_vars['category_btn_bg']) : "";
	$category_btn_hover=(trim($sls_wp_vars['category_btn_hover'])!="")? slsParseToXML($sls_wp_vars['category_btn_hover']) : "";
	$category_btn_color=(trim($sls_wp_vars['category_font_color'])!="")? slsParseToXML($sls_wp_vars['category_font_color']) : "";
	$grayscale_setting=(trim($sls_wp_vars['grayscale_setting'])!="")? slsParseToXML($sls_wp_vars['grayscale_setting']) : "false";
	
	
	
	$grid_settings["".__("12", SLS_WP_TEXT_DOMAIN).""]="1";
	$grid_settings["".__("6", SLS_WP_TEXT_DOMAIN).""]="2";
	$grid_settings["".__("4", SLS_WP_TEXT_DOMAIN).""]="3";
	$grid_settings["".__("3", SLS_WP_TEXT_DOMAIN).""]="4";
	$grid_settings["".__("2", SLS_WP_TEXT_DOMAIN).""]="5";
	$grid_settings["".__("2", SLS_WP_TEXT_DOMAIN).""]="6";
	$gridbreak="";

	foreach($grid_settings as $key=>$value) {
		$gridbreak=($sls_grid==$value)? $key : "";
	}
		
	print "\n
	var sls_wp_base='".SLS_WP_BASE."';
	var sls_wp_uploads_base='".SLS_WP_UPLOADS_BASE."';
	var sls_wp_addons_base=sls_wp_uploads_base+'".str_replace(SLS_WP_UPLOADS_BASE, '', SLS_WP_ADDONS_BASE)."';
	var sls_wp_includes_base=sls_wp_base+'".str_replace(SLS_WP_BASE, '', SLS_WP_INCLUDES_BASE)."';
	var sls_wp_map_settings='$sls_wp_map_settings'; 
	var region_show='$region_show'; 
	var brands_label='$brands_label'; 
	var of_label='$of_label'; 
	var results_label='$results_label'; 
	var show_all_label='$show_all_label'; 
	var all_category='$all_category'; 
	var select_label='$select_label'; 
	var information_label='$information_label';
	var features_label='$features_label';
	var additional_info_label='$additional_info_label';
	var location_label='$location_label';
	var contact_details_label='$contact_details_label';
	var streetview_label='$streetview_label';
	var cancel='$cancel';
	var no_brands_found='$no_brands_found';
	var load_more='$load_more';
	var sls_default_category='';
	var sls_layout='$sls_layout';
	var sls_column='$sls_grid';
	\n";

	print "//]]>\n</script>\n";

	if (function_exists("do_sls_wp_hook")){do_sls_wp_hook('sls_wp_addon_head_scripts'); }

	if (function_exists("do_sls_wp_hook")){ 

		print "<script>\n//<![CDATA[\n";

		sls_wp_js_hooks();

		print "\n//]]>\n</script>\n";

	}
	
	global $wpdb;
	$style_hover='';
	$query=$wpdb->get_results("SELECT sls_pos,sls_wp_active_color,sls_wp_bg_color FROM ".SLS_WP_TABLE." WHERE sls_wp_is_published!=0 ", ARRAY_A);
	foreach ($query as $row){
		$sls=$row['sls_pos'];
		$sls_color=$row['sls_wp_active_color'];
		$bgcolor=$row['sls_wp_bg_color'];
		if($sls_color!=''){
			$style_hover.="\n#logoshowcase_$sls:hover{
				background-color:$sls_color !important;
			}";
			
			$style_hover.="\n#logoshowcase_$sls.item--alt-layout.is-toggled,
					#logoshowcase_$sls .item__expandable-contents.is-toggled{
				background-color: $sls_color !important;
			}";
		}else {
			if($style_results_hover_bg==""){
				$style_hover.="\n#logoshowcase_$sls:hover{
				background-color:#fff !important;
			}";
			
			$style_hover.="\n#logoshowcase_$sls.item--alt-layout.is-toggled,
					#logoshowcase_$sls div.is-toggled{
				background-color: #fff !important;
			}";
				
			}
			
		}
		
		if($bgcolor!=''){
			$style_hover.="\n#logoshowcase_$sls{
				background-color:$bgcolor !important;
			}";
		}
	}
	print "<style>";
	// fixing street view in firefox issue


if($main_bg!=""){ 

	print ".sls-body, .sls-body .content-section, .sls-body .section--padded-half{
		background-color: $main_bg !important;
	}";

 } 

if($style_panel_bg!=""){ 

	 print ".sls-panel {
		background-color: $style_panel_bg !important;
	}";

}



if($style_panel_font!=""){ 

 print ".sls-panel {
   color: $style_panel_font !important;

}";

}



if($style_show_all_font!=""){ 


print "#showAllLogos{
 color: $style_show_all_font !important;
}";

 }

 if($style_contact_button_bg!=""){ 

 print "#slsApplyFilterOptions{
    background-color: $style_contact_button_bg !important;
 }
 
 #slsMainBackToTop{
    background-color: $style_contact_button_bg !important;
 }
 
 .column-header{
   background-color: $style_contact_button_bg !important;
 }
 
 .details__content .default-tbl > * > * > th:first-child {
        border-left: 1px solid $style_contact_button_bg !important;
    }
    .details__content .default-tbl > * > * > th:last-child {
        border-right-color: $style_contact_button_bg !important;
    }
 ";

 }


if($style_contact_button_font!=""){ 

 print "#slsApplyFilterOptions{
    color: $style_contact_button_font !important;
 }
 
 #mainBackToTop{
    color: $style_contact_button_font !important;
 }
 
 .column-header{
  color: $style_contact_button_font !important;
 }
 
 ";

 }

 if($style_results_bg!=""){ 

print "span.item--alt-layout {

    background-color: $style_results_bg;
}

";

 }

if($style_results_hl_bg!=""){ 

print ".item--alt-layout.is-toggled {

    background-color: $style_results_hl_bg !important;
}

#sls_main_body .pad {
  background-color: $style_results_hl_bg !important;
}";

 }

if($style_results_hover_bg!=""){
	print ".item--alt-layout:hover{
	background-color: $style_results_hover_bg !important;
	}";
}
if($style_results_font!=""){ 

 print ".item--alt-layout.is-toggled {

    color: $style_results_font !important;
}

.details-group{
  color: $style_results_font !important;
}

.details__title{
color: $style_results_font !important;
}

.item__title{
 color: $style_results_font !important;
}
";

}

if($to_top=='0'){
	print "#slsMainBackToTop{
    display: none !important;
 }";
}

if($category_btn_bg!=''){
	print ".sls_metro_layout #filterCategories label {
		background-color: $category_btn_bg !important;
	}";
}

if($category_btn_hover!=''){
	print ".sls_metro_layout #filterCategories label.sls-active{
		background-color: $category_btn_hover !important;
	}";
}

if($category_btn_color!=''){
	print ".sls_metro_layout #filterCategories label {
		color: $category_btn_color !important;
	}";
}
if($grayscale_setting=='true'){
	print ".sls-main-content.page__wrapper{
			  filter: gray;
			-webkit-filter: grayscale(1);
			filter: grayscale(1);   
		}";
		
	print "span.item.item--alt-layout:hover{
			filter: none !important;
			-webkit-filter: none !important;
			filter: none !important;   
	}";
}

if($grayscale_setting=='logo_w_h'){
	
	print ".sls-main-content .item--alt-layout{
			  filter: gray;
			-webkit-filter: grayscale(1);
			filter: grayscale(1);   
		}";
	
}


if($grayscale_setting=='logo'){
	print ".sls-main-content .item--alt-layout{
			  filter: gray;
			-webkit-filter: grayscale(1);
			filter: grayscale(1);   
		}";
		
	print "span.item.item--alt-layout:hover{
			filter: none !important;
			-webkit-filter: none !important;
			filter: none !important;   
	}";
}

print $style_hover;
	print "</style>";

	$show_results = '';
	$show_cat = '';
	$titlesettings = '';
	$show_both = '';
	if($sls_wp_map_settings=='0'){
	$show_results ="style='display:none'";
	}
	if($region_show=='0'){
	$show_cat ="style='display:none'";

	}
	
	if($region_show=='0' && $sls_wp_map_settings=='0'){
		$show_both ="style='display:none'";
	}
	
	if($title_show=='0'){
	$titlesettings ="style='display:none'";
      
	}
	
	if($sls_layout==2){
		 $slslayout='sls_metro_layout';
		 $columnsTwo=12;
		$columnsOne=12;
		$columnEight=12;
		$columnsOne="<div class='medium-12 large-12 sls-column filters-column' id='logosFilter' $show_both>
			 <div class='filter filter--popup-small-down'>
			 <div class='filter-popup filter-popup--small-down' $show_cat>
			 <div class='filter__row' id='sls-category-list'>
			 <div  id='filterCategories'>
				&nbsp;
			 </div>
			 </div>
			 </div>
			 <button type='button' class='blank expand text-left filter-button show-for-small-down' id='logosFiltersPopupToggler'>
			 Filters
			 </button>
			 </div>
         </div>";
	}else{
		$slslayout='sls_standard_layout';
		$columnsTwo=9;
		$columnEight=8;
		$columnsOne="<div class='medium-4 large-3 sls-column filters-column' id='logosFilter' $show_both>
			 <div class='filter filter--popup-small-down sls-panel space-bottom'>
			 <div class='filter__row   hide-for-small-down' $show_results>
			 <div class='filter__items-counter'>
			 1 - <span id='currentLogosCount'>0</span> $of_label <span id='totalLogosCount'>0</span> $results_label<br />
			 <a href='#show-all' id='showAllLogos'>$show_all_label</a>
			 </div>
			 </div>
			 <button type='button' class='blank expand text-left filter-button show-for-small-down' id='logosFiltersPopupToggler'>
			 Filters
			 </button>
			 <div class='filter__row filter__row--regions' style='display:none;'>
			 <div class='filter__toggler arrow-toggler is-toggled'>
			 Region
			 </div>
			 <div class='filter__toggler-contents togglerify-slider is-toggled' id='filterRegions'>
			 <div class='spinner is-shown expand'>
			 &nbsp;
			 </div>
			 </div>
			 </div>
			 <div class='filter-popup filter-popup--small-down sls-panel' $show_cat>
			 <div class='filter__row' id='sls-category-list'>
			 <div class='filter__toggler arrow-toggler' >
			 $streetview_label
			 </div>
			 <div class='filter__toggler-contents togglerify-slider' id='filterCategories'>
			 &nbsp;
			 </div>
			 </div>
			 <div class='filter__row filter__row--cta'>
			 <div class='sls-row'>
			 <div class='small-6 sls-column'>
			 <a href='#' data-close-popup='true' class='sls-button grey expand hide-for-medium-up'>Cancel</a>
			 </div>
			 <div class='small-6 large-offset-6 sls-column'>
			 <a href='#' data-close-popup='true' class='sls-button expand' id='slsApplyFilterOptions'>$select_label</a>
			 </div>
			 </div>
			 </div>
			 </div>
			 </div>
         </div>";
	}
									   
	$superlogoshowcase = "<div id='sls_main_body' class='sls-body offcanvas page__oncanvas'>
         <div id='mainContent' class='sls-main-content page__wrapper $slslayout'>
         <div id='main' role='main'>
         <div class='content-section'>
         <div class='sls-section section--padded-half'>
         <div class='sls-wrapper'  $titlesettings>
         <h2 class='sls-title'>
         $brands_label
         </h2>
         </div>
         <div class='sls-row medium-space-top-2x  shop-row'>
		 $columnsOne
         <div class='medium-$columnEight large-$columnsTwo sls-column' id='logosContent'>
         <div class='sls-row   js-sync-height-wrapper' data-sync-height-selector='.item__title, .item__contents'>
         <div id='logosCatalogue'>
         <div class='space-vertical-4x pad-vertical-4x'>
         <div class='spinner expand is-shown'>&nbsp;</div>
         </div>
         </div>
         <div class='sls-column space-top small-down-space-bottom text-center' style='display: none;' >
         <a href='#load-more' class='h4 title' id='loadMoreLogos'>Load More</a>
         </div>
         </div>
         </div>
         </div>
         </div>
         </div>      
         </div>
         </div>
         </div>
        

				<!--[if lt IE 9]> 
                  <div class='main-popup-holder is-shown' id='mainPopupHolder' style='display:none;'>
                     <div class='popup is-shown' id='modernBrowserPopup'>
                        <a href='javascript:hideOldBrowserPopup();' class='popup-closer'>Close</a> 
                        <h1 class='popup-title'>Browser not supported.</h1>
                        <div class='pad-horizontal-2x'>
                           <p>Notice: Your current web browser will prevent you from viewing the site.</p>
                           <p><b>Try one of these browsers below:</b></p>
                           <div class='text-center pad-top' style='overflow: hidden;'> <a href='http://www.google.com/chrome' style='width: 33%; float: left; display: block;'> <img src='images/icons/browser-chrome.png' alt='Google Chrome' /> <br /> Google Chrome </a> <a href='http://windows.microsoft.com/ie' style='width: 33%; float: left; display: block;'> <img src='images/icons/browser-ie.png' alt='Internet Explorer' /> <br /> Internet Explorer 9 </a> <a href='http://www.firefox.com/' style='width: 33%; float: left; display: block;'> <img src='images/icons/browser-firefox.png' alt='Mozilla Firefox' /> <br /> Mozilla Firefox </a> </div>
                        </div>
                        <script> function hideOldBrowserPopup() { $('#mainPopupHolder, #modernBrowserPopup').removeClass('is-shown'); } </script> 
                     </div>
                  </div>
                  <![endif]--> 
				  
				  <div class='main-popup-holder' id='mainPopupHolder' style='display:none;'>
                     <div class='popup' id='modernBrowserPopup'>
                        <a href='javascript:hidePopup();' class='popup-closer'>Close</a> 
                        <h1 class='popup-title'>Title</h1>
                        <div class='pad-horizontal-2x' class='popup-img'>
						<center>
						<img id='popup-image' src='' style='max-width:650px !important;'/>
						</center>
						</div>
                        <script> function hidePopup() { jQuery('#mainPopupHolder, #modernBrowserPopup').removeClass('is-shown'); } 
						function showPopup(t,i) { 
						
						jQuery('.popup-title').html(t);
						jQuery('#popup-image').attr('src',i);
						jQuery('#mainPopupHolder, #modernBrowserPopup').addClass('is-shown'); } 
						
						</script> 
                     </div>
                  </div>";
}

/*---------------------------------------*/

function sls_wp_location_form($mode="add", $pre_html="", $post_html=""){
	global $wpdb;
	$store_state_options='';
	$store_state_options.="<option value='default' selected>-None-</option>";
$locales=$wpdb->get_results("SELECT * FROM ".SLS_WP_TABLE_CATEGORY."  ORDER BY sls_wp_state", ARRAY_A);	
foreach ($locales as $value) {
	$stateValue=$value['sls_wp_state'];
	$store_state_options.="<option value='$stateValue'>$stateValue</option>";
}
	
	$html="<script type='text/javascript'>
	bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('sls_wp_intro'); });
	bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('sls_wp_features'); });
	bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('sls_wp_add_intro'); });
	</script><div class='input_section'>

	<form name='manualAddForm' method='post' enctype='multipart/form-data'>

	$pre_html

					<div class='input_title'>
						<h3><span class='fa fa-pencil'>&nbsp;</span> Add a Logo</h3>

						<span class='submit'>
						<a href='".SLS_WP_ADMIN_NAV_BASE.SLS_WP_ADMIN_DIR."/pages/logos.php' style='margin-right:10px;'>
						<input type='button' value='".__("Cancel", SLS_WP_TEXT_DOMAIN)."' class='button-primary'></a>
						<input type='submit' value='".__("Save this Logo", SLS_WP_TEXT_DOMAIN)."' class='button-primary'>
						</span>

						<div class='clearfix'></div>

					</div>

					<div class='all_options'>
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Name</label>
						<input  type='text' name='sls_wp_logo'>
						<small>Enter your logo title</small>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						 Category</label>
						<select name='sls_wp_state'>
						$store_state_options
						</select>
						<small></small>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo' style='line-height:100px;'>
						Intro Text</label>
					<textarea name='sls_wp_intro' id='sls_wp_intro' style='width: 30%; height: 100px;'></textarea>
					<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo' style='line-height:100px;'>
						Features</label>
					<textarea name='sls_wp_features' id='sls_wp_features' style='width: 350px; height: 100px;'></textarea>
					<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo' style='line-height:100px;'>
						Additional Intro </label>
					<textarea name='sls_wp_add_intro' id='sls_wp_add_intro' style='width: 350px; height: 100px;'></textarea>
					<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Background color</label>
						<input type='text' class='my-color-field wp-color-picker' id='sls_wp_bg_color' value='' name='sls_wp_bg_color'>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Active/Hover color</label>
						<input type='text' class='my-color-field wp-color-picker' id='sls_wp_active_color' value='' name='sls_wp_active_color'>
						<div class='clearfix'></div>
					</div>
				
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Logo</label>
						<input type='file' name='sls_wp_image'>
						<small></small>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Banner</label>
						<input type='file' name='sls_wp_marker'>
						<small></small>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>

					<label for='shortname_logo'>

					Logo Link</label><br/><br/>

					<input type='radio' name='sls_wp_default_media'  value='slidedown' checked> Slide-down info <br/>

					<input type='radio' name='sls_wp_default_media'  value='link'> External Link <br/>

					<small></small>

					<div class='clearfix'></div>

					</div>
					
					
					<div class='option_input option_text'>

					<label for='shortname_logo'>

					External URL</label>

					<input type='text' name='sls_wp_ext_url'>

					<small></small>

					<div class='clearfix'></div>

					</div>
					
					<div class='option_input option_text'>

					<label for='shortname_logo'>

					Pop-Up External URL?</label><br/><br/>

					<input type='radio' name='sls_wp_pop_up'  value='yes' checked> Yes <br/>

					<input type='radio' name='sls_wp_pop_up'  value='no'> No <br/>

					<small></small>

					<div class='clearfix'></div>

					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Publish Status</label>
						<input type='radio' name='sls_wp_is_published' value='1' checked> Yes 
						<input type='radio' name='sls_wp_is_published' value='0'> No <br/>
						<small></small>
						<div class='clearfix'></div>
					</div>
					<div class='input_title'>
						<span class='submit'>
						<a href='".SLS_WP_ADMIN_NAV_BASE.SLS_WP_ADMIN_DIR."/pages/logos.php' style='margin-right:10px;'>
						<input type='button' value='".__("Cancel", SLS_WP_TEXT_DOMAIN)."' class='button-primary'></a>
						<input type='submit' value='".__("Save this Logo", SLS_WP_TEXT_DOMAIN)."' class='button-primary'>
						</span>

						<div class='clearfix'></div>

					</div></div>"; 
		$html.=(function_exists("do_sls_wp_hook"))? do_sls_wp_hook("sls_wp_add_location_fields",  "append-return") : "" ;

		$html.=wp_nonce_field("add-location_single", "_wpnonce", true, false);

		$html.="

	$post_html

</form></div>";
	return $html;

}

//add states
function sls_wp_add_state()
{
global $wpdb;
	$fieldList=""; $valueList="";
	foreach ($_POST as $key=>$value) {
		if (preg_match("@sls_wp_@", $key)) {
			if ($key=="sls_wp_tags") {
				$value=sls_wp_prepare_tag_string($value);
			}
			$fieldList.="$key,";
			
			if (is_array($value)){
				$value=serialize($value); //for arrays being submitted
				$valueList.="'$value',";
				
			} else {
				$valueList.=$wpdb->prepare("%s", sls_comma(stripslashes($value))).",";
				
			}
		}
	}

	$fieldList=substr($fieldList, 0, strlen($fieldList)-1);
	$valueList=substr($valueList, 0, strlen($valueList)-1);
	$wpdb->query("INSERT INTO ".SLS_WP_TABLE_CATEGORY." ($fieldList) VALUES ($valueList)");
	}

function sls_wp_state_form($mode="add", $pre_html="", $post_html=""){
	$html="<div class='input_section'>
	<form name='manualAddForm' method='post' enctype='multipart/form-data'>
	$pre_html
					<div class='input_title'>
						<h3><span class='fa fa-pencil'>&nbsp;</span> Add a Category</h3>
						
						<div class='clearfix'></div>
					</div>
					<div class='all_options'>
					<div class='option_input option_text'>
					<label for='shortname_logo'>
					Name</label>
					<input  type='text' name='sls_wp_state'>
					<small>Enter category name</small>
					<div class='clearfix'></div>
					</div>
					<div class='input_title'>
						<span class='submit'>
						<a href='".SLS_WP_ADMIN_NAV_BASE.SLS_WP_ADMIN_DIR."/pages/states.php' style='margin-right:10px;'>
						<input type='button' value='".__("Cancel", SLS_WP_TEXT_DOMAIN)."' class='button-primary'></a>
						<input type='submit' value='".__("Save", SLS_WP_TEXT_DOMAIN)."' class='button-primary'>
						
						</span>
						<div class='clearfix'></div>
					</div></div>"; 
		
		
		$html.=(function_exists("do_sls_wp_hook"))? do_sls_wp_hook("sls_wp_add_location_fields",  "append-return") : "" ;
		$html.=wp_nonce_field("add-location_single", "_wpnonce", true, false);
		$html.="
	$post_html
</form></div>";

	
	return $html;
}


//end state



function sls_wp_add_location() {

	global $wpdb;

	$fieldList=""; $valueList="";

	foreach ($_POST as $key=>$value) {

		if (preg_match("@sls_wp_@", $key)) {

			if ($key=="sls_wp_tags") {

				$value=sls_wp_prepare_tag_string($value);

			}

			$fieldList.="$key,";

			if (is_array($value)){

				$value=serialize($value); //for arrays being submitted

				$valueList.="'$value',";


			} else {

				$valueList.=$wpdb->prepare("%s", sls_comma(stripslashes($value))).",";

				

			}

		}

	}

	//add store

	$fieldList=substr($fieldList, 0, strlen($fieldList)-1);

	$valueList=substr($valueList, 0, strlen($valueList)-1);


	$wpdb->query("INSERT INTO ".SLS_WP_TABLE." ($fieldList) VALUES ($valueList)");

	$new_loc_id=$wpdb->insert_id;

	//$address="$_POST[sls_wp_address], $_POST[sls_wp_city], $_POST[sls_wp_state] $_POST[sls_wp_zip]";

	//sls_wp_do_geocoding($address);// comment by anubhav 28/07/2015 

 	if (!empty($_POST['sls_wp_tags'])){

		sls_wp_process_tags($_POST['sls_wp_tags'], "insert", $new_loc_id);

	}

	

	/*********** image upload *************/

	$wpdb->query($wpdb->prepare("UPDATE ".SLS_WP_TABLE." SET sls_pos='".$new_loc_id."' WHERE sls_wp_id='%d'", $new_loc_id));

		if(!empty($_FILES['sls_wp_image']['name'])){
			$valid_exts = array("jpg","jpeg","gif","png");
			$ext = explode(".",strtolower(trim($_FILES["sls_wp_image"]["name"])));
			$ext = end($ext);
			if(in_array($ext,$valid_exts)){

				$max_dimension = 800; // Max new width or height, can not exceed this value.
				 $dir=SLS_WP_UPLOADS_PATH."/images/".$new_loc_id;
				 if(!is_dir($dir))
				 {
					 mkdir($dir, 0777, true);
					 @chmod($dir, 0777);
				 }
				$postvars = array(
				"image"    => $new_loc_id.'.png',
				"image_tmp"    => $_FILES["sls_wp_image"]["tmp_name"],
				"image_size"    => (int)$_FILES["sls_wp_image"]["size"],
				"image_max_width"    => (int)100,
				"image_max_height"   => (int)100

				);

					if($ext == "jpg" || $ext == "jpeg"){
					$image = imagecreatefromjpeg($postvars["image_tmp"]);
					}

					else if($ext == "gif"){
					$image = imagecreatefromgif($postvars["image_tmp"]);
					}

					else if($ext == "png"){
					$image = imagecreatefrompng($postvars["image_tmp"]);
					}

				    $size = getimagesize($postvars["image_tmp"]);
					$ratio = $size[0]/$size[1]; // width/height
					if( $ratio > 1) {
					$width = 100;
					$height = 100/$ratio;
					$width2 = $size[0];
					$height2 = $size[0]/$ratio;
					}

					else {
					$width = 100*$ratio;
					$height = 100;
					$width2 = $size[0]*$ratio;
					$height2 = $size[0];
					}

					$tmp = imagecreatetruecolor($width,$height);
					$tmp2 = imagecreatetruecolor($width2,$height2);

					if($ext == "gif" or $ext == "png"){
						imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
						imagealphablending($tmp, false);
						imagesavealpha($tmp, true);
						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);
						$filename = $dir.'/'.$postvars["image"];
						imagepng($tmp,$filename,9);
						imagecolortransparent($tmp2, imagecolorallocatealpha($tmp2, 0, 0, 0, 127));
						imagealphablending($tmp2, false);
						imagesavealpha($tmp2, true);
						imagecopyresampled($tmp2,$image,0,0,0,0,$width2,$height2,$size[0],$size[1]);
						$filename2 = $dir.'/ori_'.$postvars["image"];
						imagepng($tmp2,$filename2,9);

					}

					else

					{
						$whiteBackground = imagecolorallocate($tmp, 255, 255, 255); 
						imagefill($tmp,0,0,$whiteBackground); // fill the background with white
						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);
						$filename = $dir.'/'.$postvars["image"];
						imagejpeg($tmp,$filename,100);
						$whiteBackground2 = imagecolorallocate($tmp2, 255, 255, 255); 
						imagefill($tmp2,0,0,$whiteBackground2); // fill the background with white
						imagecopyresampled($tmp2,$image,0,0,0,0,$width2,$height2,$size[0],$size[1]);
						$filename2 = $dir.'/ori_'.$postvars["image"];
						imagejpeg($tmp2,$filename2,100);

					}

					imagedestroy($image);
					imagedestroy($tmp);
					imagedestroy($tmp2);
			
				}

			}
			
	/*custom marker add with store image */

			if(!empty($_FILES['sls_wp_marker']['name'])){

				$dir=SLS_WP_UPLOADS_PATH."/images/icons/".$new_loc_id;
				 if(!is_dir($dir))
				 {
					 mkdir($dir, 0777, true);
					 @chmod($dir, 0777);
				 }

			$valid_exts = array("jpg","jpeg","gif","png");
			$ext = explode(".",strtolower(trim($_FILES["sls_wp_marker"]["name"])));
			$ext = end($ext);
			if(in_array($ext,$valid_exts)){

				$max_dimension = 800; // Max new width or height, can not exceed this value.

				$postvars = array(

				"image"    => $new_loc_id.'.png',

				"image_tmp"    => $_FILES["sls_wp_marker"]["tmp_name"],

				);

				 

					if($ext == "jpg" || $ext == "jpeg"){

					$image = imagecreatefromjpeg($postvars["image_tmp"]);

					}

					else if($ext == "gif"){

					$image = imagecreatefromgif($postvars["image_tmp"]);

					}

					else if($ext == "png"){

					$image = imagecreatefrompng($postvars["image_tmp"]);

					}

					$size = getimagesize($postvars["image_tmp"]);

					$ratio = $size[0]/$size[1]; // width/height

					if( $ratio > 1) {

					$width = 700;

					$height = 700/$ratio;
					$width2 = $size[0];

					$height2 = $size[0]/$ratio;

					}

					else {

					$width = 700*$ratio;

					$height = 700;

					$width2 = $size[0]*$ratio;

					$height2 = $size[0];

					}

					$tmp = imagecreatetruecolor($width,$height);

					if($ext == "gif" or $ext == "png"){

						imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));

						imagealphablending($tmp, false);

						imagesavealpha($tmp, true);

						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);

						$filename = $dir.'/'.$postvars["image"];

						imagepng($tmp,$filename,9);

					}

					else
					{

						$whiteBackground = imagecolorallocate($tmp, 255, 255, 255); 

						imagefill($tmp,0,0,$whiteBackground); // fill the background with white

						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);

						$filename = $dir.'/'.$postvars["image"];

						imagejpeg($tmp,$filename,100);

					}

					imagedestroy($image);

					imagedestroy($tmp);

				}

			}
}

/*--------------------------------------------------*/

function sls_wp_define_db_tables() {
	global $wpdb; 

	$sls_wp_db_prefix = $wpdb->prefix; 

	if (!defined('SLS_WP_DB_PREFIX')){ define('SLS_WP_DB_PREFIX', $sls_wp_db_prefix); }

	if (!empty($sls_wp_db_prefix)) {

		if (!defined('SLS_WP_TABLE')){ define('SLS_WP_TABLE', SLS_WP_DB_PREFIX."sls_wp_logos"); }
        if (!defined('SLS_WP_TABLE_CATEGORY')){ define('SLS_WP_TABLE_CATEGORY', SLS_WP_DB_PREFIX."sls_wp_category"); }
		if (!defined('SLS_WP_TAG_TABLE')){ define('SLS_WP_TAG_TABLE', SLS_WP_DB_PREFIX."sls_wp_tag"); }
		if (!defined('SLS_WP_SETTING_TABLE')){ define('SLS_WP_SETTING_TABLE', SLS_WP_DB_PREFIX."sls_wp_setting"); }

	}

}

sls_wp_define_db_tables(); 

/*----------------------------------------------------*/

function sls_wp_single_location_info($value, $colspan, $bgcol) {

	global $sls_wp_hooks;
	if(isset($_GET['edit'])){
	$_GET['edit'] = $value['sls_wp_id']; 

	

	print "<tr style='background-color:$bgcol' id='sls_wp_tr_data-$value[sls_wp_id]'>";

	$cancel_onclick = "location.href=\"".str_replace("&edit=$_GET[edit]", "",$_SERVER['REQUEST_URI'])."\"";

	$dir=SLS_WP_UPLOADS_PATH."/images/".$_GET['edit']."/";
    

	if (is_dir($dir)){

	$image_upload_path="../wp-content/uploads/sls-wp-uploads/images/";

	$images = @scandir($dir);

		foreach($images as $k=>$v):

		endforeach;

		$imageEdit='<div id="editImage'.$_GET['edit'].'" style="display:inline-block;"><img src="'.$image_upload_path.$_GET['edit'].'/'.$v.'?t='.time().'" style="max-width:300px;">';

	    $imageEdit_btn="<input type=\"button\" onclick=\"deleteMarker($_GET[edit]);\" class=\"btn btn-danger\"  value=\"Delete\"></div>";

		$desabled="disabled='disabled'";

		

	}

	else

	{

		$imageEdit='';

		$imageEdit_btn='';

		$desabled='';

	}

	/* custom marker edir code */

	$dir_marker=SLS_WP_UPLOADS_PATH."/images/icons/".$_GET['edit']."/";

	if (is_dir($dir_marker)){

	$image_upload_path="../wp-content/uploads/sls-wp-uploads/images/icons/";

	$images = @scandir($dir_marker);

		foreach($images as $k=>$v):

		endforeach;

		$markerEdit='<div id="editCmarker'.$_GET['edit'].'" style="display:inline-block;"><img src="'.$image_upload_path.$_GET['edit'].'/'.$v.'?t='.time().'" style="max-width:300px;">';

	    $markerEdit_btn="<input type=\"button\" onclick=\"delMarker($_GET[edit]);\" class=\"btn btn-danger\"  value=\"Delete\"></div>";

		$des_marker="disabled='disabled'";

	}

	else

	{

		$markerEdit='';

		$markerEdit_btn='';

		$des_marker='';

	}
	/* end custom */

	$media1 = '';
	$media2 = '';
	if($value['sls_wp_is_published']=='1' || $value['sls_wp_is_published']=='')
	 { 
	 	$media1 = "checked"; 
	  }
	else {
		 $media2 = "checked";
	 }
	 
	 
	$logolink1 = '';

	$logolink2 = '';

	

	if($value['sls_wp_default_media']=='slidedown' || $value['sls_wp_default_media']=='') { $logolink1 = "checked"; }

	if($value['sls_wp_default_media']=='link') { $logolink2 = "checked"; }
	
	
	 $pop_up_val='';
	 $pop_up_val2='';
	 if($value['sls_wp_pop_up']=='yes' || $value['sls_wp_pop_up']=='') { $pop_up_val = "checked"; }

	if($value['sls_wp_pop_up']=='no') { $pop_up_val2 = "checked"; }
	global $wpdb;
	$store_state_options='';
	$store_state_options.="<option value='default'>-None-</option>";
    $locales=$wpdb->get_results("SELECT * FROM ".SLS_WP_TABLE_CATEGORY."  ORDER BY sls_wp_state", ARRAY_A);	
	foreach ($locales as $row) {
		$selected2=($row['sls_wp_state']==$value['sls_wp_state'])? " selected " : "";
		$store_state_options.="<option value='$row[sls_wp_state]' $selected2>$row[sls_wp_state]</option>\n";
		
	}

	print "<script type='text/javascript'>
	bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('sls_wp_intro'); });
	bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('sls_wp_features'); });
	bkLib.onDomLoaded(function() { new nicEditor({fullPanel : true}).panelInstance('sls_wp_add_intro'); });
	</script><div class='input_section'><td colspan='$colspan'>
	<div class='input_section'>
	<a name='a$value[sls_wp_id]'></a>
	<form name='manualAddForm'  method='post' enctype='multipart/form-data'>
					<div class='input_title'>
						<h3><span class='fa fa-pencil'>&nbsp;</span> Edit Logo</h3>
						<span class='submit'>
						<input type='submit' value='".__("Save", SLS_WP_TEXT_DOMAIN)."' class='button-primary'> <input type='button' class='button' value='".__("Cancel", SLS_WP_TEXT_DOMAIN)."' onclick='$cancel_onclick'>
						</span>
						<div class='clearfix'></div>
					</div>
					<div class='all_options'>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						 Name</label>
						<input type='text' name='sls_wp_logo-$value[sls_wp_id]' id='sls_wp_logo-$value[sls_wp_id]' value='$value[sls_wp_logo]'>
						<small>Enter your logo title</small>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Categories </label>
						<select name='sls_wp_state-$value[sls_wp_id]' id='sls_wp_state-$value[sls_wp_id]'>
						$store_state_options
						</select>
						<small></small>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo' style='line-height:100px;'>
						Intro Text</label>
					<textarea  name='sls_wp_intro-$value[sls_wp_id]' id='sls_wp_intro' style='width: 350px; height: 100px;'>
						$value[sls_wp_intro]
					</textarea>
					<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo' style='line-height:100px;'>
						Features</label>
					<textarea name='sls_wp_features-$value[sls_wp_id]' id='sls_wp_features' style='width: 350px; height: 100px;'>
						$value[sls_wp_features]
					</textarea>
					<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo' style='line-height:100px;'>
						Additional Intro </label>
					<textarea name='sls_wp_add_intro-$value[sls_wp_id]' id='sls_wp_add_intro' style='width: 350px; height: 100px;'>
					$value[sls_wp_add_intro]
					</textarea>
					<div class='clearfix'></div>
					</div>
					
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Background color</label>
						<input type='text' class='my-color-field wp-color-picker' id='sls_wp_bg_color' value='$value[sls_wp_bg_color]' name='sls_wp_bg_color-$value[sls_wp_id]'>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Active/Hover color</label>
						<input type='text' class='my-color-field wp-color-picker' id='sls_wp_active_color' value='$value[sls_wp_active_color]' name='sls_wp_active_color-$value[sls_wp_id]'>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Logo</label>
						<input type='file' class='custom_marker' name='sls_wp_image' id='sls_wp_image'  ".$desabled." size='13'>".$imageEdit.$imageEdit_btn."
						<small></small>
						<div class='clearfix'></div>
					</div>

					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Banner</label>
						<input type='file' class='custom_marker' name='sls_wp_marker' id='sls_wp_marker'  ".$des_marker." size='13'>".$markerEdit.$markerEdit_btn."
						<small></small>
						<div class='clearfix'></div>
					</div>
					
					<div class='option_input option_text'>

					<label for='shortname_logo'>

					Logo Link</label><br/><br/>

					<input type='radio' name='sls_wp_default_media-$value[sls_wp_id]' id='sls_wp_default_media-$value[sls_wp_id]' value='slidedown' $logolink1> Slide-down info <br/>

					<input type='radio' name='sls_wp_default_media-$value[sls_wp_id]' id='sls_wp_default_media-$value[sls_wp_id]' value='link' $logolink2> External Link <br/>

					<small></small>

					<div class='clearfix'></div>

					</div>
					
					<div class='option_input option_text'>

					<label for='shortname_logo'>

					External URL</label>

					<input type='text' name='sls_wp_ext_url-$value[sls_wp_id]' id='sls_wp_ext_url-$value[sls_wp_id]' value='$value[sls_wp_ext_url]' size='13'>

					<small></small>

					<div class='clearfix'></div>

					</div>
					
					<div class='option_input option_text'>

					<label for='shortname_logo'>

					Pop-Up External URL?</label><br/><br/>

					<input type='radio' name='sls_wp_pop_up-$value[sls_wp_id]' id='sls_wp_pop_up-$value[sls_wp_id]'  value='yes' $pop_up_val> Yes <br/>

					<input type='radio' name='sls_wp_pop_up-$value[sls_wp_id]' id='sls_wp_pop_up-$value[sls_wp_id]' value='no' $pop_up_val2> No <br/>

					<small></small>

					<div class='clearfix'></div>

					</div>

					<div class='option_input option_text'>
						<label for='shortname_logo'>
						Publish Status</label>
						<input type='radio' name='sls_wp_is_published-$value[sls_wp_id]' id='sls_wp_is_published-$value[sls_wp_id]' value='1' $media1> Yes 
						<input type='radio' name='sls_wp_is_published-$value[sls_wp_id]' id='sls_wp_is_published-$value[sls_wp_id]' value='0' $media2> No 
						<small></small>
						<div class='clearfix'></div>
					</div>
	
					<div class='input_title'>

						<span class='submit'>

						<input type='submit' value='".__("Save", SLS_WP_TEXT_DOMAIN)."' class='button-primary'> <input type='button' class='button' value='".__("Cancel", SLS_WP_TEXT_DOMAIN)."' onclick='$cancel_onclick'>

						

						</span>

						<div class='clearfix'></div>

					</div></div>";

		

		if (function_exists("do_sls_wp_hook")) {

			sls_wp_show_custom_fields();

		}



	if (function_exists("do_sls_wp_hook")) {do_sls_wp_hook("sls_wp_single_location_edit", "select-top");}

	print "</form></td>";



print "</tr>";

}

	}
	
//state edit code start
function sls_wp_single_state_info($value, $colspan, $bgcol) {
	global $sls_wp_hooks;
	
	if(isset($_GET['edit'])){
	$_GET['edit'] = $value['sls_wp_id']; 
	
	print "<tr style='background-color:$bgcol' id='sls_wp_tr_data-$value[sls_wp_id]'>";
	$cancel_onclick = "location.href=\"".str_replace("&edit=$_GET[edit]", "",$_SERVER['REQUEST_URI'])."\"";
	print "<td colspan='$colspan'>
	
	
	<div class='input_section'>
	<a name='a$value[sls_wp_id]'></a>
	<form name='manualAddForm'  method='post' enctype='multipart/form-data'>

					<div class='input_title'>
						
						<h3><span class='fa fa-pencil'>&nbsp;</span> Edit Category</h3>
						<div class='clearfix'></div>
					</div>
					<div class='all_options'>
					
					<div class='option_input option_text'>
					<label for='shortname_logo'>
					Name</label>
					<input type='text' name='sls_wp_state-$value[sls_wp_id]' id='sls_wp_state-$value[sls_wp_id]' value='$value[sls_wp_state]'>
					<small>Enter  state name</small>
					<div class='clearfix'></div>
					</div>
					<div class='input_title'>
						<span class='submit'>
						<input type='submit' value='".__("Save", SLS_WP_TEXT_DOMAIN)."' class='button-primary'> <input type='button' class='button' value='".__("Cancel", SLS_WP_TEXT_DOMAIN)."' onclick='$cancel_onclick'>
						
						</span>
						<div class='clearfix'></div>
					</div></div>";
		
		if (function_exists("do_sls_wp_hook")) {
			sls_wp_show_custom_fields();
		}
	if (function_exists("do_sls_wp_hook")) {do_sls_wp_hook("sls_wp_single_location_edit", "select-top");}
	print "</form></td>";

print "</tr>";

}

	}
	
//state edit code end 	
	
/*-------------------------------------------*/

function sls_wp_module($mod_name, $mod_heading="", $height="") {

	global $sls_wp_vars, $wpdb;

	if (file_exists(SLS_WP_INCLUDES_PATH."/module-{$mod_name}.php")) {

		$css=(!empty($height))? "height:$height;" : "" ;

		print "<table class='widefat' style='background-color:transparent; border:0px; padding:4px; {$css}'>";

		if ($mod_heading){

			print "<thead><tr><th style='font-weight:bold; height:22px;'>$mod_heading</th></tr></thead>";

		}

		print "<tbody style='background-color:transparent; border:0px;'><tr><td style='background-color:transparent; border:0px;'>";

		include(SLS_WP_INCLUDES_PATH."/module-{$mod_name}.php");

		print "</td></tr></tbody></table><br>";

	}

}

/*--------------------------------------------*/

function sls_wp_readme_parse($path_to_readme, $path_to_env){

	include($path_to_env);



ob_start();

include($path_to_readme);

$txt=ob_get_contents();

ob_clean();





$toc=$txt;

	preg_match_all("@\=\=\=[ ]([^\=\=\=]+)[ ]\=\=\=@", $toc, $toc_match_0);

	preg_match_all("@\=\=[ ]([^\=\=]+)[ ]\=\=@", $toc, $toc_match_1); 

	preg_match_all("@\=[ ]([^\=]+)[ ]\=@", $toc, $toc_match_2); 

	$toc_cont="";

	foreach ($toc_match_2[1] as $heading) {

	    if (!in_array($heading, $toc_match_1[1]) && !in_array($heading, $toc_match_0[1]) && !preg_match("@^[0-9]+\.[0-9]+@", $heading)) {

		$toc_cont.="<li style='margin-left:30px; list-style-type:circle'><a href='#".sls_comma($heading)."' style='text-decoration:none'>$heading</a></li></li>";

	    } elseif (!in_array($heading, $toc_match_0[1]) && !preg_match("@^[0-9]+\.[0-9]+@", $heading)) { 

	    

	    	$toc_cont.="<li style='margin-left:15px; list-style-type:disc'><b><a href='#".sls_comma($heading)."' style='text-decoration:none'>$heading</a></b></li>";

	    }

	}





$th_start="<th style='font-size:125%; font-weight:bold;'>";

$h2_start="<h2 style='font-family:Georgia; margin-bottom:0.05em;'>";

$h3_start="<h3 style='font-family:Georgia; margin-bottom:0.05em; margin-top:0.3em'>";

$txt=str_replace("=== ", "$h2_start", $txt);

$txt=str_replace(" ===", "</h2>", $txt);



$txt=str_replace("== ", "<table class='widefat' ><thead>$th_start", $txt);

$txt=str_replace(" ==", "</th></thead></table><!--a style='float:right' href='#readme_toc'>Table of Contents</a-->", $txt);

$txt=str_replace("= ", "$h3_start", $txt);

$txt=str_replace(" =", "</h3><a style='float:right; position:relative; top:-1.5em; font-size:10px' href='#readme_toc'>".__("table of contents", SLS_WP_TEXT_DOMAIN)."</a>", $txt);

$txt=preg_replace("@Tags:[ ]?[^\r\n]+\r\n@", "", $txt);





$txt=str_replace("</h2>", "</h2><a name='readme_toc'></a><div style='float:right;  width:500px; border-radius:1em; border:solid silver 1px; padding:7px; padding-top:0px; margin:10px; margin-right:0px;'><h3>".__("Table of Contents", SLS_WP_TEXT_DOMAIN)."</h2>$toc_cont</div>", $txt);

$txt=preg_replace_callback("@$h2_start<u>([^<.]*)</u></h1>@s", create_function('$matches', 

	'return "<h2 style=\'font-family:Georgia; margin-bottom:0.05em;\'><a name=\'".sls_comma($matches[1])."\'></a>$matches[1]</u></h1>";'), $txt);

$txt=preg_replace_callback("@$th_start([^<.]*)</th>@s", create_function('$matches',

	'return "<th style=\'font-size:125%; font-weight:bold;\'><a name=\'".sls_comma($matches[1])."\'></a>$matches[1]</th>";'), $txt);

$txt=preg_replace_callback("@$h3_start( )*([^<.]*)( )*</h3>@s", create_function('$matches',

	'return "<h3 style=\'font-family:Georgia; margin-bottom:0.05em; margin-top:0.3em\'><a name=\"".sls_comma($matches[2])."\"></a>{$matches[1]}$matches[2]</h3>";'), $txt);





$txt=preg_replace("@\[([a-zA-Z0-9_/?&amp;\&\ \.%20,=\-\+\-\']+)*\]\(([a-zA-Z]+://)(([.]?[a-zA-Z0-9_/?&amp;%20,=\-\+\-\#]+)*)\)@s", "<a onclick=\"window.parent.open('\\2'+'\\3');return false;\" href=\"#\">\\1</a>", $txt);





$txt=preg_replace("@\*[ ]?[ ]?([^\r\n]+)*(\r\n)?@s", "<li style='margin-left:15px; margin-bottom:0px;'>\\1</li>", $txt);





$txt=preg_replace("@`([^`]+)*`@", "<strong class='sls_wp_code code' style='padding:2px; border:0px'>\\1</strong>", $txt);

$txt=preg_replace("@__([^__]+)__@", "<strong>\\1</strong>", $txt);

$txt=preg_replace("@\r\n([0-9]\.)@", "\r\n&nbsp;&nbsp;&nbsp;\\1", $txt);

$txt=preg_replace("@([A-Za-z-0-9\/\\&;# ]+): @", "<strong>\\1: </strong>", $txt);





$txt=sls_do_hyperlink($txt, "'_blank'", "protocol");



print nl2br($txt);





}

/*---------------------------------------------------------------*/

function sls_wp_translate_stamp($dateVar="",$mode="return", $date_only=0, $abbreviate_month=0) {

if ($dateVar!="") {

		$mm=substr($dateVar,4,2);

		$dd=substr($dateVar,6,2);

		if ($dd<10) {$dd=str_replace("0","",$dd); } 		$yyyy=substr($dateVar,0,4);

		if (strlen($yyyy)==2 && $yyyy>=50) {

			$yyyy="19".$yyyy;

		}

		elseif (strlen($yyyy)==2 && $yyyy>=00 && $yyyy<50) {

			$yyyy="20".$yyyy;

		}

}

$months=array("January","February","March","April","May","June","July","August","September","October","November","December");

$dt="";

if (!empty($mm)) {

	$dt=$months[$mm-1];

	

	if ($abbreviate_month!=0) 

		$dt=substr($dt,0,3).".";

	

	if ($dd!="" && $yyyy!="")

		$dt.=" $dd, $yyyy";

}



if ($date_only==0) {



$hr=substr($dateVar,8,2);

$min=substr($dateVar,10,2);

$sec=substr($dateVar,12,2);



if ($hr<12) {$hr=str_replace("0","",$hr); }

if ($hr>12) {$hr=$hr-12; $suffix="pm";} else {$suffix="am";}

if ($hr==12) {$suffix="pm";}

if ($hr==0) {$hr=12;}



$dt.=" $hr:$min:$sec $suffix";



}



if ($mode!="print")

	return $dt;

elseif ($mode=="print")

	print $dt;



}

/*---------------------------------------------------------------*/

function sls_wp_translate_date($dateVar="",$mode="return") {

if ($dateVar!="") {

		$parts=explode("/",$dateVar);

		$mm=trim($parts[0]);

		$dd=trim($parts[1]);

		if ($dd<10) {$dd=str_replace("0","",$dd); } 		$yyyy=trim($parts[2]);

		if (strlen($yyyy)==2 && $yyyy>=50) {

			$yyyy="19".$yyyy;

		}

		elseif (strlen($yyyy)==2 && $yyyy>=00 && $yyyy<50) {

			$yyyy="20".$yyyy;

		}

}

$months=array("January","February","March","April","May","June","July","August","September","October","November","December");



if ($mm!="") {

	$dt=$months[$mm-1];

	

	if ($dd!="" && $yyyy!="")

		$dt.="&nbsp;$dd,&nbsp;$yyyy";

}



if ($mode=="return")

	return $dt;

elseif ($mode=="print")

	print $dt;



}

/*-----------------------------------------------*/

add_action('admin_bar_menu', 'sls_wp_admin_toolbar', 183);

function sls_wp_admin_toolbar($admin_bar){

	/*

	$sls_wp_admin_toolbar_array[] = array(

		'id'    => 'sls-wp-menu',

		'title' => __('Super Logos Showcase', SLS_WP_TEXT_DOMAIN),

		'href'  => SLS_WP_INFORMATION_PAGE,	

		'meta'  => array(

			'title' => 'Super Logos Showcase Wordpress Plugin',			

		),

	);

	$sls_wp_admin_toolbar_array[] = array(

		'id'    => 'sls-wp-menu-news-upgrades',

		'parent' => 'sls-wp-menu',

		'title' => __('Quick Start', SLS_WP_TEXT_DOMAIN),

		'href'  => SLS_WP_INFORMATION_PAGE,

		'meta'  => array(

			'title' => __('Quick Start', SLS_WP_TEXT_DOMAIN),

			'target' => '_self',

			'class' => 'sls_wp_menu_class'

		),

	);

	$sls_wp_admin_toolbar_array[] = array(

		'id'    => 'sls-wp-menu-locations',

		'parent' => 'sls-wp-menu',

		'title' => __('Logos', SLS_WP_TEXT_DOMAIN),

		'href'  => SLS_WP_MANAGE_LOCATIONS_PAGE,

		'meta'  => array(

			'title' => __('Logos', SLS_WP_TEXT_DOMAIN),

			'target' => '_self',

			'class' => 'sls_wp_menu_class'

		),

	);

	$sls_wp_admin_toolbar_array[] = array(

		'id'    => 'sls-wp-menu-settings',

		'parent' => 'sls-wp-menu',

		'title' => __('Settings', SLS_WP_TEXT_DOMAIN),

		'href'  => SLS_WP_SETTINGS_PAGE1,

		'meta'  => array(

			'title' => "Settings ".__('Settings', SLS_WP_TEXT_DOMAIN),

			'target' => '_self',

			'class' => 'sls_wp_menu_class'

		),

	);

	

	if (function_exists('do_sls_wp_hook')){ do_sls_wp_hook('sls_wp_admin_toolbar_filter', '', array(&$sls_wp_admin_toolbar_array)); }

	

	foreach ($sls_wp_admin_toolbar_array as $toolbar_page) {

		$admin_bar -> add_menu($toolbar_page);

	}*/

	

} 

/*---------------------------------------------------------------*/

function sls_wp_permissions_check() {

	global $sls_wp_vars;



	if (!empty($_POST['sls_wp_folder_permission'])) {

		@array_map("chmod", $_POST['sls_wp_folder_permission'], array_fill(0, count($_POST['sls_wp_folder_permission']), 0755) );

	}

	if (!empty($_POST['sls_wp_file_permission'])) {

		

		@array_map("chmod", $_POST['sls_wp_file_permission'], array_fill(0, count($_POST['sls_wp_file_permission']), 0644) );

	}



	

	$f_to_check = array(SLS_WP_UPLOADS_PATH);

	

	foreach ($f_to_check as $slf) {

		$dir_iterator = new RecursiveDirectoryIterator($slf);

		$iterator = new RecursiveIteratorIterator($dir_iterator, RecursiveIteratorIterator::SELF_FIRST);

		$files = new RegexIterator($iterator, "/\.(php|gif|jpe?g|png|css|js|csv|xml|json|txt)/");

		

	}

	clearstatcache();

	$needs=0;

	

	foreach($iterator as $value) {

		if (is_dir($value) && 0755 !== (fileperms($value) & 0777)) {

			$needs_update["folder"][] = "$value - <b>".decoct(fileperms($value) & 0777)."</b>";

			$needs++;

		}

	}

	foreach($files as $value) {

		if (!is_dir($value) && 0644 !== (fileperms($value) & 0777)) {

			$needs_update["file"][] = "$value - <b>".decoct(fileperms($value) & 0777)."</b>";

			$needs++;

		}

	}

	

	$button_note = __("Note: Clicking this button should update permissions, however, if it doesn\'t, you may need to update permissions by using an FTP program.  Click &quot;OK&quot; or &quot;Confirm&quot; to continue ...", SLS_WP_TEXT_DOMAIN);

	

	if ($needs > 0){

		$output = "";

		print "<br><div class='sls-wp-menu-alert' style='display:none;'><b>".__("Important Note", SLS_WP_TEXT_DOMAIN).":</b><br>".__("Some of your folders / files may need updating to the proper permissions (folders: 755 / files: 644), otherwise, all functionality may not work as intended.  View folders / files below", SLS_WP_TEXT_DOMAIN)." - <a href='#' onclick='show(\"file_perm_table\"); return false;'>".__("display / hide list of folders & files", SLS_WP_TEXT_DOMAIN)."</a>:<br>

		<div style='float:right'>(<a href='".$_SERVER['REQUEST_URI']."&file_perm_msg=1'>".__("Hide This Notice Permanently", SLS_WP_TEXT_DOMAIN)."</a>)</div><br><br><table cellpadding='7px' id='file_perm_table' style='display:none;'><tr>";

	}

	if (!empty($needs_update["folder"])) {

		$output .= "<td style='vertical-align: top; width:50%'><form method='post'  onsubmit=\"return confirm('".$button_note."');\"><strong>".__("Folders", SLS_WP_TEXT_DOMAIN).":</strong><br><input type='submit' class='button-primary' value=\"".__("Update Checked Folders' Permissions", SLS_WP_TEXT_DOMAIN)."\"><br><br>";

		foreach ($needs_update["folder"] as $value) {

			$output .= "\n<input name='sls_wp_folder_permission[]' checked='checked' type='checkbox' value='".substr($value, 0, -13)."'>&nbsp;/".str_replace(ABSPATH, '', $value)."<br>"; // "-13", removes 13 chars: " - <b> 777 </b>" at end of value

		}

		$output .= "</form></td>";	

	}

	if (!empty($needs_update["file"])) {

		$output .= "<td style='vertical-align: top; style: 50%;'><form method='post' onsubmit=\"return confirm('".$button_note."');\"><strong>".__("Files", SLS_WP_TEXT_DOMAIN).":</strong><br><input type='submit' class='button-primary' value=\"".__("Update Checked Files' Permissions", SLS_WP_TEXT_DOMAIN)."\"><br><br>";

		foreach ($needs_update["file"] as $value) {

			$output .= "\n<input name='sls_wp_file_permission[]' checked='checked' type='checkbox' value='".substr($value, 0, -13)."'>&nbsp;/".str_replace(ABSPATH, '', $value)."<br>";

		}

		$output .= "</form></td>";	

	}

	if ($needs > 0){

		

		print $output."</tr></table></div>";

		$sls_wp_vars['perms_need_update'] = 1;

	}

	

	if ($needs == 0) {

		$sls_wp_vars['perms_need_update'] = 0;

	}

	

}

/*---------------------------------------------------------------*/

function sls_wp_remote_data($val_arr, $decode_mode = 'json') {

	$pagetype = (!empty($val_arr['pagetype']))? $val_arr['pagetype'] : "none" ;

	$dir = (!empty($val_arr['dir']))? $val_arr['dir'] : "none" ;

	$key = (!empty($val_arr['key']))? "__".$val_arr['key'] : "" ;

	$start = (!empty($val_arr['start']))? $val_arr['start'] : 0 ;

	$val_host = (!empty($val_arr['host']))? $val_arr['host'] : 'superlogoshowcase.net' ;

	$val_url = (!empty($val_arr['url']))? $val_arr['url'] : "/show-data/". $pagetype ."/". $dir ."$key" ."/". $start ;

	$useragent = (!empty($val_arr['ua']))? $val_arr['ua'] : "Super Logo Showcase Wordpress Plugin" ;

	

	$target = "http://" . $val_host . $val_url;

  	

	$remote_access_fail = false;

	if (extension_loaded("curl") && function_exists("curl_init")) {

    			ob_start();

    			$ch = curl_init();

    			if (!empty($useragent) && $useragent != 'none'){ curl_setopt($ch, CURLOPT_USERAGENT, $useragent); }

    			curl_setopt($ch, CURLOPT_URL,$target);

    			curl_exec($ch);

		    	$returned_value = ob_get_contents();

			

   			ob_end_clean();

		} else {

	  		$request = '';

	  		$http_request  = "GET ". $val_url ." HTTP/1.0\r\n";

			$http_request .= "Host: ".$val_host."\r\n";

			$http_request .= "Content-Type: application/x-www-form-urlencoded; charset=" . SLS_WP_BLOG_CHARSET . "\r\n";

			$http_request .= "Content-Length: " . strlen($request) . "\r\n";

			if (!empty($useragent) && $useragent != 'none'){ $http_request .= "User-Agent: $useragent\r\n"; }

			$http_request .= "\r\n";

			$http_request .= $request;

			$response = '';

			if (false != ( $fs = @fsockopen($val_host, 80, $errno, $errstr, 10) ) ) {

				fwrite($fs, $http_request);

				while ( !feof($fs) )

					$response .= fgets($fs, 1160); 

				fclose($fs);

			}

			$returned_value = trim($response);

	}

	

	if (!empty($returned_value)) {

		$the_data = ($decode_mode != "serial")? json_decode($returned_value, true) : $returned_value;

		return $the_data;

	} else {

		return false;

	}

}

/*-----------------------------------------------*/

/// Loading SL Variables ///
global $wpdb;
if(defined('SLS_WP_SETTING_TABLE') && $wpdb->get_var($wpdb->prepare("SHOW TABLES LIKE %s", SLS_WP_SETTING_TABLE)) == SLS_WP_SETTING_TABLE)
{

$sls_wp_vars=sls_wp_data('sls_wp_vars');

if (!is_array($sls_wp_vars)) {

	function sls_wp_fix_corrupted_serialized_string($string) {

		$tmp = explode(':"', $string);

		$length = count($tmp);

		for($i = 1; $i < $length; $i++) {    

			list($string) = explode('"', $tmp[$i]);

        		$str_length = strlen($string);    

        		$tmp2 = explode(':', $tmp[$i-1]);

        		$last = count($tmp2) - 1;    

        		$tmp2[$last] = $str_length;         

        		$tmp[$i-1] = join(':', $tmp2);

    		}

    		return join(':"', $tmp);

	}

	$sls_wp_vars = sls_wp_fix_corrupted_serialized_string($sls_wp_vars); 

	sls_wp_data('sls_wp_vars', 'update', $sls_wp_vars);

	$sls_wp_vars = unserialize($sls_wp_vars); 
	}
}

if (defined('SLS_WP_ADDONS_PLATFORM_FILE') && file_exists(SLS_WP_ADDONS_PLATFORM_FILE)) {

	sls_wp_initialize_variables(); 

	include_once(SLS_WP_ADDONS_PLATFORM_FILE);

}

//////



/*-----------------------------------*/

if (!function_exists("sls_wp_do_image_upload")){
	

 function sls_wp_do_image_upload($sls_wp_id="") {

	 global $wpdb, $text_domain, $sls_wp_vars;

		//edit emage code
		if(!empty($_FILES['sls_wp_image']['name'])){
			$valid_exts = array("jpg","jpeg","gif","png");
			$ext = explode(".",strtolower(trim($_FILES["sls_wp_image"]["name"])));
			$ext = end($ext);
			if(in_array($ext,$valid_exts)){
				$max_dimension = 800; // Max new width or height, can not exceed this value.
				 $dir=SLS_WP_UPLOADS_PATH."/images/".$_GET['edit'];
				 mkdir($dir, 0777, true);
				 @chmod($dir, 0777);
				$postvars = array(
				"image"    => $_GET['edit'].'.png',
				"image_tmp"    => $_FILES["sls_wp_image"]["tmp_name"],
				"image_size"    => (int)$_FILES["sls_wp_image"]["size"],
				"image_max_width"    => (int)100,
				"image_max_height"   => (int)100

				);
			
					if($ext == "jpg" || $ext == "jpeg"){
					$image = imagecreatefromjpeg($postvars["image_tmp"]);
					}

					else if($ext == "gif"){
					$image = imagecreatefromgif($postvars["image_tmp"]);
					}

					else if($ext == "png"){
					$image = imagecreatefrompng($postvars["image_tmp"]);
					}

				    $size = getimagesize($postvars["image_tmp"]);
					$ratio = $size[0]/$size[1]; // width/height

					if( $ratio > 1) {
					$width = 100;
					$height = 100/$ratio;
					$width2 = $size[0];
					$height2 = $size[0]/$ratio;
					}

					else {
					$width = 100*$ratio;
					$height = 100;
					$width2 = $size[0]*$ratio;
					$height2 = $size[0];
					}

					$tmp = imagecreatetruecolor($width,$height);
					$tmp2 = imagecreatetruecolor($width2,$height2);

					if($ext == "gif" or $ext == "png"){

						imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));
						imagealphablending($tmp, false);
						imagesavealpha($tmp, true);
						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);
						$filename = $dir.'/'.$postvars["image"];
						imagepng($tmp,$filename,9);
						imagecolortransparent($tmp2, imagecolorallocatealpha($tmp2, 0, 0, 0, 127));
						imagealphablending($tmp2, false);
						imagesavealpha($tmp2, true);
						imagecopyresampled($tmp2,$image,0,0,0,0,$width2,$height2,$size[0],$size[1]);
						$filename2 = $dir.'/ori_'.$postvars["image"];
						imagepng($tmp2,$filename2,9);

					}
					else

					{

						$whiteBackground = imagecolorallocate($tmp, 255, 255, 255); 
						imagefill($tmp,0,0,$whiteBackground); // fill the background with white
						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);
						$filename = $dir.'/'.$postvars["image"];
						imagejpeg($tmp,$filename,100);
						$whiteBackground2 = imagecolorallocate($tmp2, 255, 255, 255); 
						imagefill($tmp2,0,0,$whiteBackground2); // fill the background with white
						imagecopyresampled($tmp2,$image,0,0,0,0,$width2,$height2,$size[0],$size[1]);
						$filename2 = $dir.'/ori_'.$postvars["image"];
						imagejpeg($tmp2,$filename2,100);

					}

					imagedestroy($image);
					imagedestroy($tmp);
					imagedestroy($tmp2);

				}
			}
			
			/************custom marker edit with store image ********************************************/
			if(!empty($_FILES['sls_wp_marker']['name'])){
				$dir=SLS_WP_UPLOADS_PATH."/images/icons/".$_GET['edit'];
				 mkdir($dir, 0777, true);
				 @chmod($dir, 0777);

			$valid_exts = array("jpg","jpeg","gif","png");
            $ext = explode(".",strtolower(trim($_FILES["sls_wp_marker"]["name"])));
			$ext = end($ext);
			if(in_array($ext,$valid_exts)){

				$max_dimension = 800; // Max new width or height, can not exceed this value.

				$postvars = array(

				"image"    => $_GET['edit'].'.png',

				"image_tmp"    => $_FILES["sls_wp_marker"]["tmp_name"],

				);

				 

					if($ext == "jpg" || $ext == "jpeg"){

					$image = imagecreatefromjpeg($postvars["image_tmp"]);

					}

					else if($ext == "gif"){

					$image = imagecreatefromgif($postvars["image_tmp"]);

					}

					else if($ext == "png"){

					$image = imagecreatefrompng($postvars["image_tmp"]);

					}

					$size = getimagesize($postvars["image_tmp"]);

					$ratio = $size[0]/$size[1]; // width/height

					if( $ratio > 1) {

					$width = 700;

					$height = 700/$ratio;

					

					$width2 = $size[0];

					$height2 = $size[0]/$ratio;

					}

					else {

					$width = 700*$ratio;

					$height = 700;

					

					$width2 = $size[0]*$ratio;

					$height2 = $size[0];

					}

					

					$tmp = imagecreatetruecolor($width,$height);

					if($ext == "gif" or $ext == "png"){

						imagecolortransparent($tmp, imagecolorallocatealpha($tmp, 0, 0, 0, 127));

						imagealphablending($tmp, false);

						imagesavealpha($tmp, true);

						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);

						$filename = $dir.'/'.$postvars["image"];

						imagepng($tmp,$filename,9);

					}

					

					else

					{

						$whiteBackground = imagecolorallocate($tmp, 255, 255, 255); 

						imagefill($tmp,0,0,$whiteBackground); // fill the background with white

						imagecopyresampled($tmp,$image,0,0,0,0,$width,$height,$size[0],$size[1]);

						$filename = $dir.'/'.$postvars["image"];

						imagejpeg($tmp,$filename,100);

					}

					imagedestroy($image);

					imagedestroy($tmp);

				}

			}
	/* custom marker edit from store page */	

 }

}

/*-------------------------------*/

if (!function_exists("sls_wp_template")){

   function sls_wp_template($content) {

	global $sls_wp_dir, $sls_wp_base, $sls_wp_uploads_base, $sls_wp_path, $sls_wp_uploads_path, $text_domain, $wpdb, $sls_wp_vars;

	global $superlogoshowcase;

	if(! preg_match('|\[super-logo-showcase|i', $content)) {

		return $content;

	}
	else {
		$content=str_replace(' LAYOUT=metro','',$content);
		$content=str_replace(' LAYOUT=standard','',$content);
		$start="CAT=";
		$end="]";
		$string =' '.$content;
		$ini = strpos($content, $start);
		if ($ini == 0)
		{
			$sls_default_category= '';
		}
		else {
			$ini += strlen($start);
			$len = strpos($content, $end, $ini) - $ini;
			$sls_default_category=substr($content, $ini, $len);
		}

		$sls_default_category=trim($sls_default_category);
		if($sls_default_category!=""){
			$superlogoshowcase.="<script>
			sls_default_category='$sls_default_category';
			</script>";
		}
		
		return preg_replace("@\[super-logo-showcase(.*)?\]@i", $superlogoshowcase, $content);

		}

    }

}?>