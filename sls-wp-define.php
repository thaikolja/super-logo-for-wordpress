<?php
$sls_wp_siteurl=get_option('siteurl'); $sls_wp_blog_charset=get_option('blog_charset'); $sls_wp_admin_email=get_option('admin_email');
$sls_wp_site_name=get_option('blogname');
$sls_wp_dir=dirname(plugin_basename(__FILE__)); 
$sls_wp_pub_dir=$sls_wp_dir."/sls-wp-pub";
$sls_wp_inc_dir=$sls_wp_dir."/sls-wp-inc";
$sls_wp_admin_dir=$sls_wp_dir."/sls-wp-admin";
$sls_wp_base=plugins_url('', __FILE__); 
$sls_wp_path=substr(plugin_dir_path(__FILE__), 0, -1); 
$sls_wp_uploads=wp_upload_dir();
$sls_wp_uploads_base=$sls_wp_uploads['baseurl']."/sls-wp-uploads";
$sls_wp_upload_base=$sls_wp_uploads_base; 
$sls_wp_uploads_path=$sls_wp_uploads['basedir']."/sls-wp-uploads"; 
$sls_wp_upload_path=$sls_wp_uploads_path; 
$top_nav_base="/".substr($_SERVER["PHP_SELF"],1)."?page=";
$admin_nav_base=$sls_wp_siteurl."/wp-admin/admin.php?page="; 
$text_domain="lol";
$view_link_logo="| <a href='".$admin_nav_base.$sls_wp_admin_dir."/pages/logos.php'>".__(" View Logo List", $text_domain)."</a> <script>setTimeout(function(){jQuery('.sls_wp_admin_success').fadeOut('slow');}, 6000);</script>";
$view_link_state="| <a href='".$admin_nav_base.$sls_wp_admin_dir."/pages/states.php'>".__(" View Category List", $text_domain)."</a> <script>setTimeout(function(){jQuery('.sls_wp_admin_success').fadeOut('slow');}, 6000);</script>";
$web_domain=str_replace("www.","",$_SERVER['HTTP_HOST']);

		define('SLS_WP_SITEURL', $sls_wp_siteurl); define('SLS_WP_BLOG_CHARSET', $sls_wp_blog_charset); define('SLS_WP_ADMIN_EMAIL', $sls_wp_admin_email); define('SLS_WP_SITE_NAME', $sls_wp_site_name);
		define('SLS_WP_DIR', $sls_wp_dir);
		define('SLS_WP_PUB_DIR', $sls_wp_dir);
		define('SLS_WP_CSS_DIR', SLS_WP_PUB_DIR."/css");

		define('SLS_WP_JS_DIR', SLS_WP_PUB_DIR."/js");

		define('SLS_WP_IMAGES_DIR_ORIGINAL', SLS_WP_PUB_DIR."/images");
		define('SLS_WP_INC_DIR', $sls_wp_inc_dir);
		define('SLS_WP_ACTIONS_DIR', SLS_WP_INC_DIR."/actions");
		define('SLS_WP_INCLUDES_DIR', SLS_WP_INC_DIR."/includes");
		define('SLS_WP_ADMIN_DIR', $sls_wp_admin_dir);
		define('SLS_WP_INFO_DIR', SLS_WP_ADMIN_DIR."/info");
		define('SLS_WP_PAGES_DIR', SLS_WP_ADMIN_DIR."/pages");

		define('SLS_WP_ADDONS_DIR_ORIGINAL', SLS_WP_ADMIN_DIR."/addons");
		define('SLS_WP_LANGUAGES_DIR_ORIGINAL', SLS_WP_ADMIN_DIR."/languages");
		define('SLS_WP_THEMES_DIR_ORIGINAL', SLS_WP_ADMIN_DIR."/themes");
		define('SLS_WP_BASE', $sls_wp_base);
		define('SLS_WP_PUB_BASE', SLS_WP_BASE);
		define('SLS_WP_CSS_BASE', SLS_WP_PUB_BASE."/css");

		define('SLS_WP_JS_BASE', SLS_WP_PUB_BASE."/js");

		define('SLS_WP_IMAGES_BASE_ORIGINAL', SLS_WP_PUB_BASE."/images");
		define('SLS_WP_INC_BASE', SLS_WP_BASE."/sls-wp-inc");
		define('SLS_WP_ACTIONS_BASE', SLS_WP_INC_BASE."/actions");
		define('SLS_WP_INCLUDES_BASE', SLS_WP_INC_BASE."/includes");
		define('SLS_WP_ADMIN_BASE', SLS_WP_BASE."/sls-wp-admin");
		define('SLS_WP_INFO_BASE', SLS_WP_ADMIN_BASE."/info");
		define('SLS_WP_PAGES_BASE', SLS_WP_ADMIN_BASE."/pages");

		define('SLS_WP_ADDONS_BASE_ORIGINAL', SLS_WP_ADMIN_BASE."/addons");
		define('SLS_WP_LANGUAGES_BASE_ORIGINAL', SLS_WP_ADMIN_BASE."/languages");
		define('SLS_WP_THEMES_BASE_ORIGINAL', SLS_WP_ADMIN_BASE."/themes");
		define('SLS_WP_PATH', $sls_wp_path);
		define('SLS_WP_PUB_PATH', SLS_WP_PATH);
		define('SLS_WP_CSS_PATH', SLS_WP_PUB_PATH."/css");

		define('SLS_WP_JS_PATH', SLS_WP_PUB_PATH."/js");

		define('SLS_WP_IMAGES_PATH_ORIGINAL', SLS_WP_PUB_PATH."/images");
		define('SLS_WP_INC_PATH', SLS_WP_PATH."/sls-wp-inc");
		define('SLS_WP_ACTIONS_PATH', SLS_WP_INC_PATH."/actions");
		define('SLS_WP_INCLUDES_PATH', SLS_WP_INC_PATH."/includes");
		define('SLS_WP_ADMIN_PATH', SLS_WP_PATH."/sls-wp-admin");
		define('SLS_WP_INFO_PATH', SLS_WP_ADMIN_PATH."/info");
		define('SLS_WP_PAGES_PATH', SLS_WP_ADMIN_PATH."/pages");

		define('SLS_WP_ADDONS_PATH_ORIGINAL', SLS_WP_ADMIN_PATH."/addons");
		define('SLS_WP_LANGUAGES_PATH_ORIGINAL', SLS_WP_ADMIN_PATH."/languages");
		define('SLS_WP_THEMES_PATH_ORIGINAL', SLS_WP_ADMIN_PATH."/themes");
		define('SLS_WP_UPLOADS_BASE', $sls_wp_uploads_base);
		define('SLS_WP_UPLOADS_PATH', $sls_wp_uploads_path);
		define('SLS_WP_TOP_NAV_BASE', $top_nav_base);
		define('SLS_WP_ADMIN_NAV_BASE', $admin_nav_base);
		define('SLS_WP_TEXT_DOMAIN', $text_domain);
		define('SLS_WP_VIEW_LINK', $view_link_logo);
		define('SLS_WP_WEB_DOMAIN', $web_domain);

		define('SLS_WP_ADDONS_BASE', SLS_WP_UPLOADS_BASE."/addons");
		define('SLS_WP_CACHE_BASE', SLS_WP_UPLOADS_BASE."/cache");
		define('SLS_WP_CUSTOM_CSS_BASE', SLS_WP_UPLOADS_BASE."/custom-css");

		define('SLS_WP_IMAGES_BASE', SLS_WP_UPLOADS_BASE."/images");
		define('SLS_WP_LANGUAGES_BASE', SLS_WP_UPLOADS_BASE."/languages");
		define('SLS_WP_THEMES_BASE', SLS_WP_UPLOADS_BASE."/themes");

		define('SLS_WP_ADDONS_PATH', SLS_WP_UPLOADS_PATH."/addons");
		define('SLS_WP_CACHE_PATH', SLS_WP_UPLOADS_PATH."/cache");
		define('SLS_WP_CUSTOM_CSS_PATH', SLS_WP_UPLOADS_PATH."/custom-css");

		define('SLS_WP_IMAGES_PATH', SLS_WP_UPLOADS_PATH."/images");
		define('SLS_WP_LANGUAGES_PATH', SLS_WP_UPLOADS_PATH."/languages");
		define('SLS_WP_THEMES_PATH', SLS_WP_UPLOADS_PATH."/themes");

		define('SLS_WP_INFORMATION_PAGE', SLS_WP_TOP_NAV_BASE.SLS_WP_PAGES_DIR."/quickstart.php");
		define('SLS_WP_MANAGE_LOCATIONS_PAGE', SLS_WP_TOP_NAV_BASE.SLS_WP_PAGES_DIR."/logos.php");
		define('SLS_WP_ADD_LOCATIONS_PAGE', SLS_WP_MANAGE_LOCATIONS_PAGE."&pg=add-logo");
		define('SLS_WP_MANAGE_STATE_PAGE', SLS_WP_TOP_NAV_BASE.SLS_WP_PAGES_DIR."/states.php");
		define('SLS_WP_ADD_STATES_PAGE', SLS_WP_MANAGE_STATE_PAGE."&pg=add-state");
		define('SLS_WP_IMPORT_PAGE', SLS_WP_TOP_NAV_BASE.SLS_WP_PAGES_DIR."/import.php");
		define('SLS_WP_SETTINGS_PAGE1', SLS_WP_TOP_NAV_BASE.SLS_WP_PAGES_DIR."/settings.php");
		define('SLS_WP_SETTINGS_PAGE', SLS_WP_SETTINGS_PAGE1); 

		define('SLS_WP_PARENT_PAGE', SLS_WP_INFORMATION_PAGE); 
		define('SLS_WP_PARENT_URL', preg_replace("@".preg_quote(SLS_WP_TOP_NAV_BASE)."@", "",SLS_WP_PARENT_PAGE)); 

$sls_wp_aps=glob(SLS_WP_ADDONS_PATH.'/*addons-platform*', GLOB_NOSORT); 
if (!empty($sls_wp_aps)){
	$sls_wp_addons_platform_dir = basename(current($sls_wp_aps));
	foreach ($sls_wp_aps as $sls_wp_ap_path) {
		if (file_exists($sls_wp_ap_path.'/'.basename($sls_wp_ap_path).'.php')) {
			$sls_wp_addons_platform_dir = basename($sls_wp_ap_path);
			break;
		} 
	}
	define('SLS_WP_ADDONS_PLATFORM_DIR', $sls_wp_addons_platform_dir);
	define('SLS_WP_ADDONS_PLATFORM_PATH', SLS_WP_ADDONS_PATH.'/'.SLS_WP_ADDONS_PLATFORM_DIR );
	define('SLS_WP_ADDONS_PLATFORM_BASE', SLS_WP_ADDONS_BASE.'/'.SLS_WP_ADDONS_PLATFORM_DIR );
	define('SLS_WP_ADDONS_PLATFORM_FILE', SLS_WP_ADDONS_PLATFORM_PATH.'/'.SLS_WP_ADDONS_PLATFORM_DIR.'.php');
	
}

?>