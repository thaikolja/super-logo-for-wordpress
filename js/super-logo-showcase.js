jQuery(function() {
	
	if(document.getElementById('sls_main_body')==null){
		return;
	}

    var d = new Date();
    var baseURL = '', 
        pageSize = 15,
        currentPage = -1,

        hashState = {},
        newHashState = getHashState(),
        $filterRegionRadios,
        $filterCategoriesRadios,
        regions = {},
        logosData = {},
        logos = [],
        categories = {},
        isMediumScreen;

    jQuery.ajax({
        type: 'GET',
        url: sls_wp_base + '/sls-wp-xml.php?t='+d.getTime(),
        dataType: 'xml',
        success: function(data) {
            /*---- Organise all the info first ----*/
            jQuery(data).find('logo').each(function() {
                var $thisLogo = jQuery(this),
                    $children = $thisLogo.children(),
                    logoId = getText($thisLogo.find('id'));

                if($children.length <= 4) {
                    if(!logosData[logoId]) return;

                    logosData[logoId].nearbyOutlets.push(getText($thisLogo.find('nearbyOutlets')));
                    logosData[logoId].contactNum.push(getText($thisLogo.find('contactNum')));
                } else {
                    var logoRegions = getText($thisLogo.find('region')).split('\n'),
                        logoCategory = getText($thisLogo.find('category')),
                        filteredLogoCategoryStr = filterText(logoCategory);
                    if(typeof categories[filteredLogoCategoryStr] === 'undefined' && logoCategory!='') {
                        categories[filteredLogoCategoryStr] = logoCategory;
                    }

                    logosData[logoId] = new Logo({
                        id : logoId,
						imgid : getText($thisLogo.find('imgid')),
						banner : getText($thisLogo.find('banner')),
                        name : getText($thisLogo.find('name')),
                        features : getText($thisLogo.find('features')),
                        nearbyOutlets : [getText($thisLogo.find('nearbyOutlets'))],
                        contactNum : [getText($thisLogo.find('contactNum'))],
                        info : getText($thisLogo.find('info')),
						extUrl : getText($thisLogo.find('externalurl')),
						popup : getText($thisLogo.find('popup')),
						media : getText($thisLogo.find('defaultmedia')),
                        additionalInfo : getText($thisLogo.find('additionalInfo'))
                    });

                    var filteredRegionStr;

                    for(var i = 0, ii = logoRegions.length; i < ii; i++) {
                        filteredRegionStr = filterText(logoRegions[i]);

                        if(typeof regions[filteredRegionStr] === 'undefined') {
                            regions[filteredRegionStr] = { name : logoRegions[i] };
                        }

                        if(typeof regions[filteredRegionStr][filteredLogoCategoryStr] === 'undefined') {
                            regions[filteredRegionStr][filteredLogoCategoryStr] = [logoId];
                        } else {
                            regions[filteredRegionStr][filteredLogoCategoryStr].push(logoId);
                        }
                    }
                }
            });

            var regionsArr = [];

            jQuery.each(regions, function(key, value) {
                if(key === 'Nationwide'
                || key === 'KualaLumpurSelangor') return;

                regionsArr.push({
                    name : value.name,
                    id : key
                });
            });

            regionsArr.sort(function(a, b) {
                a = a.name.toLowerCase();
                b = b.name.toLowerCase();

                return (a < b) ? -1 : 1;
            });

            

            var categoriesArr = [];

            jQuery.each(categories, function(key, value) {
                if(key === 'Others') return;

                categoriesArr.push({
                    id : key,
                    name : value
                });
            });

            categoriesArr.sort(function(a, b) {
                a = a.id.toLowerCase();
                b = b.id.toLowerCase();
                return (a < b) ? -1 : 1;
            });

            categoriesArr.push({
                id : 'Others',
                name : categories.Others
            });


            /*---- Populate radio options for regions ----*/


            var regionsStrArray = [];

            for(var i = 0, ii = regionsArr.length; i < ii; i++) {
                regionsStrArray.push([
                    '<label class="sls-label">',
                        '<input',
                            ' type="radio"',
                            ' name="region"',
                            (newHashState.region === regionsArr[i].id
                            || newHashState.region === '' && i === 0)
                                ? ' checked="true"' : '',
                            ' value="', regionsArr[i].id, '"',
                        ' /> ',
                        regionsArr[i].name,
                    '</label>'
                ].join(''));
            }
                
            $filterRegionRadios = jQuery('#filterRegions').html(regionsStrArray.join('')).find('input[type="radio"]').inputify();
			
            $filterRegionRadios.on('change', function() {
                if(!jQuery(this).is(':checked')) return;

                if(!isMediumScreen) {
                    jQuery('#slsApplyFilterOptions').click();
                }
            });


            /*---- radio button for categories ----*/


            var categoryStrArray = [
                ['<label class="space-top sls-active">',
                    '<input',
                        ' type="radio"',
                        ' name="category"',
                        (newHashState.category === '')
                            ? ' checked="true"' : '',
                        ' value=""',
                        ' class="js-inputify"',
                    ' /> ',
                    'All',
                '</label>'].join('')
            ];

            for(var j = 0, jj = categoriesArr.length-1; j < jj; j++) {
				if(categoriesArr[j].id!='default'){
                categoryStrArray.push([
                    '<label  class="sls-label">',
                        '<input',
                            ' type="radio"',
                            ' name="category"',
                            (newHashState.category === categoriesArr[j].id)
                                ? ' checked="true"' : '',
                            ' value="', categoriesArr[j].id, '"',
                            ' class="js-inputify"',
                        ' /> ',
                        categoriesArr[j].name,
                    '</label>'
                ].join(''));
				}
            }
            if(sls_layout!=2){
				$filterCategoriesRadios = jQuery('#filterCategories').html(categoryStrArray.join('')).find('input[type="radio"]').inputify();
			}else{
				$filterCategoriesRadios = jQuery('#filterCategories').html(categoryStrArray.join(''));
			}
			if(sls_default_category!='' && sls_default_category!=undefined)	
			{
			sls_default_category=filterText(sls_default_category);
			sls_default_category=sls_default_category.replace("#038;", "");
				jQuery(function() {
				setTimeout(function(){
				     
					jQuery('#filterCategories input[value='+sls_default_category+']').trigger('click');
					jQuery('#sls-category-list .filter__toggler').trigger('click');
					jQuery('#slsApplyFilterOptions').click();
					},200);
				});
			}
		

            /*---- radio button for categories ----*/


            reflectHashState('first-time');
            attachEventHandlers();
        },
        error: function(d) {
            console.log(d);
        }
    });

    function attachEventHandlers() {
        jQuery('#filterRegions, #slsApplyFilterOptions').on('click', function(e) {
            e.preventDefault();

            var hashStateObj = { logo : '' },
                hashLabels = ['region', 'category'];

            jQuery('#filterRegions, #filterCategories').each(function(index) {
                var $checkedRadio = jQuery(this).find('input[type="radio"]:checked');
                hashStateObj[hashLabels[index]] = ($checkedRadio.length) ? $checkedRadio.val() : '';
            });
            if(isSameHashState(hashStateObj)) {
                reflectHashState();
            } else {
                pushHashState(hashStateObj);
            }

            scrollToIfNeeded(jQuery('#logosCatalogue'));
        });
		
		
		jQuery('#filterCategories label').on('click', function(e) {
			if(sls_layout!=2){
				return;
			}
			jQuery('body').removeClass('filter-popup-is-shown');
			e.preventDefault();
			jQuery('#filterCategories label').removeClass('sls-active');
			jQuery(this).addClass('sls-active');
			jQuery(this).children("input:radio[name=category]").prop('checked', true);
			var hashStateObj = { logo : '' },
				hashLabels = ['category'];

			jQuery('#filterCategories').each(function(index) {
				var $checkedRadio = jQuery(this).find('input[type="radio"]:checked');
				hashStateObj[hashLabels[index]] = ($checkedRadio.length) ? $checkedRadio.val() : '';
			});
			if(isSameHashState(hashStateObj)) {
				reflectHashState();
			} else {
				pushHashState(hashStateObj);
			}

			scrollToIfNeeded(jQuery('#logosCatalogue'));
		});

        jQuery('#showAllLogos').on('click', function(e) {
		
		jQuery('.space-top:first').trigger('click');
		jQuery('#slsApplyFilterOptions').trigger('click');

            e.preventDefault();
            if(jQuery(this).hasClass('is-disabled')) return;
            showAllLogos();
        });

        var $loadMore = jQuery('#loadMoreLogos');

        $loadMore.on('click', function(e) {
            e.preventDefault();
            addLogos(currentPage+1);
        });

        jQuery(window).on('scroll', function() {
            if($loadMore.css('display') !== 'none'
            && jQuery(window).scrollTop() > $loadMore.offset().top - jQuery(window).height()*1.5) {
                addLogos(currentPage+1);
            }
        });
    }

    function getHashState() {
        var hash = decodeURI(window.location.hash),
            hashStateObj = {
                region : '',
                category : '',
                logo : ''
            };

        if(hash === '' || hash === '#/') return hashStateObj;

        var hashSplit = hash.substr(2).split('&'),
            tempArray;

        while(hashSplit.length) {
            tempArray = hashSplit.shift().split('=');
            hashStateObj[tempArray[0]] = tempArray[1];
        }

        return hashStateObj;
    }

    function pushHashState(stateObj) {
        var hashStateObj = jQuery.extend({}, hashState),
            hashStrArray = [];

        jQuery.extend(hashStateObj, stateObj);

        jQuery.each(hashStateObj, function(key, value) {
            if(value === '') return;
            hashStrArray.push(key + '=' + encodeURIComponent(value));
        });

        window.location.hash = '/' + hashStrArray.join('&');
    }

    function isSameHashState(stateObj) {
        var same = true;

        jQuery.each(stateObj, function(key, value) {
            if(hashState[key] !== value) {
                same = false;
                return false;
            }
        });

        return same;
    }

    function reflectHashState(init) {
        if(init) {
            setTimeout(function() {
                jQuery(window).on('hashchange', function() {
                    reflectHashState();
                });
            }, 25);
        }

        newHashState = getHashState();
        var hasChanged = {
                region : hashState.region !== newHashState.region,
                category : hashState.category !== newHashState.category,
                logo : hashState.logo !== newHashState.logo
            };

        if(hasChanged.region || hasChanged.category) {
            populateLogos();
            reflectLogo();
        } else {
            reflectLogo();
        }

        hashState = newHashState;
    }

    function populateLogos() {
        var targetRegion = newHashState.region,
            logoIDs = [];

        logos = [];

        if(targetRegion === '') {
            targetRegion = [];

            jQuery.each(regions, function(key, value) {
                targetRegion.push(value);
            });
        } else if(regions[targetRegion]) {
            targetRegion = [regions[targetRegion]];
        }

        var categoryID = newHashState.category,
            targetCategory,
            currentTargetRegion;

        while(targetRegion.length) {
            currentTargetRegion = targetRegion.shift();
            targetCategory = currentTargetRegion[categoryID];

            if(targetCategory) {
                for(var i = 0, ii = targetCategory.length; i < ii; i++) {
                    logos.push(targetCategory[i]);
                }
            } else if(categoryID === '') {
                jQuery.each(currentTargetRegion, function(key, value) {
                    if(key === 'name') return;
                    logos = logos.concat(value);
                });
            }
        }

        /* Sort alphabetically */
        logos.sort(function(a, b) {
           a = parseInt(a);
           b = parseInt(b);
           return (a < b) ? -1 : 1;
        });

        /* Remove duplicates */
        logos = logos.reduce(function(a, b) {
            if(a.indexOf(b) < 0) a.push(b);
            return a;
        }, []);

        currentPage = -1;
        jQuery('#logosCatalogue').empty();

        if(logos.length) {
            addLogos(currentPage+1);
        } else {
            jQuery('#currentLogosCount').text('0');
            jQuery('#logosCatalogue').html('<div class="text-center h4 title space-top medium-space-top-3x">',no_brands_found,'</div>');
        }

        jQuery('#totalLogosCount').text(logos.length);

        checkLoadMoreLogos();
        checkShowAllLogos();
    }

    function reflectLogo() {
        var toggleOffLogos = function() {
                jQuery('.item__expandable-contents-toggler').togglerify('toggleOff');
            };

        
        if(newHashState.logo === '') {
            toggleOffLogos();
        } else {
            var $logo = jQuery('#' + newHashState.logo),
                toggleOnLogo = function() {
                    scrollToIfNeeded($logo);
                    $logo.togglerify('toggleOn');
                }

            /* Toggle matching logo. */
            if($logo.length) {
                toggleOnLogo();
            } else {
                var targetIndex = 'nope';

                
                for(var i = 0, ii = logos.length; i < ii; i++) {
                    if(logos[i] === newHashState.logo) {
                        targetIndex = i;
                        break;
                    }
                }

                if(targetIndex === 'nope') {
                   
                    toggleOffLogos();
                } else {
                    /* Logo found in deeper page; advance the pagination
                     * to this page and then toggle the target logo. */
                    var targetPage = Math.ceil((targetIndex+1) / pageSize, 10) - 1;
                    addLogos(targetPage);

                    addLogos(targetPage, function() {
                        $logo = jQuery('#' + newHashState.logo);
                        toggleOnLogo();
                    });
                }
            }
        }
    }

    function scrollToIfNeeded($obj) {
        var targetScrollPos = $obj.offset().top - jQuery('#mainNav').height(),
            $targetContent = jQuery('.item.is-toggled');

        if($targetContent.length
        && $targetContent.offset().top < $obj.offset().top) {
            targetScrollPos -= $targetContent.find('.item__expandable-contents').height() + 15;
        }
       if(sls_layout!=2){
			//jQuery('html, body').animate({ scrollTop : targetScrollPos }, 400);
		}
    }

    function Logo(settings) {
        jQuery.extend(this, settings);
    }

    jQuery.extend(Logo.prototype, {
        getfeaturesStr : function() {
            return (this.features) ? [
                '<div class="details-group sls-details-group--with-icon sls-icon-tick">',
                    '<div class="details__title">',features_label,'</div>',
                    '<div class="details__content">',
                            markdownStr(splitSymbolLinesIntoTag(this.features).replace(/\n/g, '<br />')),
                    '</div>',
                '</div>'
            ].join('') : '';
        },

        getnearbyOutletsStr : function() {
            var instance = this,
                outletStrArray = [],
                tableAddressArray = [];

            for(var i = 0, ii = instance.nearbyOutlets.length; i < ii; i++) {
                if(!instance.nearbyOutlets[i]) continue;

                if(instance.nearbyOutlets[i].indexOf('http://') > -1) {
                    outletStrArray.push(instance.getOutletLinkStr(i));
                } else {
                    tableAddressArray.push({
                        outlet : instance.nearbyOutlets[i],
                        contactNum : instance.contactNum[i]
                    });
                }
            }

            if(tableAddressArray.length) {
                outletStrArray.push(instance.getOutletAddressTableStr(tableAddressArray));
            }

            return (outletStrArray.length) ? [
                '<div class="details-group sls-details-group--with-icon sls-icon-globe">',
                    '<div class="details__title">Nearby outlets</div>',
                    '<div class="details__content">',
                        outletStrArray.join(''),
                    '</div>',
                '</div>'
            ].join('') : '';
        },

        getOutletAddressTableStr : function(addressesArray) {
            var hasContact = false,
                addressTableStrArray = [];

            for(var i = addressesArray.length-1; i > -1; i--) {
                if(addressesArray[i].contactNum) {
                    hasContact = true;
                    break;
                }
            }

            addressTableStrArray.push([
                '<table class="default-tbl default-tbl--zebra-columns default-tbl--small space-top">',
                    '<tr>',
                        '<th class="column-header">Location</th>',
                        (hasContact) ? '<th class="column-header">Contact Details</th>' : '',
                    '</tr>'
            ].join(''));

            var addressObj;
            while(addressesArray.length) {
                addressObj = addressesArray.shift();

                addressTableStrArray.push([
                    '<tr>',
                        '<td>' + addressObj.outlet.replace(/\n/g, '<br />') + '</td>',
                        (hasContact)
                            ? (addressObj.contactNum)
                                ? '<td>' + addressObj.contactNum.replace(/\n/g, '<br />') + '</td>'
                                : '<td>&nbsp;</td>'
                            : '',
                    '</tr>'
                ].join(''));
            }

            addressTableStrArray.push('</table>');

            return addressTableStrArray.join('');
        },

        getOutletLinkStr : function(index, linksArray) {
            var linksArray = splitSymbolLines(this.nearbyOutlets[index]),
                linkPairValArray,
                outletLinkStrArray = [];

            while(linksArray.length) {
                linkPairValArray = getBracketPairValues(linksArray.shift());

                outletLinkStrArray.push([
                    '<a href="', linkPairValArray[1], '" target="_blank">',
                        linkPairValArray[0],
                    '</a>'
                ].join(''));                
            }

            return outletLinkStrArray.join('<br />');
        },

        getAdditionalInfoStr : function() {
            var instance = this,
                additionalInfo = instance.additionalInfo;
            if(!additionalInfo) return '';

            var infoArray = splitSymbolLines(additionalInfo),
                info,
                infoPairValue,
                iconClass,
                infoStrArray = [];

            while(infoArray.length) {
                info = infoArray.shift();
                infoPairValue = getBracketPairValues(info);

                if(infoPairValue[0].toLowerCase().indexOf('contact') > -1
                && infoPairValue[0].toLowerCase().indexOf('number') > -1) {
                    iconClass = 'sls-icon-phone';
                } else if(infoPairValue[1].indexOf('http://') === 0) {
                    iconClass = 'sls-icon-globe';
                } else {
                    iconClass = 'sls-icon-tick';
                }

                infoStrArray.push([
                    '<div class="details-group sls-details-group--with-icon ', iconClass, '">',
                        '<div class="details__title">', infoPairValue[0], '</div>',
                        '<div class="details__content">',
                            (infoPairValue[1].indexOf('http://') === 0)
                            ? '<a href="' + infoPairValue[1] + '" target="_blank">' + infoPairValue[1] + '</a>'
                            : infoPairValue[1].replace(/\n/g, '<br />'),
                        '</div>',
                    '</div>'
                ].join(''));
            }

            return (infoStrArray.length) ? infoStrArray.join('') : '';
        },

        getadditionalInfoStr : function() {
            if(!this.additionalInfo) return '';

            var splitStr = this.additionalInfo.split('\n'),
                finalStr = [
                    '<div class="details-group sls-details-group--with-icon sls-icon-ribbon">',
                        '<div class="details__title">',additional_info_label,'</div>',
                        '<div class="details__content">',
                            '', 
                        '</div>',
                    '</div>'
                ],
                listOpen = false;

            for(var i = 0, ii = splitStr.length; i < ii; i++) {
                if(splitStr[i].substr(0,2) === '` ') {
                    if(!listOpen) {
                        listOpen = true;
                        finalStr[3] += '<ul class="notes-list">';
                    }

                    finalStr[3] += ['<li>', splitStr[i].substring(2), '</li>'].join('');
                } else {
                    if(listOpen) {
                        listOpen = false;
                        finalStr[3] += '</ul>';
                    }

                    finalStr[3] += ['<div>', splitStr[i], '</div>'].join('');
                }
            }

            if(listOpen) {
                finalStr[3] += '</ul>';
            }

            return finalStr.join('');
        },

        getinfoStr : function() {
			if(!this.info) return '';
            return (this.info) ? [
                '<div class="details-group sls-details-group--with-icon sls-icon-star">',
                    '<div class="details-group__block">',
                        '<div class="details__title">',information_label,'</div>',
                        '<div class="details__content">',
                                markdownStr(splitSymbolLinesIntoTag(this.info).replace(/\n/g, '<br />')),
                        '</div>',
                    '</div>',
                '</div>',
            ].join('') : '';
        },
    
						
		
        joinStr : function() {
            var instance = this;
			var toggler;
			var callUrlEnd='';
			var callUrl='';
				if(instance.media=='' || instance.media=='slidedown')
			{
				toggler='item__expandable-contents-toggler';
			}
			else { 
				toggler='';
				 if(instance.extUrl!='' && (instance.popup=='yes' || instance.popup==''))
			{
				callUrl='<a target="new" href="'+instance.extUrl.replace(/(http:\/\/)\1/, '$1')+'">';
				callUrlEnd='</a>'
			} 
			 else if(instance.extUrl!='' && instance.popup=='no'){
				 callUrl='<a  href="'+instance.extUrl.replace(/(http:\/\/)\1/, '$1')+'">';
				 callUrlEnd='</a>'
			   }
			 }
			 var logobanner='';
			 if(instance.banner!="")
			 {
				logobanner='<img src="'+instance.banner+'" alt="" class="space-bottom" />';
			 }
			 
			 if(sls_layout==2){
				 return [
                '<div class="xsmall-12 small-6 medium-4 large-',sls_column,' sls-column', (instance.clearClasses) ? ' ' + instance.clearClasses : '', '">',
                    '<span class="item item--alt-layout" id="logoshowcase_',instance.id,'">',
                        '<div class="item__contents item__contents--align-top ',toggler,'" id="', instance.id, '">',
							callUrl,'<center><img src="', instance.imgid,'" alt="" class="item__visual" /></center>',callUrlEnd,
                        '</div>',
						
						'<div class="item__expandable-contents togglerify-slider">',
							'<div class="pad">',
								'<center>',logobanner,'</center>',
								instance.getinfoStr(),
								instance.getfeaturesStr(),
								instance.getnearbyOutletsStr(),
								instance.getAdditionalInfoStr(),
								instance.getadditionalInfoStr(),
							'</div>',
						'</div>',
                    '</span>',
                '</div>'
				].join('');
			 }else{
			   return [
					'<div class="xsmall-12 small-6 large-4 sls-column', (instance.clearClasses) ? ' ' + instance.clearClasses : '', '">',
						'<span class="item item--alt-layout" id="logoshowcase_',instance.id,'">',
							'<div class="item__contents item__contents--align-top ',toggler,'" id="', instance.id, '">',
								'<h2 class="item__title">', instance.name, '</h2>',
								callUrl,'<center><img src="', instance.imgid,'" alt="" class="item__visual" /></center>',callUrlEnd,
							'</div>',
							
							'<div class="item__expandable-contents togglerify-slider">',
								'<div class="pad">',
									'<center>',logobanner,'</center>',
									instance.getinfoStr(),
									instance.getfeaturesStr(),
									instance.getnearbyOutletsStr(),
									instance.getAdditionalInfoStr(),
									instance.getadditionalInfoStr(),
								'</div>',
							'</div>',
							
						'</span>',
					'</div>'
				].join('');
			 }
        }
    });

    function getText($obj) {
        if(!$obj.length) return false;
        return jQuery.trim($obj.text());
    }

    function splitLinesIntoTag(str, tag) {
        var openTag,
            closeTag,
            strArray,
            resultStrArray = [];

        switch(tag) {
            case undefined:
                openTag = closeTag = '';
            break;

            case 'br':
                openTag = '';
                closeTag = '<br />';
            break;

            default:
                openTag = '<' + tag + '>';
                closeTag = '</' + tag + '>';
            break;
        }

        if(typeof str === 'string') {
            strArray = str.split('\n');
        } else if(typeof str === 'object' && str instanceof Array) {
            strArray = str;
        }

        while(strArray.length) {
            resultStrArray.push([
                openTag, strArray.shift(), closeTag
            ].join(''));
        }

        return resultStrArray.join('');
    }

    function splitSymbolLines(str) {
        var strArray = str.split('\n` ');

        for(var i = strArray.length-1; i > -1; i--) {
            if(strArray[i].indexOf('` ') === 0) {
                strArray[i] = strArray[i].substr(2);
            }
        }

        return strArray;
    }

    function splitSymbolLinesIntoTag(str, tag) {
        return splitLinesIntoTag(splitSymbolLines(str), tag);
    }

    function getBracketPairValues(str) {
        var val1 = jQuery.trim(str.substring(str.indexOf('[')+1, str.lastIndexOf(']'))),
            val2 = jQuery.trim(str.substring(str.indexOf('(')+1, str.lastIndexOf(')')));

        return [val1, val2];
    }

    function markdownStr(str) {
        var boldMatchArray = str.match(/\*\*(.*?)\*\*/g),
            tempMatchStr,
            tempStrArray = [];

        if(boldMatchArray !== null) {
            while(boldMatchArray.length) {
                tempMatchStr = boldMatchArray.shift();
                tempStrArray = [];
                tempStrArray.push(str.substring(0, str.indexOf(tempMatchStr)));
                tempStrArray.push(str.substring(tempStrArray[0].length + tempMatchStr.length, str.length));
                str = tempStrArray[0] + '<strong>' + tempMatchStr.substring(2, tempMatchStr.length-2) + '</strong>' + tempStrArray[1];
            }
        }

        var hrefTextMatchArray = str.match(/\[(.*?)\]/g);
        if(hrefTextMatchArray !== null) {
            var textLink,
                tempLinkStr,
                tempLinkMatch;

            while(hrefTextMatchArray.length) {
                tempMatchStr = hrefTextMatchArray.shift();
                tempStrArray = [];
                tempStrArray.push(str.substring(0, str.indexOf(tempMatchStr)));

                tempLinkStr = jQuery.trim(str.substring(tempStrArray[0].length + tempMatchStr.length, str.length));
                tempLinkMatch = tempLinkStr.match(/\((.*?)\)/g);

                if(tempLinkMatch === null) {
                    continue;
                } else {
                    tempStrArray.push(tempLinkStr.substring(tempLinkMatch[0].length, tempLinkStr.length));
                    str = tempStrArray[0] + '<a href="' + tempLinkMatch[0].substring(1,tempLinkMatch[0].length-1)  + '">' + tempMatchStr.substring(1, tempMatchStr.length-1) + '</a>' + tempStrArray[1];
                }
            }
        }


        return str;
    }

    function filterText(str) {
        return str.replace(/\s|&/g, '');
    }

    function addLogos(targetPage, callback) {
        var appendStrArray = [],
            targetLogos = logos.slice((currentPage+1)*pageSize, (targetPage+1)*pageSize),
            indexOffset = (currentPage+1)*pageSize,
            tempLogoData,
            clearClasses = [];
			var columns=12/sls_column;
           for(var i = 0, ii = targetLogos.length; i < ii; i++) {
            tempLogoData = logosData[targetLogos[i]];
            clearClasses = [];
			if(sls_layout==2){
				if((indexOffset + i) % 3 === 0) clearClasses.push('sls_small-to-medium-clear-left');
				if((indexOffset + i) % 2 === 0) clearClasses.push('small-only-clear-left');
				if((indexOffset + i) % columns === 0) clearClasses.push('large-clear-left');
			}else{
				if((indexOffset + i) % 2 === 0) clearClasses.push('sls_small-to-medium-clear-left');
				if((indexOffset + i) % 3 === 0) clearClasses.push('large-clear-left');
			}

            tempLogoData.clearClasses = clearClasses.join(' ');
            appendStrArray.push(tempLogoData.joinStr());
        }

        var syncTimer,
            $catalogueParent = jQuery('#logosCatalogue').parent(),
            heightSyncSelectors = $catalogueParent.data('sync-height-selector');

        jQuery('#logosCatalogue')
            .append(appendStrArray.join(''))
            .find('img')
                .not('.js-init-logos-img')
                    .on({
                        error : function() {
                            jQuery(this).remove();

                            clearTimeout(syncTimer);

                            syncTimer = setTimeout(function() {
                                $catalogueParent.heightSyncify('sync');
                            }, 50);
                        }
                    });

        if(typeof heightSyncSelectors === 'string'
        && heightSyncSelectors !== '') {
            heightSyncSelectors = heightSyncSelectors.split(',');

            for(var i = heightSyncSelectors.length-1; i > -1; i--) {
                heightSyncSelectors[i] = jQuery.trim(heightSyncSelectors[i]);
            }

            $catalogueParent.heightSyncify({
                items : heightSyncSelectors
            });
        }

        initTogglers();

        var logosCount = (targetPage+1)*pageSize;
        if(logosCount > logos.length) logosCount = logos.length;
        jQuery('#currentLogosCount').text(logosCount);

        currentPage = targetPage;

        checkLoadMoreLogos();
        checkShowAllLogos();

        if(typeof callback === 'function') {
            if(document.readyState !== 'complete') {
                document.onreadystatechange = function() {
                    if(document.readyState === 'complete') {
                        document.onreadystatechange = null;
                        setTimeout(callback, 25);
                    }
                }
            } else {
                setTimeout(callback, 25);
            }
        }
    }

    function showAllLogos() {
	    
        var targetPage = Math.ceil(logos.length / pageSize, 10) - 1;
        addLogos(targetPage);
    }

    function checkLoadMoreLogos() {
        jQuery('#loadMoreLogos').toggle(((currentPage+1)*pageSize < logos.length));
    }

    function checkShowAllLogos() {
        //jQuery('#showAllLogos').toggleClass('is-disabled', (currentPage+1)*pageSize >= logos.length);
		
    }

    function initTogglers() {
        var $togglers = jQuery('.item__expandable-contents-toggler');

        $togglers.each(function() {
            if(typeof jQuery(this).data('togglerify') !== 'undefined') return;

            jQuery(this)
                .togglerify({
                    slide: true,
                    content: function() {
                        return jQuery(this).next('.item__expandable-contents');
                    }
                })
                .togglerify('deactivate')
                .on({
                    toggleOn : function(e, $thisToggler, $target) {
                        $thisToggler.parent('.item').addClass('is-toggled');
                    },

                    toggleOff : function(e, $thisToggler, $target) {
                        $thisToggler.parent('.item').removeClass('is-toggled');
                    },

                    afterToggleOn : function(e, $thisToggler, $target) {
                        jQuery(window).trigger('scroll');
                    },

                    afterToggleOff : function(e, $thisToggler, $target) {
                        jQuery(window).trigger('scroll');
                    },

                    click : function(e) {
                        e.preventDefault();

                        if(jQuery(this).hasClass('is-toggled')) {
                            pushHashState({ logo : '' });
                        } else {
                            pushHashState({ logo : jQuery(this).attr('id') });

                            //added by baksin 20140606 for ga tracking
                            var serviceName = window.location.pathname.substr(window.location.pathname.lastIndexOf("/service")+1);
                            
                        }
                    }
                });
        });

        $togglers.off('toggleOn.shared').on('toggleOn.shared', function(e, $thisItemToggler, $content) {
            $togglers.not($thisItemToggler).togglerify('toggleOff');

            $thisItemToggler.one('toggleOff', function() {
                jQuery(window).off('resize.adjustItemWidth');
            });

            refreshContentWidth($thisItemToggler, $content);

            jQuery(window).off('resize.adjustItemWidth').on('resize.adjustItemWidth', homebrew.utils.throttle(function() {
                refreshContentWidth($thisItemToggler, $content);
            }, 100));
        });
    }

    function refreshContentWidth($toggler, $content) {
        $content.css({
            'width': $toggler.closest('.sls-row').closest('.sls-column').width() + 'px',
            'margin-left': -$toggler.parent().parent().position().left + 'px'
        });
    }

    function sortArrayAlphabetically(array) {
        return array.sort(function(a, b) {
            a = a.toLowerCase();
            b = b.toLowerCase();

            return (a < b) ? -1 : 1;
        });
    }





    /*---- Filter togglers  ----*/


    var $filter = jQuery('#logosFilter'),
        $filterTogglers = $filter.find('.filter__toggler'),
        $filterPopupTogglers = $filter.find('.filter-popup').find('.filter__toggler'),
        $filterTogglerContents = $filter.find('.filter__toggler-contents');

    $filterTogglers.togglerify({
        pretoggle: 0,
        singleActive: false,
        slide: true,
        content: function(index) {
            return $filterTogglerContents.eq(index);
        }
    });

    FE.watchSize('medium', function(mq) {
        isMediumScreen = mq;
        
        if(mq) {
            $filterTogglers
                .togglerify('toggleOff', { noSlide : true })
                .togglerify('activate')
                .eq(0)
                    .togglerify('toggleOn', { noSlide : true });
        } else {
            $filterPopupTogglers
                .togglerify('toggleOn', { noSlide : true })
                .togglerify('deactivate');
        }
    });


    /*---- Filter popup toggler ----*/


    var $filterPanel = jQuery('#logosFilter').children('.filter');

    $filterPanel.data('filter-popup', {
        reveal : function() {
            jQuery('body').addClass('filter-popup-is-shown');
        },

        conceal : function() {
            jQuery('body').removeClass('filter-popup-is-shown');
        }
    });

    $filterPanel.find('[data-close-popup]').on('click', function(e) {
        e.preventDefault();
        $filterPanel.data('filter-popup').conceal();
    });

    jQuery('#logosFiltersPopupToggler').on('click', function(e) {
        e.preventDefault();
        $filterPanel.data('filter-popup').reveal();
    });
});


var base64 = {};

base64.PADCHAR = '=';

base64.ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';



base64.makeDOMException = function() {

    // sadly in FF,Safari,Chrome you can't make a DOMException

    var e, tmp;

    try {

        return new DOMException(DOMException.INVALID_CHARACTER_ERR);

    } catch (tmp) {

        // not available, just passback a duck-typed equiv

        // https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Error

        // https://developer.mozilla.org/en/Core_JavaScript_1.5_Reference/Global_Objects/Error/prototype

        var ex = new Error("DOM Exception 5");

        // ex.number and ex.description is IE-specific.

        ex.code = ex.number = 5;

        ex.name = ex.description = "INVALID_CHARACTER_ERR";

        // Safari/Chrome output format

        ex.toString = function() { return 'Error: ' + ex.name + ': ' + ex.message; };

        return ex;

    }

}

base64.getbyte64 = function(s,i) {

    // This is oddly fast, except on Chrome/V8.

    //  Minimal or no improvement in performance by using a

    //   object with properties mapping chars to value (eg. 'A': 0)

    var idx = base64.ALPHA.indexOf(s.charAt(i));

    if (idx === -1) {

        throw base64.makeDOMException();

    }

    return idx;

}



base64.decode = function(s) {

    // convert to string

    s = '' + s;

    var getbyte64 = base64.getbyte64;

    var pads, i, b10;

    var imax = s.length

    if (imax === 0) {

        return s;

    }

    if (imax % 4 !== 0) {

        throw base64.makeDOMException();

    }

    pads = 0

    if (s.charAt(imax - 1) === base64.PADCHAR) {

        pads = 1;

        if (s.charAt(imax - 2) === base64.PADCHAR) {

            pads = 2;

        }

        // either way, we want to ignore this last block

        imax -= 4;

    }



    var x = [];

    for (i = 0; i < imax; i += 4) {

        b10 = (getbyte64(s,i) << 18) | (getbyte64(s,i+1) << 12) |

            (getbyte64(s,i+2) << 6) | getbyte64(s,i+3);

        x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff, b10 & 0xff));

    }

    switch (pads) {

    case 1:

        b10 = (getbyte64(s,i) << 18) | (getbyte64(s,i+1) << 12) | (getbyte64(s,i+2) << 6);

        x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff));

        break;

    case 2:

        b10 = (getbyte64(s,i) << 18) | (getbyte64(s,i+1) << 12);

        x.push(String.fromCharCode(b10 >> 16));

        break;

    }

    return x.join('');

}

base64.getbyte = function(s,i) {

    var x = s.charCodeAt(i);

    if (x > 255) {

        throw base64.makeDOMException();

    }

    return x;

}

base64.encode = function(s) {

    if (arguments.length !== 1) {

        throw new SyntaxError("Not enough arguments");

    }

    var padchar = base64.PADCHAR;

    var alpha   = base64.ALPHA;

    var getbyte = base64.getbyte;

    var i, b10;

    var x = [];

    // convert to string

    s = '' + s;

    var imax = s.length - s.length % 3;

    if (s.length === 0) {

        return s;

    }

    for (i = 0; i < imax; i += 3) {

        b10 = (getbyte(s,i) << 16) | (getbyte(s,i+1) << 8) | getbyte(s,i+2);

        x.push(alpha.charAt(b10 >> 18));

        x.push(alpha.charAt((b10 >> 12) & 0x3F));

        x.push(alpha.charAt((b10 >> 6) & 0x3f));

        x.push(alpha.charAt(b10 & 0x3f));

    }

    switch (s.length - imax) {

    case 1:

        b10 = getbyte(s,i) << 16;

        x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) +

               padchar + padchar);

        break;

    case 2:

        b10 = (getbyte(s,i) << 16) | (getbyte(s,i+1) << 8);

        x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) +

               alpha.charAt((b10 >> 6) & 0x3f) + padchar);

        break;

    }

    return x.join('');

}